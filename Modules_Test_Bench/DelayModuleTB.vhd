library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity DelayModuleTB is
end DelayModuleTB; 


architecture sim of DelayModuleTB is
	component DelayModule is
		port	(CLK : in std_logic;
                      
				 Delay_Signal_In	: in std_logic;
				 
				 Delay_Value : in STD_LOGIC_VECTOR (7 DOWNTO 0);
				 Reset				: IN STD_LOGIC;
				 
				 Delay_Out	: out std_logic);

	end component;
	
	signal CLK : std_logic;
	signal		Delay_Signal_In	: std_logic;
	signal 		Delay_Value : STD_LOGIC_VECTOR (7 DOWNTO 0):= "00001010";
	signal Reset				: STD_LOGIC;
			
	signal		Delay_Out	: std_logic;
			
			
			constant clk_period     : time := 20 ns;

--			constant RAM_DEPTH : INTEGER := 10;
			
			begin 
			
			--instantiating "Device Under Test and all necessary inputs
				DUT     : DelayModule port map (
				Delay_Value => Delay_Value,
				CLK => CLK,
				Reset => Reset,
				Delay_Signal_In => Delay_Signal_In,
				Delay_Out => Delay_Out);
				
				
				PROC_Sequence   : process
                begin
                   clk <= '0';
                wait for clk_period/2;
                        clk <='1';
                
                wait for clk_period/2;
             end process;
				
			
				STIM_proc	: process
					
					begin
					Delay_Signal_In <= '0';
					Reset <= '1';
					 wait for clk_period*10.5;
                    Delay_Signal_In <= '0';
                wait for clk_period*5;
                    Delay_Signal_In <= '1';
					 wait for clk_period*3;
						  Delay_Signal_In <= '0';
						  Reset <= '0';
					 wait for clk_period*10;
						  Delay_Signal_In <= '1';
					 wait for clk_period*2;
						  Delay_Signal_In <= '0';
					wait for clk_period*12;
							Delay_Signal_In <= '1';
					wait for clk_period*6;
							Delay_Signal_In <= '0';
					wait for clk_period*12;
							Delay_Signal_In <= '1';
					 
				end process;
				
end architecture;