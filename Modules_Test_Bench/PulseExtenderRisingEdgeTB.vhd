library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;


entity PulseExtenderRisingEdgeTB is
    end PulseExtenderRisingEdgeTB;


architecture sim of PulseExtenderRisingEdgeTB is
    -- adds "compononet" instantiates it.
        component PulseExtenderRisingEdge is
                port (CLK : in std_logic;
							 Pulse_Extend_Signal_In : in std_logic;
                      Coincidence_Window : in std_logic_vector (7 downto 0);
							 Reset : IN Std_logic;
							 Pulse_Extend_Out : out std_logic);
        end component;
    
    signal CLK: std_logic;
	 signal Pulse_Extend_Signal_In : std_logic;
	 signal Pulse_Extend_Out : std_logic;
	 signal Reset : Std_logic;
    signal Coincidence_Window : std_logic_vector (7 downto 0) := "00001000";
	 
    constant CLK_period     : time := 20 ns; 

	 
    begin
            --instantiating "Device Under Test and all necessary inputs
				DUT     : PulseExtenderRisingEdge port map (
							CLK => CLK,
							Reset => Reset,
							Pulse_Extend_Signal_In => Pulse_Extend_Signal_In,
							Pulse_Extend_Out => Pulse_Extend_Out,
							Coincidence_Window => Coincidence_Window --pins on the  module and i'm connecting to
 
					);
    
    
         PROC_Sequence   : process
			begin
               CLK <= '0';
			wait for CLK_period/2;
					CLK <='1';
			
			wait for clk_period/2;
         end process;
			
        
         STIM_proc	: process
         begin
					Pulse_Extend_Signal_In <= '0';
					Reset <= '1';
      	wait for clk_period*5.5;
					Pulse_Extend_Signal_In <= '1';

			wait for clk_period*9;
					Pulse_Extend_Signal_In <= '0';
					Reset <= '0';

			wait for clk_period*8;
					Pulse_Extend_Signal_In <= '1';
			wait for clk_period*5;
					Pulse_Extend_Signal_In <= '0';
			wait for clk_period*2;
					Pulse_Extend_Signal_In <= '1';
			wait for clk_period*4;
					Pulse_Extend_Signal_In <= '0';
			wait for clk_period*2;
					Reset <= '1';
			wait for clk_period*4;
					Pulse_Extend_Signal_In <= '1';
			wait for clk_period*4;
					Pulse_Extend_Signal_In <= '0';
			wait for clk_period*4;
			
			
        end process;
    
    
    end sim;