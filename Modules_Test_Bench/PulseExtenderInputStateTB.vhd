library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;


entity PulseExtenderInputStateTB is
    end PulseExtenderInputStateTB;


architecture sim of PulseExtenderInputStateTB is
    -- adds "compononet" instantiates it.
        component PulseExtenderInputState is
                port (CLK : in std_logic;
							 Pulse_Extend_Signal_In : in std_logic;
							 Coincidence_Window 		: in std_logic_vector(7  DOWNTO 0);
							 Reset	: in std_logic;
							 Pulse_Extend_Out : out std_logic);
        end component;
    
    signal CLK: std_logic;
	 signal Pulse_Extend_Signal_In : std_logic;
	 signal Pulse_Extend_Out : std_logic;
	 signal Coincidence_Window 	: std_logic_vector(7  DOWNTO 0);
	 signal Reset: 	std_logic;
    constant clk_period     : time := 20 ns; 

    begin
            --instantiating "Device Under Test and all necessary inputs
		DUT     : PulseExtenderInputState port map (
					CLK => CLK,
					Pulse_Extend_Signal_In => Pulse_Extend_Signal_In,
					Pulse_Extend_Out => Pulse_Extend_Out,
					Coincidence_Window => Coincidence_Window,
					Reset => Reset
            );
     
    
         PROC_Sequence   : process
			begin
               clk <= '0';
			wait for clk_period/2;
					clk <='1';
			
			wait for clk_period/2;
         end process;
			
        
         STIM_proc	: process
         begin
					Coincidence_Window <= "00000100";
					Reset <= '1';

					Pulse_Extend_Signal_In <= '0';
      	wait for clk_period*10.5;
					Reset <= '0';
					Pulse_Extend_Signal_In <= '1';
			wait for clk_period*2;
					Pulse_Extend_Signal_In <= '0';
					Reset <= '1';
			wait for clk_period*5;
					Pulse_Extend_Signal_In <= '1';
					Reset <= '0';
			wait for clk_period*5;
					Pulse_Extend_Signal_In <= '0';
			wait for clk_period*3;
					Pulse_Extend_Signal_In <= '1';
			wait for clk_period*4;	
				Pulse_Extend_Signal_In <= '0';
			wait for clk_period*10;	
				Pulse_Extend_Signal_In <= '1';
        end process;
    
    
    end sim;