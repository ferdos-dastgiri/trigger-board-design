-- Copyright (C) 2021  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "03/11/2022 10:25:22"
                                                            
-- Vhdl Test Bench template for design  :  SinglesCounter
-- 
-- Simulation tool : Questa Intel FPGA (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY SinglesCounterTB IS
END SinglesCounterTB;
ARCHITECTURE SinglesCounter_arch OF SinglesCounterTB IS
-- constants                                                 
-- signals                                                   
SIGNAL CLK_counter : STD_LOGIC;
SIGNAL Singles_in : STD_LOGIC;
SIGNAL Singles_out : STD_LOGIC;
SIGNAL Singles_scalar : STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL Singles_veto : STD_LOGIC;
COMPONENT SinglesCounter
	PORT (
	CLK_counter : IN STD_LOGIC;
	Singles_in : IN STD_LOGIC;
	Singles_out : OUT STD_LOGIC;
	Singles_scalar : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
	Singles_veto : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : SinglesCounter
	PORT MAP (
-- list connections between master ports and signals
	CLK_counter => CLK_counter,
	Singles_in => Singles_in,
	Singles_out => Singles_out,
	Singles_scalar => Singles_scalar,
	Singles_veto => Singles_veto
	);
init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once  
		
		Singles_in <= '0';
		
		--Singles_out <= '0';
		Singles_veto <= '0';
		Singles_scalar <= "0000000000000011";
		
		wait for 50 ns;
		Singles_veto <= '1';
		
		wait for 50 ns;
		Singles_in <= '1';
		
		wait for 50 ns;
		Singles_in <= '0';
		
		wait for 50 ns;
		Singles_in <= '1';
		
		wait for 50 ns;
		Singles_in <= '0';
		
		wait for 50 ns;
		Singles_veto <= '0';
		
		wait for 50 ns;
		Singles_in <= '1';
		
		wait for 50 ns;
		Singles_in <= '0';
		
		wait for 50 ns;
		Singles_in <= '1';
		
		wait for 50 ns;
		Singles_in <= '0';
		
		wait for 50 ns;
		Singles_veto <= '1';
		
		wait for 50 ns;
		Singles_in <= '1';
		
		wait for 50 ns;
		Singles_in <= '0';
		
		wait for 50 ns;
		Singles_in <= '1';
		
		wait for 50 ns;
		Singles_in <= '0';
		
		wait for 50 ns;
		Singles_veto <= '0';
		
		wait for 50 ns;
		Singles_in <= '1';
		
		wait for 50 ns;
		Singles_in <= '0';
		
		wait for 50 ns;
		Singles_in <= '1';
		
		wait for 50 ns;
		Singles_in <= '0';
		
		
WAIT;                                                       
END PROCESS init;                                           
always : PROCESS                                              
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN                                                         
        -- code executes for every event on sensitivity list
	
	CLK_counter <= '0';
	wait for 10 ns;
	CLK_counter <= '1';
	wait for 10 ns;	  
                                                        
END PROCESS always;                                             
END SinglesCounter_arch;
