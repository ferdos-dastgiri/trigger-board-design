library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;


entity MultiplicityTriggerTB is
    end MultiplicityTriggerTB;


architecture simulation of MultiplicityTriggerTB is
    -- adds "compononet" instantiates it.
        component MultiplicityTrigger is
                port (CLK : in std_logic;
                      Multiplicity_Signal_in : in std_logic_vector (31 downto 0);
                      Multiplicity : in std_logic_vector (7 downto 0);
							 MultiplicityTrigger_Out	: out std_logic;
							 Reset			: IN STD_LOGIC);
        end component;
    signal CLK: std_logic;
    signal Multiplicity_Signal_in: std_logic_vector (31 downto 0);
    signal Multiplicity : std_logic_vector (7 downto 0);
	 signal MultiplicityTrigger_Out	: std_logic;
	 signal Reset			: STD_LOGIC;
    constant clk_period     : time := 20 ns; 

    --signal sim_stop : std_logic;		
    begin
            --instantiating "Device Under Test and all necessary inputs
            DUT         : MultiplicityTrigger port map (
                CLK => CLK,
                Multiplicity_Signal_in => Multiplicity_Signal_in,
                Multiplicity => Multiplicity, --pins on the  module and i'm connecting to 
					 MultiplicityTrigger_Out => MultiplicityTrigger_Out,
					 Reset => Reset
            );
    
    --	stop_condition <= not stop_condition after 1 ms;  
    
        PROC_Sequence   : process
        begin
                clk <= '0';
		wait for clk_period/2;
		        clk <='1';
        wait for clk_period/2;
        end process;
        
        STIM_proc		: process
        begin
            Multiplicity <= "00000010";
        -- 	-- hold the reset state for 200 ns
				Reset <= '1';
            Multiplicity_Signal_in <= "00000000000000000000000000000000";
			wait for clk_period*10.5;
			 Reset <= '0';
		    Multiplicity_Signal_in(2) <= '1';
	    wait for clk_period*13;
		    Multiplicity_Signal_in(2) <= '1';
		    Multiplicity_Signal_in(11) <= '1';
		 wait for clk_period*8;
			 Reset <= '1';
	    wait for clk_period*15;
		    Multiplicity_Signal_in(29) <= '1';
	    wait for clk_period*15;
		    Multiplicity_Signal_in(2) <= '0';
	    wait for clk_period*20;
		    Multiplicity_Signal_in(29) <= '0';
		    Multiplicity_Signal_in(11) <= '1';
	    wait for clk_period*2;
        end process;
    
    
    end simulation;
