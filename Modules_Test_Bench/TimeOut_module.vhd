LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;

LIBRARY work;

ENTITY TimeOut_module IS
	PORT (
		CLK			 			: IN STD_LOGIC;
		Reset 					: IN STD_LOGIC := '0';
		
		Triggered_in	 		: IN STD_LOGIC;
		
		Timeout_limit 			: IN STD_LOGIC_VECTOR (5 DOWNTO 0) := "111111";
		
		Timeout_out 			: OUT STD_LOGIC := '0'
	);
END TimeOut_module;

ARCHITECTURE timeout OF TimeOut_Module IS

	
BEGIN	timeout_process : PROCESS (CLK) IS
	variable count	: std_logic_vector (5 DOWNTO 0) := "000000";

	BEGIN

		IF (rising_edge(CLK)) THEN
			-- Cycles through trigger modes
			IF (Reset = '1') THEN
				Timeout_out <= '0';
			ELSE
				IF (Triggered_in = '1') THEN
					IF (count < Timeout_limit) THEN
						count := count + 1;
						Timeout_out <= '1';
					ELSE
						count := "000000";
						Timeout_out <= '0';
					END IF;
				ELSE
					count := "000000";
					Timeout_out <= '0';
				END IF;			
			END IF;
		END IF;

	END PROCESS timeout_process;
END ARCHITECTURE timeout;