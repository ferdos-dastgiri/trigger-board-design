-- Copyright (C) 2021  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "03/16/2022 13:50:22"
                                                            
-- Vhdl Test Bench template for design  :  StartModule
-- 
-- Simulation tool : Questa Intel FPGA (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY StartModuleTB IS
END StartModuleTB;
ARCHITECTURE StartModule_arch OF StartModuleTB IS
-- constants                                                 
-- signals                                                   
SIGNAL CLK_start : STD_LOGIC;
SIGNAL Start_Out : STD_LOGIC;
SIGNAL User_Start : STD_LOGIC;
COMPONENT StartModule
	PORT (
	CLK_start : IN STD_LOGIC;
	Start_Out : OUT STD_LOGIC;
	User_Start : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : StartModule
	PORT MAP (
-- list connections between master ports and signals
	CLK_start => CLK_start,
	Start_Out => Start_Out,
	User_Start => User_Start
	);
init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once 
	User_Start <= '0';
	
	wait for 50 ns;
	
	User_Start <= '1';
	wait for 30 ns;
	User_Start <= '0';
	
	wait for 50 ns;
	
	User_Start <= '1';
	wait for 30 ns;
	User_Start <= '0';
	
	wait for 30 ns;
	User_Start <= '1';
	wait for 30 ns;
	User_Start <= '0';
		  
WAIT;                                                       
END PROCESS init;                                           
always : PROCESS                                              
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN                                                         
        -- code executes for every event on sensitivity list  
		  CLK_start <= '0';
		  wait for 10 ns;
		  CLK_start <= '1';
		  wait for 10 ns;
END PROCESS always;                                          
END StartModule_arch;
