-- Copyright (C) 2021  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "03/16/2022 14:11:48"
                                                            
-- Vhdl Test Bench template for design  :  singles_Veto_Module
-- 
-- Simulation tool : Questa Intel FPGA (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY SinglesDetectorTB IS
END SinglesDetectorTB;
ARCHITECTURE singles_Veto_Module_arch OF SinglesDetectorTB IS
-- constants                                                 
-- signals                                                   
SIGNAL CLK_Sing_Veto : STD_LOGIC;
SIGNAL Pulses_In : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL Singlesveto : STD_LOGIC;
COMPONENT SinglesDetector
	PORT (
	CLK_Sing_Veto : IN STD_LOGIC;
	Pulses_In : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
	Singlesveto : OUT STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : SinglesDetector
	PORT MAP (
-- list connections between master ports and signals
	CLK_Sing_Veto => CLK_Sing_Veto,
	Pulses_In => Pulses_In,
	Singlesveto => Singlesveto
	);
init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once  
		FOR i in 0 to 31 LOOP
			Pulses_In(i) <= '0';
		END LOOP;
		
		wait for 50 ns;

		Pulses_In(1) <= '1';
		wait for 30 ns;
		Pulses_In(1) <= '0';
		wait for 30 ns;

		Pulses_In(1) <= '1';
		wait for 30 ns;
		Pulses_In(1) <= '0';
		wait for 30 ns;

		Pulses_In(1) <= '1';
		wait for 30 ns;
		Pulses_In(1) <= '0';
		wait for 30 ns;

		wait for 50 ns;
		
		Pulses_In(1) <= '1';
		wait for 30 ns;
		Pulses_In(2) <= '1';
		wait for 30 ns;
		Pulses_In(1) <= '0';
		wait for 30 ns;
		Pulses_In(2) <= '0';
		wait for 30 ns;
		  
WAIT;                                                       
END PROCESS init;                                           
always : PROCESS                                              
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN                                                         
        -- code executes for every event on sensitivity list  

		CLK_Sing_Veto <= '0';
		wait for 10 ns;
		CLK_Sing_Veto <= '1';
		wait for 10 ns;
		
END PROCESS always;                                          
END singles_Veto_Module_arch;
