library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity Counter_method_tb is
	end Counter_method_tb;


architecture sim of Counter_method_tb is

			component Counter_Method is
				port ( 
					CLK					: in	std_logic;
					Signal_In		: in 	std_logic;
					Reset			: in std_logic;
					Counter_Out	: out std_logic_vector (31 DOWNTO 0) );
			end component;
			
			signal CLK	: std_logic;
			signal Signal_In	: 	std_logic;
			signal Reset			: std_logic;
			signal Counter_Out	: std_logic_vector (31 DOWNTO 0);
			
			
			constant CLK_period     : time := 20 ns;
			
			begin
			
				DUT     : Counter_Method port map (
							CLK => CLK,
							Signal_In => Signal_In,
							Counter_Out => Counter_Out,
							Reset => Reset
 
					);
				
					
				PROC_Sequence   : process
				begin
					CLK <= '0';
					wait for CLK_period/2;
							CLK <='1';
					
					wait for clk_period/2;
				end process;
			
			
				STIM_proc	: process
				begin
					Signal_In <= '0';
					Reset <= '0';
					
					wait for clk_period*10.5;
						Signal_In <= '1';

					wait for clk_period*9;
							Signal_In <= '0';

					wait for clk_period*8;
							Signal_In <= '1';
							-- Reset <= '1';
					wait for clk_period*5;
							Signal_In <= '0';
							-- Reset <= '0';
					wait for clk_period*2;
							Signal_In <= '1';
					wait for clk_period*4;
							Signal_In <= '0';
					-- wait for clk_period*2;


					wait for clk_period*9.5;
							Signal_In <= '1';

					wait for clk_period*10;
							Signal_In <= '0';

					wait for clk_period*12;
							Signal_In <= '1';

					wait for clk_period*4;
							Signal_In <= '0';
					wait for clk_period*4;
							Signal_In <= '1';
					wait for clk_period*10;
					wait for clk_period*2;

					wait for clk_period*9.5;
					Signal_In <= '1';

					wait for clk_period*10;
							Signal_In <= '0';

					wait for clk_period*12;
							Signal_In <= '1';

					wait for clk_period*4;
							Signal_In <= '0';
					wait for clk_period*4;
							-- Reset <= '1';
							Signal_In <= '1';
					wait for clk_period*10;
							Reset <= '0';
					wait for clk_period*2;
			
				end process;
end sim; 