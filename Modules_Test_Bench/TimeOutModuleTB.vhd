-- Copyright (C) 2021  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "03/16/2022 13:50:22"
                                                            
-- Vhdl Test Bench template for design  :  StartModule
-- 
-- Simulation tool : Questa Intel FPGA (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;  
use ieee.numeric_std.all;                              

ENTITY TOModuleTB IS
END TOModuleTB;

ARCHITECTURE timeoutarch OF TOModuleTB IS
-- constants                                                 
-- signals                                                   
-- signal CLK			 			: STD_LOGIC;
-- signal Reset 					: STD_LOGIC;
-- signal Triggered_in	 			: STD_LOGIC;
-- signal Timeout_limit 			: STD_LOGIC_VECTOR (5 DOWNTO 0);
-- signal Timeout_out 				: STD_LOGIC;

COMPONENT TimeOut_module IS
	PORT (
		CLK			 			: IN STD_LOGIC;
		Reset 					: IN STD_LOGIC;
		Triggered_in	 		: IN STD_LOGIC;
		Timeout_limit 			: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		Timeout_out 			: OUT STD_LOGIC
	);
END COMPONENT;

signal CLK			 			: STD_LOGIC;
signal Reset 					: STD_LOGIC := '0';
signal Triggered_in	 			: STD_LOGIC;
signal Timeout_limit 			: STD_LOGIC_VECTOR (5 DOWNTO 0) := "111111";
signal Timeout_out 				: STD_LOGIC;

BEGIN
	i1 : TimeOut_Module
	PORT MAP (
-- list connections between master ports and signals
	CLK => CLK,
	Reset => Reset,
	Triggered_in => Triggered_in,
	Timeout_limit => Timeout_limit,
	Timeout_out => Timeout_out
	);

init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once 
	Triggered_in <= '0';
	
	wait for 50 ns;
	
	Triggered_in <= '1';
	wait for 100 ns;
	Triggered_in <= '0';
	
	wait for 50 ns;

	Timeout_limit <= "000011";

	wait for 50 ns;
	Triggered_in <= '1';
	wait for 200 ns;
	Triggered_in <= '0';
	wait for 50 ns;


	wait for 50 ns;

	Timeout_limit <= "000000";

	wait for 50 ns;
	Triggered_in <= '1';
	wait for 200 ns;
	Triggered_in <= '0';
	wait for 50 ns;


	wait for 50 ns;

	Timeout_limit <= "000011";

	wait for 50 ns;
	Triggered_in <= '1';
	wait for 40 ns;
	Triggered_in <= '0';
	wait for 20 ns;
	Triggered_in <= '1';
	wait for 40 ns;
	Triggered_in <= '0';
	wait for 50 ns;
	
	
		  
WAIT;                                                       
END PROCESS init;                                           
always : PROCESS                                              
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN                                                         
        -- code executes for every event on sensitivity list  
		  CLK <= '0';
		  wait for 10 ns;
		  CLK <= '1';
		  wait for 10 ns;
END PROCESS always;                                          
END architecture;
