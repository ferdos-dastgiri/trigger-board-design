-- Copyright (C) 2021  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "03/16/2022 10:44:48"
                                                            
-- Vhdl Test Bench template for design  :  GlobalTrigger
-- 
-- Simulation tool : Questa Intel FPGA (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;   
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;                             

ENTITY GlobalTriggerTB IS
END GlobalTriggerTB;
ARCHITECTURE GlobalTrigger_arch OF GlobalTriggerTB IS
-- constants                                                 
-- signals                                                   
SIGNAL A_mult : STD_LOGIC_VECTOR (5 DOWNTO 0);
SIGNAL A_Trigger_singles : STD_LOGIC;
SIGNAL B_mult : STD_LOGIC_VECTOR (5 DOWNTO 0);
SIGNAL B_Trigger_singles : STD_LOGIC;
SIGNAL CLK : STD_LOGIC;
SIGNAL Trigger_mode : STD_LOGIC_VECTOR(1 DOWNTO 0);
SIGNAL Trigger_out : STD_LOGIC;
SIGNAL A_Thresh : STD_LOGIC_VECTOR (5 DOWNTO 0);
SIGNAL B_Thresh : STD_LOGIC_VECTOR (5 DOWNTO 0);
SIGNAL Reset : STD_LOGIC;

COMPONENT GlobalTrigger_Revamped
	PORT (
		CLK			 			: IN STD_LOGIC;
		Reset 					: IN STD_LOGIC := '0';
		
		A_Trigger_singles 		: IN STD_LOGIC;
		A_mult 					: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		B_Trigger_singles 		: IN STD_LOGIC;
		B_mult 					: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		
		Trigger_mode 			: IN STD_LOGIC_VECTOR (1 DOWNTO 0);
		A_Thresh 				: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		B_Thresh 				: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		
		Trigger_out 			: OUT STD_LOGIC := '0'
	);
END COMPONENT;
BEGIN
	i1 : GlobalTrigger_Revamped
	PORT MAP (
-- list connections between master ports and signals
	CLK => CLK,
	Reset => Reset,

	A_Trigger_singles => A_Trigger_singles,
	B_Trigger_singles => B_Trigger_singles,
	A_mult => A_mult,
	B_mult => B_mult,
	
	Trigger_mode => Trigger_mode,
	A_Thresh => A_Thresh,
	B_Thresh => B_Thresh,
	
	Trigger_out => Trigger_out
	);
init : PROCESS                                           
-- variable declarations  
                                   
BEGIN                                                        
        -- code that executes only once  
	A_mult <= "000000";
	A_Trigger_singles <= '0';
	B_mult <= "000000";
	B_Trigger_singles <= '0';
	Trigger_mode <= "00";
	Reset <= '0';
	A_Thresh <= "000001";
	B_Thresh <= "000001";
	--Trigger_out <=	"00000";
	
	wait for 100 ns;
	
	Trigger_mode <= "00";
	wait for 50 ns;
	A_mult <= "000001";
	wait for 50 ns;
	A_mult <= "000000";
	wait for 50 ns;
	A_Trigger_singles <= '1';
	wait for 50 ns;
	A_Trigger_singles <= '0';
	wait for 50 ns;
	B_mult <= "000001";
	wait for 50 ns;
	B_mult <= "000000";
	wait for 50 ns;
	B_Trigger_singles <= '1';
	wait for 50 ns;
	B_Trigger_singles <= '0';
	
	wait for 50 ns;
	A_mult <= "000001";
	wait for 50 ns;
	A_Trigger_singles <= '1';
	wait for 50 ns;
	A_Trigger_singles <= '0';
	A_mult <= "000000";
	wait for 50 ns;
	B_mult <= "000001";
	wait for 50 ns;
	B_Trigger_singles <= '1';
	wait for 50 ns;
	B_mult <= "000000";
	B_Trigger_singles <= '0';
	
	wait for 50 ns;
	A_mult <= "000001";
	wait for 50 ns;
	B_mult <= "000001";
	wait for 50 ns;
	B_mult <= "000000";
	A_mult <= "000000";
	
	wait for 100 ns;
	
	Trigger_mode <= "01";
	wait for 50 ns;
	A_mult <= "000001";
	wait for 50 ns;
	A_mult <= "000000";
	wait for 50 ns;
	A_Trigger_singles <= '1';
	wait for 50 ns;
	A_Trigger_singles <= '0';
	wait for 50 ns;
	B_mult <= "000001";
	wait for 50 ns;
	B_mult <= "000000";
	wait for 50 ns;
	B_Trigger_singles <= '1';
	wait for 50 ns;
	B_Trigger_singles <= '0';
	
	wait for 50 ns;
	A_mult <= "000001";
	wait for 50 ns;
	A_Trigger_singles <= '1';
	wait for 50 ns;
	A_Trigger_singles <= '0';
	A_mult <= "000000";
	wait for 50 ns;
	B_mult <= "000001";
	wait for 50 ns;
	B_Trigger_singles <= '1';
	wait for 50 ns;
	B_mult <= "000000";
	B_Trigger_singles <= '0';
	
	wait for 50 ns;
	A_mult <= "000001";
	wait for 50 ns;
	B_mult <= "000001";
	wait for 50 ns;
	B_mult <= "000000";
	A_mult <= "000000";
	
	wait for 100 ns;
	
	Trigger_mode <= "10";
	wait for 50 ns;
	A_mult <= "000001";
	wait for 50 ns;
	A_mult <= "000000";
	wait for 50 ns;
	A_Trigger_singles <= '1';
	wait for 50 ns;
	A_Trigger_singles <= '0';
	wait for 50 ns;
	B_mult <= "000001";
	wait for 50 ns;
	B_mult <= "000000";
	wait for 50 ns;
	B_Trigger_singles <= '1';
	wait for 50 ns;
	B_Trigger_singles <= '0';
	
	wait for 50 ns;
	A_mult <= "000001";
	wait for 50 ns;
	A_Trigger_singles <= '1';
	wait for 50 ns;
	A_Trigger_singles <= '0';
	A_mult <= "000000";
	wait for 50 ns;
	B_mult <= "000001";
	wait for 50 ns;
	B_Trigger_singles <= '1';
	wait for 50 ns;
	B_mult <= "000000";
	B_Trigger_singles <= '0';
	
	wait for 50 ns;
	A_mult <= "000001";
	wait for 50 ns;
	B_mult <= "000001";
	wait for 50 ns;
	B_mult <= "000000";
	A_mult <= "000000";
	
	wait for 100 ns;
	
	Trigger_mode <= "11";
	wait for 50 ns;
	A_mult <= "000001";
	wait for 50 ns;
	A_mult <= "000000";
	wait for 50 ns;
	A_Trigger_singles <= '1';
	wait for 50 ns;
	A_Trigger_singles <= '0';
	wait for 50 ns;
	B_mult <= "000001";
	wait for 50 ns;
	B_mult <= "000000";
	wait for 50 ns;
	B_Trigger_singles <= '1';
	wait for 50 ns;
	B_Trigger_singles <= '0';
	
	wait for 50 ns;
	A_mult <= "000001";
	wait for 50 ns;
	A_Trigger_singles <= '1';
	wait for 50 ns;
	A_Trigger_singles <= '0';
	A_mult <= "000000";
	wait for 50 ns;
	B_mult <= "000001";
	wait for 50 ns;
	B_Trigger_singles <= '1';
	wait for 50 ns;
	B_mult <= "000000";
	B_Trigger_singles <= '0';
	
	wait for 50 ns;
	A_mult <= "000001";
	wait for 50 ns;
	B_mult <= "000001";
	wait for 50 ns;
	B_mult <= "000000";
	A_mult <= "000000";
	
	wait for 100 ns;
	
WAIT;                                                       
END PROCESS init;                                           

always : PROCESS                                              
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN                                                         
	CLK <= '0';
	wait for 10 ns;
	CLK <= '1';
	wait for 10 ns;                                                        
END PROCESS always;                                          
END GlobalTrigger_arch;
