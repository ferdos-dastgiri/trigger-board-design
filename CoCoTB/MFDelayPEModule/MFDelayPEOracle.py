from ast import Del
import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb.triggers import FallingEdge
from queue import Queue
from collections import deque

import os
import sys

sys.path.insert(0,'../MultiFlopSynchroniser/')
from MultiFlopSynchroniserOracle import MultiFlopSynchroniserOracle
sys.path.insert(0,'../PulseExtenderCombined/')
from PulseExtenderCombinedOracle import PulseExtenderCombined
sys.path.insert(0,'../DelayModule/')
from DelayModuleOracle import DelayModuleOracle

class MFDelayPEOracle:
    def __init__(self, logger):
        self.logger = logger
        self.multiFlopSynchroniserOracles = []
        self.delayOracles = []
        self.peCombinedOracles = []
        for i in range(32):
            self.multiFlopSynchroniserOracles.append(MultiFlopSynchroniserOracle(3))
            self.delayOracles.append(DelayModuleOracle(3))
            self.peCombinedOracles.append(PulseExtenderCombined(0, 3))

        self.clockcycle = 0
        
        # #Have blank virtual values between modules
        self.mfOutput = []
        self.delayOutput = []
        self.peOutput = []
        
        for _ in range(32):
            self.mfOutput.append(0)
            self.delayOutput.append(0)
            self.peOutput.append(0)

        
    def setInput(self, Signal_In, Delay_Value, Coincidence_window, reset, Coincidence_type):
        self.clockcycle += 1
        self.reset = reset
        
        # Pulse Extender, Check for updated coincidence window or mode
        for i in range(32):
            self.peCombinedOracles[i].setInput(self.delayOutput[i], Coincidence_window, reset, Coincidence_type.value)
            self.peOutput[i] = self.peCombinedOracles[i].getPredictedOutput()
        
        #Delay Module
        for i in range(32):
            #Check for updated delay length
            self.delayOracles[i].setInput(self.mfOutput[i], int(str(Delay_Value[i]), 2), reset)
            self.delayOutput[i] = self.delayOracles[i].getPredictedOutput()
            # Delayoutputs.append(self.delayOutput[i])

        #Multiflop
        for i in range(32):
            self.multiFlopSynchroniserOracles[i].setInput(Signal_In[i])
            # self.multiFlopSynchroniserOracles[i].checkOutput(dut.syncOut.value)
            self.mfOutput[i] = self.multiFlopSynchroniserOracles[i].getPredictedOutput()


    def checkOutput(self, output):
        for i in range(32):
            # print("|", self.peOutput[i], output[i], type(self.peOutput[i]), type(output[i]), int(output[i]))
            #VALUE = int(str(output[i]), 2)
            assert self.peOutput[i] == int(str(output[i]), 2), f"Outputs are not the same: VHDL {output[i]} != Oracle {self.peOutput[i]}, on cycle number {self.clockcycle}"
        #print("Did this work?")


    def getPredictedOutput(self):
        return self.peOutput





async def everyClock(dut):
    mFDelayPEOracle = MFDelayPEOracle(dut._log)
    
    while True:
        await FallingEdge(dut.CLK)
        #print("A",dut.Signal_In.value)
        mFDelayPEOracle.setInput(dut.Signal_In.value, dut.Delay_Value.value, dut.Coincidence_window.value, dut.Reset.value, dut.Coincidence_type.value)
        mFDelayPEOracle.checkOutput(dut.Signal_Out.value) 
     