import random
import cocotb
from MFDelayPEOracle import everyClock
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb import simulator
from cocotb.handle import *

checkpoint_hier = []
checkpoints = {}
#this function must be called prior to creating any checkpoint
def get_checkpoint_hier(entity):
   for ii in entity._handle.iterate(simulator.OBJECTS):
      hdl = SimHandle(ii, entity._path + "." + entity._handle.get_name_string())
      if ((entity._handle.get_type() is simulator.MODULE) or
          (entity._handle.get_type() is simulator.NETARRAY) or
          (entity._handle.get_type() is simulator.GENARRAY)):
            get_checkpoint_hier(hdl)
      elif entity._handle.get_type() is simulator.REG:
            checkpoint_hier.append(hdl)
            
#returns a created checkpoint
def checkpoint():
    checkpoint_data = {}
    #print(len(checkpoint_hier))
    for reg in checkpoint_hier:
        checkpoint_data[reg] = reg.value
    return checkpoint_data
#restores a checkpoint
def restore(checkpoint_data):
    for reg in checkpoint_data:
        reg.setimmediatevalue(checkpoint_data[reg])

async def generate_clock(dut):
    """Generate clock pulses."""
    while True:
            dut.CLK.value = 0
            await Timer(10, units="ns")
            dut.CLK.value = 1
            await Timer(10, units="ns")

async def reset_dut(dut):
    """Resets all the values of the module before a test"""
    for i in range(32):
        dut.Delay_Value[i].value = 0

    dut.Signal_In.value = 0
    dut.Reset.value = 0

    dut.Coincidence_window.value = 0
    dut.Coincidence_type.value = 0

    await Timer(100, units="ns")

async def reset_signal(dut):
    #run the reset on the dut
    dut.Reset.value = 1
    await Timer(20, units="ns")
    dut.Reset.value = 0

    
"""
Tests are located below here.
Print statements if needed:
dut._log.info("Input signal is %s", dut.Delay_Signal_In.value)
dut._log.info("Output signal is %s", dut.Delay_Out.value)
dut._log.info("Delay Length is %s", dut.Delay_Value.value)
"""

@cocotb.test()
async def test1_basic_set_checkpoint(dut):
    """
    Very basic test, turns off and on. This test is used to set the reset condition for future tests.
    """
    
    await reset_dut(dut)
    
    #reset input signals
    
    #Set up checkpoint
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    
    await Timer(40, units="ns")
    #start clock
    
    await cocotb.start(generate_clock(dut))
    
    await RisingEdge(dut.CLK)
    
    await reset_signal(dut)
    #start monitoring
    await cocotb.start(everyClock(dut))
    # reset the system
    #await reset_signal(dut)
    await Timer(40, units="ns")

    dut.Delay_Value[1].value = 3
    await reset_signal(dut)

    # #await cocotb.start(reset_dut(dut))
    # await cocotb.start(generate_clock(dut))
    # await RisingEdge(dut.CLK)
    # await cocotb.start(everyClock(dut))

    await Timer(20, units="ns")
    dut.Signal_In[1].value = 1

    await Timer(40, units="ns")
    dut.Signal_In[1].value = 0
    
    await Timer(260, units="ns")

@cocotb.test()
async def test2_increasing_the_delay_length(dut):

    """
    In this test the delay length is increased after being triggered
    """
    await Timer(40, units="ns")
    #Reset everything
    
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)
    await reset_signal(dut)
    await cocotb.start(everyClock(dut))
    #await reset_signal(dut)
    await Timer(40, units="ns")

    #Set delay length to 3
    dut.Delay_Value[1].value = 3
    await reset_signal(dut) # need to reset for it to take effect
    await RisingEdge(dut.CLK)
    
    #set input high
    await Timer(20, units="ns")
    dut.Signal_In[1].value = 1
    
    #set input low
    await Timer(40, units="ns")
    dut.Signal_In[1].value = 0
    
    #Await the delay length before changing
    await Timer(60, units="ns")

    #change delay length to 5
    dut.Delay_Value[1].value = 5
    await reset_signal(dut) # need to reset for it to take effect
    await RisingEdge(dut.CLK)

    #set input high
    await Timer(20, units="ns")
    dut.Signal_In[1].value = 1
    
    #set input low
    await Timer(40, units="ns")
    dut.Signal_In[1].value = 0
    
    await Timer(200, units="ns")

@cocotb.test()
async def test3_decreasing_the_delay_length(dut):

    """
    In this test the delay length is increased after being triggered
    """
    await Timer(40, units="ns")
    #Reset everything
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)
    await cocotb.start(everyClock(dut))
    await reset_signal(dut)
    await Timer(40, units="ns")

    #Set delay length to 3
    dut.Delay_Value[1].value = 5
    await reset_signal(dut) # need to reset for it to take effect
    await RisingEdge(dut.CLK)
    
    #set input high
    await Timer(20, units="ns")
    dut.Signal_In[1].value = 1
    
    #set input low
    await Timer(40, units="ns")
    dut.Signal_In[1].value = 0
    
    #Await the delay length before changing
    await Timer(60, units="ns")

    #change delay length to 5
    dut.Delay_Value[1].value = 3
    await reset_signal(dut) # need to reset for it to take effect
    await RisingEdge(dut.CLK)

    #set input high
    await Timer(20, units="ns")
    dut.Signal_In[1].value = 1
    
    #set input low
    await Timer(40, units="ns")
    dut.Signal_In[1].value = 0
    
    await Timer(200, units="ns")

@cocotb.test()
async def test4_PE_basic_long_pulse(dut):
    """
    This is a basic test to check pulse extension with a pulse longer than the pulse extender
    """
    restore(checkpoints['0'][0])
    await Timer(40, units="ns")
    #Reset everything
    
    
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)
    await reset_signal(dut)
    
    await cocotb.start(everyClock(dut))

    #Set coincidence window
    await Timer(20, units="ns")
    coincidence = 3
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)
    await reset_signal(dut)

    #Test pulse
    await Timer(20, units="ns")
    dut.Signal_In[1].value = 1
    await Timer(100, units="ns")
    dut.Signal_In[1].value = 0
    
    #Reset time
    await Timer(160, units="ns")


@cocotb.test()
async def test5_multiple_peaks(dut):
    """
    This is a test to check what happens when the module is retriggered in the pulse extender window
    """
    await Timer(40, units="ns")
    
    #Reset everything
    
    restore(checkpoints['0'][0])
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)
    await reset_signal(dut)

    await cocotb.start(everyClock(dut))


    #Set coincidence window
    await Timer(20, units="ns")
    coincidence = 3
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)
    await reset_signal(dut)

    #First pulse
    await Timer(20, units="ns")
    dut.Signal_In[1].value = 1
    await Timer(20, units="ns")
    dut.Signal_In[1].value = 0
    
    #Second Pulse
    await Timer(40, units="ns")
    dut.Signal_In[1].value = 1
    await Timer(20, units="ns")
    dut.Signal_In[1].value = 0
    
    #reset for next test
    await Timer(200, units="ns")

@cocotb.test()
async def test6_all_extensions_0(dut):
    """
    This tests all of the available extension to the pulse extender
    """
    await Timer(40, units="ns")
    #await reset_dut(dut)
    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))
    dut.Coincidence_type.value = 0
    await reset_signal(dut)

    for i in range(255):
        #Set coincidence window
        await Timer(20, units="ns")
        coincidence = i
        coinwind = format(coincidence,'08b')
        dut.Coincidence_Window.value = int(coinwind, base=2)
        await reset_signal(dut)

        #Test pulse
        await Timer(20, units="ns")
        dut.Signal_In[1].value = 1
        await Timer(20, units="ns")
        dut.Signal_In[1].value = 0
        await Timer(20*i+40, units="ns")
        
    #Reset time
    await Timer(100, units="ns")

@cocotb.test()
async def test7_all_extensions_1(dut):
    """
    This tests all of the available extension to the pulse extender
    """
    await Timer(40, units="ns")
    #await reset_dut(dut)
    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))
    dut.Coincidence_type.value = 1
    await reset_signal(dut)

    for i in range(255):
        #Set coincidence window
        await Timer(20, units="ns")
        coincidence = i
        coinwind = format(coincidence,'08b')
        dut.Coincidence_Window.value = int(coinwind, base=2)
        await reset_signal(dut)

        #Test pulse
        await Timer(20, units="ns")
        dut.Signal_In[1].value = 1
        await Timer(20, units="ns")
        dut.Signal_In[1].value = 0
        await Timer(20*i+40, units="ns")
        
    #Reset time
    await Timer(100, units="ns")

@cocotb.test()
async def test8_combined_delay(dut):
    """
    
    """
    await Timer(40, units="ns")
    #await reset_dut(dut)
    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))
    dut.Coincidence_type.value = 0
    coincidence = 2
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)
    await reset_signal(dut)

    for i in range(31):
        #Set channel delay
        dut.Delay_Value[i] = i
        
    await reset_signal(dut)
    
    #Test pulse
    await Timer(20, units="ns")    
    for i in range(31):
        dut.Signal_In[i].value = 1
    await Timer(20, units="ns")
    for i in range(31):
        dut.Signal_In[i].value = 0
    
    await Timer(600, units="ns")
    