import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb.triggers import FallingEdge
from queue import Queue
from collections import deque

class StartModuleOracle:
    def __init__(self, pulseLength):
        self.inputs = deque(maxlen = (pulseLength))
        self.inputs.extend([0]*(pulseLength))
        self.clockCycle = 0
        
    def setInput(self, moduleInput):
        self.clockCycle += 1
        self.input = moduleInput
        self.inputs.append(moduleInput)

    def checkOutput(self, moduleOutput):
        self.output = moduleOutput
        if 1 in self.inputs:
             assert moduleOutput == 1, f"Failed to set outputs high: {moduleOutput} != 1, on cycle number {self.clockCycle}, the previous inputs were {self.inputs}"
        else:
            assert moduleOutput == 0, f"Outputs not low or undefined: {moduleOutput} != 0, on cycle number {self.clockCycle}, the previous inputs were {self.inputs}"


    def getPredictedOutput(self):
        if 1 in self.inputs:
            return 1
        else:
            return 0


async def everyClock(dut):
    startModuleOracle = StartModuleOracle(5)

    while True:
        await FallingEdge(dut.CLK_start)
        startModuleOracle.setInput(dut.User_Start.value) 
        startModuleOracle.checkOutput(dut.Start_Out.value) 
     