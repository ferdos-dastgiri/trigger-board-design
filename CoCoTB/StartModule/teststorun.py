import random
from StartModuleOracle import everyClock
import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge


async def generate_clock(dut):
    """Generate clock pulses."""
    for cycle in range(20):
            dut.CLK_start.value = 0
            #print(dut.CLK_start.value)
            await Timer(10, units="ns")
            dut.CLK_start.value = 1
            #print(dut.CLK_start.value)
            await Timer(10, units="ns")


async def reset_dut(dut):
    """Resets all the values of the module before a test"""
    dut.Start_Out.value = 0
    dut.User_Start.value = 0
    dut.Status.value = 0
    for i in range(3):
        dut.Counter[i].value = 0
    await Timer(100, units="ns")

@cocotb.test()
async def test_on(dut):

    await cocotb.start(reset_dut(dut))

    dut.Start_Out.value = 0
    dut.User_Start.value = 0
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK_start)

    await cocotb.start(everyClock(dut))

    dut._log.info("Input signal is %s", dut.User_Start.value)
    dut._log.info("Output signal is %s", dut.Start_Out.value)

    await Timer(20, units="ns")
    dut.User_Start.value = 1
    
    await Timer(20, units="ns")
    dut.User_Start.value = 0
    dut._log.info("Input signal is %s", dut.User_Start.value)
    dut._log.info("Output signal is %s", dut.Start_Out.value)

    await Timer(60, units="ns")
    dut._log.info("Input signal is %s", dut.User_Start.value)
    dut._log.info("Output signal is %s", dut.Start_Out.value)
    
    #assert dut.Start_Out.value == 1, f"Doesn't turn the output high: {dut.Start_Out.value} != 1"


@cocotb.test()
async def test_off(dut):

    await cocotb.start(reset_dut(dut))

    dut.Start_Out.value = 0
    dut.User_Start.value = 0
    
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK_start)
    
    dut._log.info("Input signal is %s", dut.User_Start.value)
    dut._log.info("Output signal is %s", dut.Start_Out.value)
    
    await cocotb.start(everyClock(dut))
    
    await Timer(20, units="ns")
    dut.User_Start.value = 1
    
    await Timer(20, units="ns")
    dut.User_Start.value = 0
    dut._log.info("Input signal is %s", dut.User_Start.value)
    dut._log.info("Output signal is %s", dut.Start_Out.value)

    #Wait until after the required 5 clock cycles to pass
    await Timer(100, units="ns")
    dut._log.info("Input signal is %s", dut.User_Start.value)
    dut._log.info("Output signal is %s", dut.Start_Out.value)
    
    
    #assert dut.Start_Out.value == 0, f"Output doesn't return to low: {dut.Start_Out.value} != 0"