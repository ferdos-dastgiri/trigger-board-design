import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb.triggers import FallingEdge
from queue import Queue
from collections import deque

class ResetModuleOracle:
    def __init__(self):
        self.inputs = deque(maxlen = (5))
        self.inputs.extend([0]*(5))
        self.outputs = deque(maxlen = (5))
        self.outputs.extend([0]*(5))
        self.clockCycle = 0
        
    def setInput(self, moduleInput):
        self.clockCycle += 1
        self.input = moduleInput
        self.inputs.append(moduleInput)

        sum = 0
        for i in self.outputs:
            sum += i

        self.sum = sum

    def checkOutput(self, moduleOutput):            
        self.outputs.append(moduleOutput)
        if 1 in self.inputs:
            if self.sum == 5:
                assert moduleOutput == 0, f"Outputs not low or undefined: {moduleOutput} != 0, on cycle number {self.clockCycle}, the previous inputs were {self.inputs}"
            else:
                assert moduleOutput == 1, f"Failed to set outputs high: {moduleOutput} != 1, on cycle number {self.clockCycle}, the previous inputs were {self.inputs}"
        else:
            assert moduleOutput == 0, f"Outputs not low or undefined: {moduleOutput} != 0, on cycle number {self.clockCycle}, the previous inputs were {self.inputs}"


    def getPredictedOutput(self):            
        if 1 in self.inputs:
            if self.sum == 5:
                return 0
            else:
                return 1
        else:
            return 0


async def everyClock(dut):
    resetModuleOracle = ResetModuleOracle()

    while True:
        await FallingEdge(dut.CLK)
        resetModuleOracle.setInput(dut.Reset_In.value) 
        resetModuleOracle.checkOutput(dut.Reset_Out.value) 
     