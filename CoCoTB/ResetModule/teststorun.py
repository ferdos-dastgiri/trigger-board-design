import random
from ResetModuleOracle import everyClock
import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge


async def generate_clock(dut):
    """Generate clock pulses."""
    for cycle in range(200):
            dut.CLK.value = 0
            #print(dut.CLK_start.value)
            await Timer(10, units="ns")
            dut.CLK.value = 1
            #print(dut.CLK_start.value)
            await Timer(10, units="ns")


async def reset_dut(dut):
    """Resets all the values of the module before a test"""
    dut.Reset_Out.value = 0
    dut.Reset_In.value = 0
    await Timer(100, units="ns")

@cocotb.test()
async def test_on(dut):

    #await reset_dut(dut)
    dut.Reset_In.value = 0
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    await Timer(60, units="ns")
    dut.Reset_In.value = 1
    
    await Timer(280, units="ns")
    #dut.User_Start.value = 0

    dut.Reset_In.value = 0

    await Timer(200, units="ns")

    
    #assert dut.Start_Out.value == 1, f"Doesn't turn the output high: {dut.Start_Out.value} != 1"


# @cocotb.test()
# async def test_off(dut):

#     await cocotb.start(reset_dut(dut))

#     dut.Start_Out.value = 0
#     dut.User_Start.value = 0
    
#     await cocotb.start(generate_clock(dut))
#     await RisingEdge(dut.CLK_start)
    
#     dut._log.info("Input signal is %s", dut.User_Start.value)
#     dut._log.info("Output signal is %s", dut.Start_Out.value)
    
#     await cocotb.start(everyClock(dut))
    
#     await Timer(20, units="ns")
#     dut.User_Start.value = 1
    
#     await Timer(20, units="ns")
#     dut.User_Start.value = 0
#     dut._log.info("Input signal is %s", dut.User_Start.value)
#     dut._log.info("Output signal is %s", dut.Start_Out.value)

#     #Wait until after the required 5 clock cycles to pass
#     await Timer(100, units="ns")
#     dut._log.info("Input signal is %s", dut.User_Start.value)
#     dut._log.info("Output signal is %s", dut.Start_Out.value)
    
    
    #assert dut.Start_Out.value == 0, f"Output doesn't return to low: {dut.Start_Out.value} != 0"