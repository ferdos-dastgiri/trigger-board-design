import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb.triggers import FallingEdge
from queue import Queue
from collections import deque

"""
This version of the pulse extender is to give a fixed length of output signal set by the coincidence window, regardless of the input signal length.

"""

class PulseExtenderRisingEdgeOracle:
    def __init__(self, Coincidence_Window):
        self.Coincidence = Coincidence_Window
        self.input = 0 
        self.inputs = deque()

        self.inputs.extend([0]*(self.Coincidence+1))
        self.clockCycle = 0
        self.Status = 0
        self.Countdown = 0


    def setInput(self, moduleInput, coincidence, reset):
        self.reset = reset
        self.clockCycle += 1
        if int(coincidence) != 0:
            if int(coincidence) != (self.Coincidence-1):
                if self.Countdown != 0:
                    if int(coincidence) > int(self.Coincidence):
                        self.Countdown = (int(coincidence) - (int(self.Coincidence)-1)) + self.Countdown
                    else:
                        self.Countdown = (int(coincidence) - (int(self.Coincidence)-1)) + self.Countdown
                        if self.Countdown < 0:
                            self.Countdown = 0

                self.Coincidence = int(coincidence)+1

        if moduleInput == 1 and moduleInput != self.input and self.Status == 0:
            self.Status = 1
            self.Countdown = int(self.Coincidence)

        self.input = moduleInput

        if self.Status == 1:
            if self.Countdown > 0:
                self.Countdown -= 1
            elif self.Countdown == 0 and self.input == 0:
                self.Status = 0

        if self.reset == 1:
            self.Status = 0
            self.Countdown = 0
            self.inputs = deque()
            self.inputs.extend([0]*(self.Coincidence+1))
   

    # needs to return values expected by checkOutput
    def getPredictedOutput(self):
        if self.Countdown != 0:
            if self.reset == 0:
                return 1
            else:
                return 0
        else:
            return 0


    def checkOutput(self, moduleOutput):
        self.output = moduleOutput

        if self.Countdown != 0:
            if self.reset == 0:
                assert moduleOutput == 1, f"Failed to set outputs high: {moduleOutput} != 1, on cycle number {self.clockCycle}, the previous inputs were {self.input}"
            else:
                assert moduleOutput == 0, f"Outputs not low or undefined: {moduleOutput} != 0, on cycle number {self.clockCycle}, the previous inputs were {self.input}"  
        else:
            assert moduleOutput == 0, f"Outputs not low or undefined: {moduleOutput} != 0, on cycle number {self.clockCycle}, the previous inputs were {self.input}"


async def everyClock(dut):
    pulseExtenderRisingEdgeOracle = PulseExtenderRisingEdgeOracle(dut.Coincidence_Window.value)

    while True:
        await FallingEdge(dut.CLK)
        pulseExtenderRisingEdgeOracle.setInput(dut.Pulse_Extend_Signal_In.value, dut.Coincidence_Window.value, dut.Reset.value) 
        pulseExtenderRisingEdgeOracle.checkOutput(dut.Pulse_Extend_Out.value) 