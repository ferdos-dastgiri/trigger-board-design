from ast import Del
import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb.triggers import FallingEdge
from queue import Queue
from collections import deque

import os
import sys

sys.path.insert(0,'../MFDelayPEModule/')
from MFDelayPEOracle import MFDelayPEOracle

sys.path.insert(0,'../SinglesDetector/')
from SinglesDetectorOracle import SinglesDetectorOracle

sys.path.insert(0,'../SinglesCounter/')
from SinglesCounterOracle import SinglesCounterOracle

sys.path.insert(0,'../DelayModule/')
from DelayModuleOracle import DelayModuleOracle

sys.path.insert(0,'../MultiplicityTrigger/')
from MultiplicityTriggerOracle import MultiplicityTriggerOracle



class InputPortModuleOracle:
    def __init__(self, logger):
        self.logger = logger
        self.clockcycle = 0

        self.mfDelayPEOracle = MFDelayPEOracle(logger)
        self.singlesDetectorOracle = SinglesDetectorOracle(1)
        self.multiplicityTriggerOracle = MultiplicityTriggerOracle()
        self.delayMultiplicityOracle = DelayModuleOracle(1)


        self.singlesCounterOracle = []
        self.peDelayOracles = []

        for i in range(32):
            self.singlesCounterOracle.append(SinglesCounterOracle(1))
            self.peDelayOracles.append(DelayModuleOracle(1))
        
        # #Have blank virtual values between modules
        self.S1Output = []
        self.SingDetOutput = [0]
        self.singlesdelayOutput = []
        self.MultOutput = []
        self.multdelayOutput = [0]
        self.singlescounterOutput = []
        
        for _ in range(32):
            self.S1Output.append(0)
            self.singlesdelayOutput.append(0)
            self.singlescounterOutput.append(0)

        
    def setInput(self, reset, Port_Signal_In, Delay_Value, Coincidence_window, Coincidence_type, Multiplicity, Scalar_Value):
        self.clockcycle += 1
        self.reset = reset

        #Multiplicity delay
        self.delayMultiplicityOracle.setInput(self.MultOutput, 1, reset)
        self.multdelayOutput = self.delayMultiplicityOracle.getPredictedOutput()
        
        #Multiplicity module
        #print("S1 output is", self.S1Output)
        for i in range(32):
            self.multiplicityTriggerOracle.setInput(i, self.S1Output[i], Multiplicity, reset)
        self.MultOutput = self.multiplicityTriggerOracle.getPredictedOutput()
        

        #Singles Counter
        In1 = Scalar_Value[0]
        In2 = Scalar_Value[1]
        In3 = Scalar_Value[2]
        In4 = Scalar_Value[3]
        In5 = Scalar_Value[4]
        In6 = Scalar_Value[5]
        In7 = Scalar_Value[6]
        In8 = Scalar_Value[7]
        In9 = Scalar_Value[8]
        In10 = Scalar_Value[9]
        In11 = Scalar_Value[10]
        In12 = Scalar_Value[11]
        In13 = Scalar_Value[12]
        In14 = Scalar_Value[13]
        In15 = Scalar_Value[14]
        In16 = Scalar_Value[15]
        for i in range(32):
            self.singlesCounterOracle[i].setInput(self.singlesdelayOutput[i], In1, In2, In3, In4, In5, In6, In7, In8, In9, In10, In11, In12, In13, In14, In15, In16, self.SingDetOutput, reset)
            self.singlescounterOutput[i] = self.singlesCounterOracle[i].getPredictedOutput()

        #Singles delay
        for i in range(32):
            self.delayMultiplicityOracle.setInput(self.S1Output[i], Delay_Value[i], reset)
            self.singlesdelayOutput[i] = self.delayMultiplicityOracle.getPredictedOutput()

        #Singles Detector
        self.singlesDetectorOracle.setInput(self.S1Output, reset)
        self.SingDetOutput = self.singlesDetectorOracle.getPredictedOutput()
        
        #Stage1 module
        #for i in range(32):
            #print(i)
        #print(i, type(Delay_Value[i]), type(Delay_Value))
        self.mfDelayPEOracle.setInput(Port_Signal_In, Delay_Value, Coincidence_window, reset, Coincidence_type)
        self.S1Output = self.mfDelayPEOracle.getPredictedOutput()


        # # Pulse Extender, Check for updated coincidence window or mode
        # for i in range(32):
        #     self.peCombinedOracles[i].setInput(self.delayOutput[i], Coincidence_window, reset, Coincidence_type.value)
        #     self.peOutput[i] = self.peCombinedOracles[i].getPredictedOutput()
        
        # #Delay Module
        # for i in range(32):
        #     #Check for updated delay length
        #     self.delayOracles[i].setInput(self.mfOutput[i], int(str(Delay_Value[i]), 2), reset)
        #     self.delayOutput[i] = self.delayOracles[i].getPredictedOutput()
        #     # Delayoutputs.append(self.delayOutput[i])

        # #Multiflop
        # for i in range(32):
        #     self.multiFlopSynchroniserOracles[i].setInput(Signal_In[i])
        #     # self.multiFlopSynchroniserOracles[i].checkOutput(dut.syncOut.value)
        #     self.mfOutput[i] = self.multiFlopSynchroniserOracles[i].getPredictedOutput()


    def checkOutput(self, singleTriggerOut, multiplicityTriggerOut):
        
        if 1 in self.singlescounterOutput:
            assert singleTriggerOut == 1, f"Singles Outputs do not match: VHDL {singleTriggerOut} != Oracle High, on cycle number {self.clockcycle}"
            assert multiplicityTriggerOut == self.multdelayOutput, f"Multipplicity Outputs do not match: VHDL {multiplicityTriggerOut} != Oracle {self.multdelayOutput}, on cycle number {self.clockcycle}"
        else:
            assert singleTriggerOut == 0, f"Outputs do not match: VHDL {singleTriggerOut} != Oracle Low, on cycle number {self.clockcycle}"
            assert multiplicityTriggerOut == self.multdelayOutput, f"Multipplicity Outputs do not match: VHDL {multiplicityTriggerOut} != Oracle {self.multdelayOutput}, on cycle number {self.clockcycle}"



    def getPredictedOutput(self):
        
        singlesout = 0
        if 1 in self.singlescounterOutput:
            singlesout = 1

        return self.multdelayOutput, singlesout


        # for i in range(32):
            # print("|", self.peOutput[i], output[i], type(self.peOutput[i]), type(output[i]), int(output[i]))
            #VALUE = int(str(output[i]), 2)
            # assert self.peOutput[i] == int(str(output[i]), 2), f"Outputs are not the same: VHDL {output[i]} != Oracle {self.peOutput[i]}, on cycle number {self.clockcycle}"
        #print("Did this work?")


async def everyClock(dut):
    inputPortModuleOracle = InputPortModuleOracle(dut._log)
    
    while True:
        await FallingEdge(dut.CLK)
        inputPortModuleOracle.setInput(dut.Reset.value, dut.Port_Signal_In.value, dut.Delay_Value.value, dut.Coincidence_window.value, dut.Coincidence_type.value, dut.Multiplicity.value, dut.Scalar_Value.value)
        inputPortModuleOracle.checkOutput(dut.Single_Trigger_Out.value, dut.Mult_Trigger_Out.value) 
    