import random
from MultiFlopSynchroniserOracle import everyClock
import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge


async def generate_clock(dut):
    """Generate clock pulses."""
    while True:
            dut.CLK.value = 0
            await Timer(10, units="ns")
            dut.CLK.value = 1
            await Timer(10, units="ns")


		# asyncIn : IN STD_LOGIC;
		# syncOut : OUT STD_LOGIC := '0'

async def reset_dut(dut):
    """Resets all the values of the module before a test"""
    dut.asyncIn.value = 0
    await Timer(100, units="ns")

@cocotb.test()
async def test1(dut):

    await reset_dut(dut)

    dut.asyncIn.value = 0
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)
    await cocotb.start(everyClock(dut))
    

    await Timer(20, units="ns")
    dut.asyncIn.value = 1
    await Timer(20, units="ns")
    dut.asyncIn.value = 0
    await Timer(20, units="ns")
    dut.asyncIn.value = 1
    await Timer(20, units="ns")
    dut.asyncIn.value = 0
    await Timer(20, units="ns")
    dut.asyncIn.value = 1
    await Timer(20, units="ns")
    dut.asyncIn.value = 0
    await Timer(20, units="ns")
    dut.asyncIn.value = 1
    await Timer(20, units="ns")
    dut.asyncIn.value = 0
    await Timer(20, units="ns")
    dut.asyncIn.value = 1
    await Timer(20, units="ns")
    dut.asyncIn.value = 0
    await Timer(20, units="ns")
    dut.asyncIn.value = 1
    await Timer(20, units="ns")
    dut.asyncIn.value = 0
    await Timer(20, units="ns")
    dut.asyncIn.value = 1
    await Timer(20, units="ns")
    dut.asyncIn.value = 0
    await Timer(120, units="ns")

    


@cocotb.test()
async def test2(dut):

    await reset_dut(dut)

    dut.asyncIn.value = 0
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)
    await cocotb.start(everyClock(dut))
    

    await Timer(20, units="ns")
    dut.asyncIn.value = 1
    await Timer(40, units="ns")
    dut.asyncIn.value = 0
    await Timer(60, units="ns")
    dut.asyncIn.value = 1
    await Timer(80, units="ns")
    dut.asyncIn.value = 0
    await Timer(100, units="ns")
    dut.asyncIn.value = 1
    await Timer(120, units="ns")
    dut.asyncIn.value = 0
    await Timer(140, units="ns")
    dut.asyncIn.value = 1
    await Timer(160, units="ns")
    dut.asyncIn.value = 0
    await Timer(180, units="ns")
    dut.asyncIn.value = 1
    await Timer(200, units="ns")
    dut.asyncIn.value = 0
    await Timer(220, units="ns")
    dut.asyncIn.value = 1
    await Timer(240, units="ns")
    dut.asyncIn.value = 0
    await Timer(260, units="ns")
    dut.asyncIn.value = 1
    await Timer(280, units="ns")
    dut.asyncIn.value = 0
    await Timer(1000, units="ns")