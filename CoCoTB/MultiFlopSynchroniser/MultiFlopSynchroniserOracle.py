import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb.triggers import FallingEdge
from queue import Queue
from collections import deque

class MultiFlopSynchroniserOracle:
    def __init__(self, delayLength):
        self.inputs = deque(maxlen = (delayLength))
        self.inputs.extend([0]*(delayLength))
        self.clockCycle = 0
        
    def setInput(self, moduleInput):
        self.clockCycle += 1
        self.input = moduleInput
        self.inputs.append(moduleInput)

    def checkOutput(self, moduleOutput):
        self.output = moduleOutput
        if self.inputs[0] == 1:
             assert moduleOutput == 1, f"Failed to set output high: {moduleOutput} != 1, on cycle number {self.clockCycle}, the previous inputs were {self.inputs}"
        else:
            assert moduleOutput == 0, f"Outputs not low or undefined: {moduleOutput} != 0, on cycle number {self.clockCycle}, the previous inputs were {self.inputs}"

        return self.inputs[0]

    def getPredictedOutput(self):
        return self.inputs[0]

async def everyClock(dut):
    multiFlopSynchroniserOracle = MultiFlopSynchroniserOracle(3)

    while True:
        await FallingEdge(dut.CLK)
        multiFlopSynchroniserOracle.setInput(dut.asyncIn.value) 
        multiFlopSynchroniserOracle.checkOutput(dut.syncOut.value) 
     