from itertools import count
import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb.triggers import FallingEdge
from queue import Queue
from collections import deque
import sys

class SinglesCounterOracle:
    def __init__(self, outputDelay):
        #outputDelay is the delay time between the input and output states
        self.counters = deque(maxlen = (outputDelay))
        self.counters.extend([0]*(outputDelay)) #fill with the default value

        self.clockCycle = 0
        self.Singles_veto = 0
        self.Reset = 0
        self.Singles_in = 0

        self.counter = 0
        self.insidePulse = False


    def setInput(self, Singles_in, In1, In2, In3, In4, In5, In6, In7, In8, In9, In10, In11, In12, In13, In14, In15, In16, Singles_veto, Reset):
        self.Singles_in = Singles_in
        self.Singles_veto = Singles_veto
        self.Reset = Reset
        self.clockCycle += 1

        self.Singles_scalar = [In1, In2, In3, In4, In5, In6, In7, In8, In9, In10, In11, In12, In13, In14, In15, In16]
        scalarInStr = ''
        self.Singles_scalar.reverse()
        for i in self.Singles_scalar:
            scalarInStr = scalarInStr + str(i)
        self.singlesScalar = int('0b'+scalarInStr,2)    

        if Reset:
            self.counter = 0
            self.insidePulse = False
        elif self.Singles_veto and self.Singles_in and not self.insidePulse:
            self.counter += 1

        if self.Singles_in:
            if not Reset:
                self.insidePulse = True
        else:
            self.insidePulse = False


    def setOutput(self, moduleOutput):
        if self.Reset == 1:
            assert moduleOutput == 0, f"The output is wrongly high: {moduleOutput} != 0, on cycle number {self.clockCycle}"
        else: 
            if self.counter == self.singlesScalar and self.singlesScalar:
                self.counter = 0
                assert moduleOutput == 1, f"Failed to set outputs high: {moduleOutput} != 1, on cycle number {self.clockCycle}"
            else:
                assert moduleOutput == 0, f"The output is wrongly high: {moduleOutput} != 0, on cycle number {self.clockCycle}"


    def getPredictedOutput(self):
        if self.Reset == 1:
            return 0
        else: 
            if self.counter == self.singlesScalar and self.singlesScalar:
                self.counter = 0
                return 1
            else:
                return 0



async def everyClock(dut):
    singlesCounterOracle = SinglesCounterOracle(1)

    while True:
        await FallingEdge(dut.CLK_counter)
        singlesCounterOracle.setInput(dut.Singles_in.value, 
                                      dut.Singles_scalar[0].value, 
                                      dut.Singles_scalar[1].value, 
                                      dut.Singles_scalar[2].value, 
                                      dut.Singles_scalar[3].value, 
                                      dut.Singles_scalar[4].value, 
                                      dut.Singles_scalar[5].value, 
                                      dut.Singles_scalar[6].value, 
                                      dut.Singles_scalar[7].value, 
                                      dut.Singles_scalar[8].value, 
                                      dut.Singles_scalar[9].value, 
                                      dut.Singles_scalar[10].value, 
                                      dut.Singles_scalar[11].value, 
                                      dut.Singles_scalar[12].value, 
                                      dut.Singles_scalar[13].value, 
                                      dut.Singles_scalar[14].value, 
                                      dut.Singles_scalar[15].value, 
                                      dut.Singles_veto.value, 
                                      dut.Reset.value)
        singlesCounterOracle.setOutput(dut.Singles_out.value) 
        