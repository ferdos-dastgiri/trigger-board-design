import random
import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from SinglesCounterOracle import everyClock
from cocotb import simulator
from cocotb.handle import *
import sys

checkpoint_hier = []
checkpoints = {}

#this function must be called prior to creating any checkpoint
def get_checkpoint_hier(entity):
   for ii in entity._handle.iterate(simulator.OBJECTS):
      hdl = SimHandle(ii, entity._path + "." + entity._handle.get_name_string())
      if ((entity._handle.get_type() is simulator.MODULE) or
          (entity._handle.get_type() is simulator.NETARRAY) or
          (entity._handle.get_type() is simulator.GENARRAY)):
            get_checkpoint_hier(hdl)
      elif entity._handle.get_type() is simulator.REG:
            checkpoint_hier.append(hdl)

#returns a created checkpoint
def checkpoint():
    checkpoint_data = {}
    #print(len(checkpoint_hier))
    for reg in checkpoint_hier:
        checkpoint_data[reg] = reg.value
    return checkpoint_data
#restores a checkpoint
def restore(checkpoint_data):
    for reg in checkpoint_data:
        reg.setimmediatevalue(checkpoint_data[reg])

async def generate_clock(dut):
    """Generate clock pulses."""
    while True:
            dut.CLK_counter.value = 0
            await Timer(10, units="ns")
            dut.CLK_counter.value = 1
            await Timer(10, units="ns")

async def reset_dut(dut):
    """Resets all the values of the module before a test"""
    for i in range(16):
        dut.Singles_scalar[i].value = 0
    dut.Singles_in.value = 0
    dut.Reset.value = 1
    dut.Singles_veto.value = 0
    await Timer(100, units="ns")

async def reset_signal(dut):
    dut.Reset.value = 1
    dut.Singles_veto.value = 0
    for _ in range(5):
            dut.CLK_counter.value = 0
            await Timer(10, units="ns")
            dut.CLK_counter.value = 1
            await Timer(10, units="ns")
    dut.Reset.value = 0
    await Timer(20, units="ns")

@cocotb.test()
async def test_on(dut):

    """Basic test to see if the sub-mod goes high when scalar == 1"""

    await reset_dut(dut)
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK_counter)

    await cocotb.start(everyClock(dut))

    #Turn on the veto
    await Timer(20, units="ns")
    dut.Singles_veto.value = 1
    #set singles scalar to 1
    dut.Singles_scalar[0].value = 1
    dut.Singles_scalar[1].value = 0


    dut._log.info("Veto signal is %s", dut.Singles_veto.value)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Input signal is %s", dut.Singles_in.value)
    dut._log.info("Scalar value is %s", dut.Singles_scalar.value)
    # dut._log.info("Output signal is %s", dut.Singles_out.value)
    
    

    await Timer(60, units="ns")
    dut.Singles_in.value = 1
    # dut._log.info("Scalar value is %s", dut.Singles_scalar.value)

    await Timer(60, units="ns")
    dut._log.info("Veto signal is %s", dut.Singles_veto.value)
    dut._log.info("Input signal is %s", dut.Singles_in.value)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Singles_out.value)



@cocotb.test()
async def test_mode1(dut):

    """Basic test setting scalar to 3 and inputting 3 or more signals."""

    await reset_dut(dut)
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK_counter)
    
    await cocotb.start(everyClock(dut))


    #Turn on the veto
    dut.Singles_veto.value = 1
    #set singles scalar to 3
    dut.Singles_scalar[0].value = 1
    dut.Singles_scalar[1].value = 1

    dut._log.info("Scalar value is %s", dut.Singles_scalar.value)
    dut._log.info("Veto signal is %s", dut.Singles_veto.value)
    dut._log.info("Input signal is %s", dut.Singles_in.value)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Singles_out.value)

    await Timer(60, units="ns")
    dut.Singles_in.value = 0
    # dut._log.info("Input signal is %s", dut.Singles_in.value)
    
    await Timer(20, units="ns")
    dut.Singles_in.value = 1
    # dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(20, units="ns")
    dut.Singles_in.value = 0
    # dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(60, units="ns")
    dut.Singles_in.value = 1
    # dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(20, units="ns")
    dut.Singles_in.value = 0
    # dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(20, units="ns")
    dut.Singles_in.value = 1
    # dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(20, units="ns")
    dut.Singles_in.value = 0

    await Timer(40, units="ns")
    dut.Singles_in.value = 1
    # dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(40, units="ns")
    dut.Singles_in.value = 0
    # dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(40, units="ns")
    # dut._log.info("Scalar value is %s", dut.Singles_scalar.value)
    # dut._log.info("Reset signal is %s", dut.Reset.value)
    # dut._log.info("Veto signal is %s", dut.Singles_veto.value)
    # dut._log.info("Input signal is %s", dut.Singles_in.value)
    # dut._log.info("Output signal is %s", dut.Singles_out.value)
    


@cocotb.test()
async def test_mode2(dut):

    """Simple test that will have the veto off - output should be low no matter the scalar value"""

    await reset_dut(dut)
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK_counter)
    
    await cocotb.start(everyClock(dut))

    #Have no veto on
    dut.Singles_veto.value = 0
    #set singles scalar to 2
    dut.Singles_scalar[1].value = 1
    dut._log.info("Scalar value is %s", dut.Singles_scalar.value)

    await Timer(60, units="ns")
    dut.Singles_in.value = 0
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(60, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(20, units="ns")
    dut.Singles_in.value = 0
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(40, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(40, units="ns")
    dut._log.info("Scalar value is %s", dut.Singles_scalar.value)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Veto signal is %s", dut.Singles_veto.value)
    dut._log.info("Input signal is %s", dut.Singles_in.value)
    dut._log.info("Output signal is %s", dut.Singles_out.value)
    


@cocotb.test()
async def test_mode3(dut):

    """Testing with the maximum scalar value"""

    await reset_dut(dut)
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK_counter)
    
    await cocotb.start(everyClock(dut))

    #Have veto on
    await Timer(20, units="ns")
    dut.Singles_veto.value = 1
    #set singles scalar to max
    for i in range(16):
         dut.Singles_scalar[i].value = 1

    dut._log.info("Scalar value is %s", dut.Singles_scalar.value)
    
    await Timer(60, units="ns")
    dut.Singles_in.value = 0
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(20, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(40, units="ns")
    dut.Singles_in.value = 0
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(20, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(20, units="ns")
    dut.Singles_in.value = 0
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(20, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(20, units="ns")
    dut.Singles_in.value = 0
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(60, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Scalar value is %s", dut.Singles_scalar.value)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Veto signal is %s", dut.Singles_veto.value)
    dut._log.info("Input signal is %s", dut.Singles_in.value)
    dut._log.info("Output signal is %s", dut.Singles_out.value)

@cocotb.test()
async def test_mode4(dut):

    """Tests turning the veto on and off"""

    await reset_dut(dut)
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK_counter)
    
    await cocotb.start(everyClock(dut))

    await Timer(20, units="ns")
    dut.Singles_veto.value = 1
    #set singles scalar to 4
    dut.Singles_scalar[2].value = 1

    await Timer(40, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(20, units="ns")
    dut.Singles_in.value = 0
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(40, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(40, units="ns")
    dut.Singles_in.value = 0
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(40, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(20, units="ns")
    dut.Singles_in.value = 0
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(80, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(120, units="ns")
    dut.Singles_veto.value = 0
    dut.Singles_in.value = 1

    await Timer(40, units="ns")
    dut.Singles_veto.value = 1
    dut.Singles_in.value = 1

    await Timer(60, units="ns")
    dut.Reset.value = 1

    await Timer(20, units="ns")
    dut.Reset.value = 0

    await Timer(60, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Scalar value is %s", dut.Singles_scalar.value)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Veto signal is %s", dut.Singles_veto.value)
    dut._log.info("Input signal is %s", dut.Singles_in.value)
    dut._log.info("Output signal is %s", dut.Singles_out.value)



@cocotb.test()
async def test_mode5(dut):

    """Simiar to test_mode4 but this time the veto will deactivate as the scalar counter is reached"""

    await reset_dut(dut)
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK_counter)
    
    await cocotb.start(everyClock(dut))

    await Timer(20, units="ns")
    dut.Singles_veto.value = 1
    #set singles scalar to 4
    dut.Singles_scalar[2].value = 1

    await Timer(40, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(20, units="ns")
    dut.Singles_in.value = 0
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(40, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(40, units="ns")
    dut.Singles_in.value = 0
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(40, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(20, units="ns")
    dut.Singles_in.value = 0
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(80, units="ns")
    dut.Singles_in.value = 1
    dut.Singles_veto.value = 0
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(120, units="ns")
    dut.Singles_in.value = 0

    await Timer(40, units="ns")
    dut.Singles_veto.value = 1
    dut.Singles_in.value = 1

    await Timer(60, units="ns")
    dut.Reset.value = 1

    await Timer(120, units="ns")
    dut.Reset.value = 0

    await Timer(60, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Scalar value is %s", dut.Singles_scalar.value)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Veto signal is %s", dut.Singles_veto.value)
    dut._log.info("Input signal is %s", dut.Singles_in.value)
    dut._log.info("Output signal is %s", dut.Singles_out.value)


@cocotb.test()
async def test_mode6(dut):

    """Simple test to check the reset is working and changing the scalar value in between"""

    await reset_dut(dut)
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK_counter)
    
    await cocotb.start(everyClock(dut))

    await Timer(20, units="ns")
    dut.Singles_veto.value = 1
    #set singles scalar to 3
    dut.Singles_scalar[0].value = 1
    dut.Singles_scalar[1].value = 1

    await Timer(40, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(20, units="ns")
    dut.Singles_in.value = 0
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(40, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(40, units="ns")
    dut.Singles_in.value = 0
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(40, units="ns")
    dut.Reset.value = 1
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(20, units="ns")
    dut.Singles_scalar[0].value = 0
    dut._log.info("Scalar value is %s", dut.Singles_scalar.value)

    await Timer(20, units="ns")
    dut.Reset.value = 0
    dut.Singles_in.value = 1
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(80, units="ns")
    dut.Singles_in.value = 0
    dut._log.info("Input signal is %s", dut.Singles_in.value)

    await Timer(120, units="ns")
    dut.Singles_in.value = 1

    await Timer(60, units="ns")
    dut.Singles_in.value = 1
    dut._log.info("Scalar value is %s", dut.Singles_scalar.value)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Veto signal is %s", dut.Singles_veto.value)
    dut._log.info("Input signal is %s", dut.Singles_in.value)
    dut._log.info("Output signal is %s", dut.Singles_out.value)