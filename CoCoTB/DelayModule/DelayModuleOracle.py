from ast import Del
import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb.triggers import FallingEdge
from queue import Queue
from collections import deque

class DelayModuleOracle:
    def __init__(self, delay):
        self.delay_length = delay + 1   #delay + 2
        self.inputs = deque()
        for _ in range(self.delay_length):
            self.inputs.append(0)
        self.clockCycle = 0
        
    def setInput(self, moduleInput, delay_length, reset):
        self.reset = reset
        # print(type(delay_length))
        # print(delay_length)
        # print(type(self.delay_length))
        if reset == 1 or (delay_length + 1) != self.delay_length:
            # print("if opened!")
            self.inputs = deque()
            for _ in range(delay_length + 1):
                self.inputs.append(0)
            self.delay_length = delay_length + 1
        else:
            #print("Delay length is", delay_length)
            self.clockCycle += 1
            self.input = moduleInput
            self.inputs.append(moduleInput)


    def checkOutput(self, moduleOutput):
        self.output = moduleOutput
        outcome = self.inputs.popleft()
        if outcome == 1:
            if self.reset == 0:
                assert moduleOutput == 1, f"Failed to set outputs high: {moduleOutput} != 1, on cycle number {self.clockCycle}, the previous inputs were {self.inputs}"
            else:
                assert moduleOutput == 0, f"Outputs not low or undefined: {moduleOutput} != 0, on cycle number {self.clockCycle}, the previous inputs were {self.inputs}"
        else:
            assert moduleOutput == 0, f"Outputs not low or undefined: {moduleOutput} != 0, on cycle number {self.clockCycle}, the previous inputs were {self.inputs}"
        return outcome


    def getPredictedOutput(self):
        outcome = self.inputs.popleft()
        if outcome == 1:
            if self.reset == 0:
                return 1
            else:
                return 0
        else:
            return 0

    def getPredictedOutputSameState(self):
        outcome = self.inputs[0]
        if outcome == 1:
            if self.reset == 0:
                return 1
            else:
                return 0
        else:
            return 0



async def everyClock(dut):
    delayModuleOracle = DelayModuleOracle(dut.Delay_Value.value)

    while True:
        await FallingEdge(dut.CLK)
        delayModuleOracle.setInput(dut.Delay_Signal_In.value, dut.Delay_Value.value, dut.Reset.value) 
        delayModuleOracle.checkOutput(dut.Delay_Out.value) 
     