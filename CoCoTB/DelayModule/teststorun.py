import random
from xml.dom.pulldom import SAX2DOM
import cocotb
from DelayModuleOracle import everyClock
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb import simulator
from cocotb.handle import *

checkpoint_hier = []
checkpoints = {}
#this function must be called prior to creating any checkpoint
def get_checkpoint_hier(entity):
   for ii in entity._handle.iterate(simulator.OBJECTS):
      hdl = SimHandle(ii, entity._path + "." + entity._handle.get_name_string())
      if ((entity._handle.get_type() is simulator.MODULE) or
          (entity._handle.get_type() is simulator.NETARRAY) or
          (entity._handle.get_type() is simulator.GENARRAY)):
            get_checkpoint_hier(hdl)
      elif entity._handle.get_type() is simulator.REG:
            checkpoint_hier.append(hdl)
            
#returns a created checkpoint
def checkpoint():
    checkpoint_data = {}
    for reg in checkpoint_hier:
        checkpoint_data[reg] = reg.value
    return checkpoint_data

#restores a checkpoint
def restore(checkpoint_data):
    for reg in checkpoint_data:
        reg.setimmediatevalue(checkpoint_data[reg])

async def generate_clock(dut):
    """Generate clock pulses."""
    while True:
        dut.CLK.value = 0
        await Timer(10, units="ns")
        dut.CLK.value = 1
        await Timer(10, units="ns")

async def reset_inputs(dut):
    #set inputs to defaults (clean input state)
    dut.Delay_Signal_In.value = 0
    dut.Delay_Value.value = 0

async def reset_signal(dut):
    #run the reset on the dut
    dut.Reset.value = 1
    await Timer(40, units="ns")
    dut.Reset.value = 0
    await Timer(20, units="ns")


"""
Tests are located below here.
Print statements if needed:
dut._log.info("Input signal is %s", dut.Delay_Signal_In.value)
dut._log.info("Output signal is %s", dut.Delay_Out.value)
dut._log.info("Delay Length is %s", dut.Delay_Value.value)
"""

@cocotb.test()
async def test1_basic_set_checkpoint(dut):
    """
    Very basic test, turns off and on. This test is used to set the reset condition for future tests.
    """

    dut.Delay_Signal_In.value = 0
    dut.Delay_Value.value = 0
    dut.CLK.value = 0
    dut.Reset.value = 0

    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)
    await cocotb.start(everyClock(dut))
    await reset_signal(dut)
    await Timer(40, units="ns")

    
    
    dut.Delay_Value.value = 3
    await reset_signal(dut)
    
    await RisingEdge(dut.CLK)
    
   
    await Timer(80, units="ns")
    dut.Delay_Signal_In.value = 1

    await Timer(40, units="ns")
    dut.Delay_Signal_In.value = 0
    
    await Timer(200, units="ns")

@cocotb.test()
async def test2_increasing_the_delay_length(dut):

    """
    In this test the delay length is increased after being triggered
    """
    await reset_inputs(dut)
    await Timer(40, units="ns")
    #Reset everything
    await reset_inputs(dut)
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)
    await cocotb.start(everyClock(dut))
    await reset_signal(dut)
    await Timer(40, units="ns")

    

    #Set delay length to 3
    dut.Delay_Value.value = 3
    await reset_signal(dut) # need to reset for it to take effect
    await RisingEdge(dut.CLK)
    

    #set input high
    await Timer(20, units="ns")
    dut.Delay_Signal_In.value = 1
    
    #set input low
    await Timer(40, units="ns")
    dut.Delay_Signal_In.value = 0
    
    #Await the delay length before changing
    await Timer(60, units="ns")

    #change delay length to 5
    dut.Delay_Value.value = 5
    await reset_signal(dut) # need to reset for it to take effect
    await RisingEdge(dut.CLK)

    #set input high
    await Timer(20, units="ns")
    dut.Delay_Signal_In.value = 1
    
    #set input low
    await Timer(40, units="ns")
    dut.Delay_Signal_In.value = 0
    
    await Timer(200, units="ns")
    
    

@cocotb.test()
async def test3_shortening_the_delay_length(dut):

    """
    In this test the delay length is shortened after having being triggered
    """
    await reset_inputs(dut)
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)
    await cocotb.start(everyClock(dut))
    await reset_signal(dut)
    await Timer(40, units="ns")

    

    #Set a 'long' delay length
    dut.Delay_Value.value = 5
    await reset_signal(dut) # need to reset for it to take effect    
    
    #Set input to be high
    await Timer(20, units="ns")
    dut.Delay_Signal_In.value = 1
    
    #Set input to be low
    await Timer(40, units="ns")
    dut.Delay_Signal_In.value = 0
    
    #wait for pulse to occur
    await Timer(60, units="ns")

    #Shorten delay length
    dut.Delay_Value.value = 3
    await reset_signal(dut) # need to reset for it to take effect
    await RisingEdge(dut.CLK)

    #Set input to be high
    await Timer(20, units="ns")
    dut.Delay_Signal_In.value = 1
    
    #Set input to be low
    await Timer(40, units="ns")
    dut.Delay_Signal_In.value = 0
    
    await Timer(200, units="ns")


@cocotb.test()
async def test4_altered_delay_length_to_overlap_outputs(dut):

    """
    In this test, the delay module is altered partway 
    through a signal to deliberately cause an overlapped output signal 
    """
    await reset_inputs(dut)
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)
    await cocotb.start(everyClock(dut))
    #Reset everything
    await reset_signal(dut)
    await Timer(40, units="ns")

    
    #Set a 'long' delay length
    dut.Delay_Value.value = 10
    await reset_signal(dut) # need to reset for it to take effect    
    
    #Set input to be high
    await Timer(20, units="ns")
    dut.Delay_Signal_In.value = 1
    
    #Set input to be low
    await Timer(40, units="ns")
    dut.Delay_Signal_In.value = 0
    
    #Pulse would not have occured
    await Timer(60, units="ns")
  
    #Shorten delay length
    dut.Delay_Value.value = 3
    await reset_signal(dut) # need to reset for it to take effect
    await RisingEdge(dut.CLK)

    #Set input to be high
    await Timer(120, units="ns")
    dut.Delay_Signal_In.value = 1
    
    #Set input to be low
    await Timer(40, units="ns")
    dut.Delay_Signal_In.value = 0
    
    await Timer(220, units="ns")  
    await Timer(256*20, units="ns")  

#NOT A VALID TEST, it just gives a different signal to the oracle and dut
# @cocotb.test()
# async def test5_test_off_clock(dut):

#     """
#     As the delay module is the first module in on the board the
#      aim of this test is to see how well the module copes with 
#      signals arriving off clock cycle (Falling edges)
#     """
#     await Timer(40, units="ns")
#     await cocotb.start(generate_clock(dut))
#     await RisingEdge(dut.CLK)
#     await cocotb.start(everyClock(dut))
#     #Reset everything
#     await reset_signal(dut)
#     await Timer(40, units="ns")

    

#     dut.Delay_Value.value = 3
#     await reset_signal(dut) # need to reset for it to take effect

#     await Timer(30, units="ns")
#     dut.Delay_Signal_In.value = 1
    
#     #Signal length is 1 clock cycle but on the falling edge clock cycle 
#     await Timer(20, units="ns")
#     dut.Delay_Signal_In.value = 0
    
#     await Timer(200, units="ns")

#NOT A VALID TEST
# @cocotb.test()
# async def test6_short_pulses(dut):

#     """
#     As the delay module is the first module in on the board the
#      aim of this test is to see how well the module copes with 
#      short signals
#     """
#     await Timer(40, units="ns")
#     #Reset everything
#     await reset_signal(dut)
#     await Timer(40, units="ns")

#     await cocotb.start(generate_clock(dut))
#     await cocotb.start(everyClock(dut))

#     dut.Delay_Value.value = 3
#     await reset_signal(dut) # need to reset for it to take effect
#     await RisingEdge(dut.CLK)

#     await Timer(20, units="ns")
#     dut.Delay_Signal_In.value = 1
    
#     #Signal length is 1/2 clock cycle  
#     await Timer(10, units="ns")
#     dut.Delay_Signal_In.value = 0

#     await Timer(90, units="ns")
#     dut.Delay_Signal_In.value = 1
    
#     #Signal length is 1/4 clock cycle  
#     await Timer(5, units="ns")
#     dut.Delay_Signal_In.value = 0
    
    await Timer(200, units="ns")
    
    
@cocotb.test()
async def test7_long_pulses(dut):

    """
    As the delay module is the first module in on the board the
     aim of this test is to see how well the module copes with 
     very long signals
    """
    await reset_inputs(dut)
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)
    await cocotb.start(everyClock(dut))
    #Reset everything
    await reset_signal(dut)
    await Timer(40, units="ns")

    

    dut.Delay_Value.value = 3
    await reset_signal(dut) # need to reset for it to take effect

    await Timer(40, units="ns")
    dut.Delay_Signal_In.value = 1
    
    #Signal length is 5 clock cycle  
    await Timer(100, units="ns")
    dut.Delay_Signal_In.value = 0

    await Timer(200, units="ns")
    dut.Delay_Signal_In.value = 1
    
    #Signal length is 10 clock cycle  
    await Timer(200, units="ns")
    dut.Delay_Signal_In.value = 0
    
    await Timer(300, units="ns")


@cocotb.test()
async def test8_all_delays(dut):

    """
    Tests that the delay function works at all levels
    """
    await reset_inputs(dut)
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)
    await cocotb.start(everyClock(dut))
    #Reset everything
    await reset_signal(dut)
    await Timer(40, units="ns")

    for i in range(64):
        #Reset everything
        await reset_inputs(dut)
        await reset_signal(dut)
        
        #Define delay value
        dut.Delay_Value.value = (i)
        await reset_signal(dut) # need to reset for it to take effect
        await RisingEdge(dut.CLK)
        
        #Set signal high
        await Timer(40, units="ns")
        dut.Delay_Signal_In.value = 1
        
        #Signal length is 1 clock cycle  
        await Timer(20, units="ns")
        dut.Delay_Signal_In.value = 0
        
        #Wait for the expected delay signal
        await Timer((i*20)+20, units="ns")

@cocotb.test()
async def test9_all_RAM_use(dut):

    """
    Tests that the RAM function works at all levels
    """
    await reset_inputs(dut)
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await reset_signal(dut)
    await Timer(40, units="ns")
    await RisingEdge(dut.CLK)
    await cocotb.start(everyClock(dut))
    #Reset everything
    await reset_signal(dut)
    await Timer(40, units="ns")

    dut.Delay_Value.value = 63
    await reset_signal(dut) # need to reset for it to take effect
    await RisingEdge(dut.CLK)
    await Timer(30, units="ns")

    for i in range(256):
    
        await Timer(10, units="ns")
        dut.Delay_Signal_In.value = 1
        
        #Signal length is 1 clock cycle  
        await Timer(20, units="ns")
        dut.Delay_Signal_In.value = 0
        
        await Timer(10, units="ns")

    await Timer(300, units="ns")

@cocotb.test()
async def test10_reset_test(dut):

    """
    This test is designed to test if the reset goes high that the output should be low.
    """
    await reset_inputs(dut)
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await reset_signal(dut)
    await Timer(40, units="ns")
    await RisingEdge(dut.CLK)
    await cocotb.start(everyClock(dut))
    #Reset everything
    await reset_signal(dut)
    await Timer(40, units="ns")


    dut.Delay_Value.value = 3
    await reset_signal(dut) # need to reset for it to take effect
    await RisingEdge(dut.CLK)

    await Timer(20, units="ns")
    dut.Delay_Signal_In.value = 1
    
    #Signal length is 5 clock cycle  
    await Timer(100, units="ns")
    dut.Delay_Signal_In.value = 0

    await Timer(200, units="ns")
    dut.Delay_Signal_In.value = 1
    
    await Timer(100, units="ns")
    dut.Reset.value = 1

    await Timer(60, units="ns")
    dut.Reset.value = 0

    #Signal length is 10 clock cycle  
    await Timer(60, units="ns")
    dut.Delay_Signal_In.value = 0
    
    await Timer(300, units="ns")

@cocotb.test()
async def test11_overflow_test(dut):

    """
    This test is designed to test what happens if the RAM overflows
    """
    await reset_inputs(dut)
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await reset_signal(dut)
    await Timer(40, units="ns")
    await RisingEdge(dut.CLK)
    await cocotb.start(everyClock(dut))
    #Reset everything
    await reset_signal(dut)
    await Timer(40, units="ns")


    dut.Delay_Value.value = 63
    await reset_signal(dut) # need to reset for it to take effect
    await RisingEdge(dut.CLK)

    await Timer(20, units="ns")
    dut.Delay_Signal_In.value = 1
    
    
    
    await Timer(65*20, units="ns")