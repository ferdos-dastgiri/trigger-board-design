import random
import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from GlobalTriggerOracle import everyClock
from cocotb import simulator
from cocotb.handle import *
import sys

'''
Trigger Modes
    00 - Either port
    01 - Only port A
    10 - Only port B
    11 - Coincidence
'''

checkpoint_hier = []
checkpoints = {}

#this function must be called prior to creating any checkpoint
def get_checkpoint_hier(entity):
   for ii in entity._handle.iterate(simulator.OBJECTS):
      hdl = SimHandle(ii, entity._path + "." + entity._handle.get_name_string())
      if ((entity._handle.get_type() is simulator.MODULE) or
          (entity._handle.get_type() is simulator.NETARRAY) or
          (entity._handle.get_type() is simulator.GENARRAY)):
            get_checkpoint_hier(hdl)
      elif entity._handle.get_type() is simulator.REG:
            checkpoint_hier.append(hdl)

#returns a created checkpoint
def checkpoint():
    checkpoint_data = {}
    #print(len(checkpoint_hier))
    for reg in checkpoint_hier:
        checkpoint_data[reg] = reg.value
    return checkpoint_data

#restores a checkpoint
def restore(checkpoint_data):
    for reg in checkpoint_data:
        reg.setimmediatevalue(checkpoint_data[reg])

async def generate_clock(dut):
    """Generate clock pulses."""
    for cycle in range(20):
            dut.CLK_trigger.value = 0
            await Timer(10, units="ns")
            dut.CLK_trigger.value = 1
            await Timer(10, units="ns")

async def reset_dut(dut):
    """Resets all the values of the module before a test"""
    dut.A_Trigger_singles.value = 0
    dut.A_Trigger_mult.value = 0
    dut.B_Trigger_singles.value = 0
    dut.B_Trigger_mult.value = 0
    dut.Reset.value = 1
    for i in range(2):
        dut.Trigger_mode[i].value = 0
    dut.Trigger_out.value = 0
    await Timer(100, units="ns")

async def reset_signal(dut):
    dut.Reset.value = 1
    for _ in range(5):
            dut.CLK_trigger.value = 0
            await Timer(10, units="ns")
            dut.CLK_trigger.value = 1
            await Timer(10, units="ns")
    dut.Reset.value = 0
    await Timer(20, units="ns")

@cocotb.test()
async def test_on(dut):

    """Basic test, just to see if Trigger will fire with mode = 00"""

    await reset_dut(dut)
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK_trigger)

    await cocotb.start(everyClock(dut))
    

    #set initial inputs
    #Set trigger mode
    dut.Trigger_mode[0].value = 0
    dut.Trigger_mode[1].value = 0
    dut.Reset.value = 0 


    inputs = [dut.A_Trigger_singles.value, dut.A_Trigger_mult.value, dut.B_Trigger_singles.value, dut.B_Trigger_mult.value]
    dut._log.info("Input signal is %s", inputs)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Trigger_out.value)

    await Timer(40, units="ns")
    dut.A_Trigger_mult.value = 1
    
    await Timer(80, units="ns")
    inputs = [ dut.A_Trigger_singles.value, dut.A_Trigger_mult.value, dut.B_Trigger_singles.value, dut.B_Trigger_mult.value]
    dut._log.info("Input signal is %s", inputs)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Trigger_out.value)
    
@cocotb.test()
async def test_off(dut):

    """ Basic test to check that output does not become high"""

    await reset_dut(dut)
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK_trigger)

    await cocotb.start(everyClock(dut))

    inputs = [dut.A_Trigger_singles.value, dut.A_Trigger_mult.value, dut.B_Trigger_singles.value, dut.B_Trigger_mult.value]
    dut._log.info("Input signal is %s", inputs)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Trigger_out.value)
    dut.Reset.value = 0 

    #Set trigger mode
    await Timer(20, units="ns")
    dut.Trigger_mode[0].value = 0
    dut.Trigger_mode[1].value = 0

    await Timer(20, units="ns")
    dut.A_Trigger_mult.value = 0
    dut.B_Trigger_mult.value = 0
    dut.A_Trigger_singles.value = 0
    dut.B_Trigger_singles.value = 0
    
    await Timer(40, units="ns")
    dut.A_Trigger_mult.value = 0
    inputs = [ dut.A_Trigger_singles.value, dut.A_Trigger_mult.value, dut.B_Trigger_singles.value, dut.B_Trigger_mult.value]
    dut._log.info("Input signal is %s", inputs)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Trigger_out.value)



@cocotb.test()
async def test_mode1(dut):

    """Basic test to check that while trigger mode is 01 that only port A firing will cause a trigger out"""

    await reset_dut(dut)
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK_trigger)

    await cocotb.start(everyClock(dut))

    inputs = [dut.A_Trigger_singles.value, dut.A_Trigger_mult.value, dut.B_Trigger_singles.value, dut.B_Trigger_mult.value]
    dut._log.info("Input signal is %s", inputs)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Trigger_out.value)
    dut.Reset.value = 0 

    #Set trigger mode
    await Timer(20, units="ns")
    dut.Trigger_mode[0].value = 1
    dut.Trigger_mode[1].value = 0

    await Timer(40, units="ns")
    dut.B_Trigger_mult.value = 1
    
    await Timer(120, units="ns")
    dut.B_Trigger_mult.value = 1
    dut.Trigger_mode[0].value = 0
    dut.Trigger_mode[1].value = 1

    await Timer(40, units="ns")
    inputs = [ dut.A_Trigger_singles.value, dut.A_Trigger_mult.value, dut.B_Trigger_singles.value, dut.B_Trigger_mult.value]
    dut._log.info("Input signal is %s", inputs)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Trigger_out.value)


@cocotb.test()
async def test_mode2(dut):

    await reset_dut(dut)
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK_trigger)

    await cocotb.start(everyClock(dut))

    inputs = [dut.A_Trigger_singles.value, dut.A_Trigger_mult.value, dut.B_Trigger_singles.value, dut.B_Trigger_mult.value]
    dut._log.info("Input signal is %s", inputs)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Trigger_out.value)
    dut.Reset.value = 0 

    #Set trigger mode
    await Timer(20, units="ns")
    dut.Trigger_mode[0].value = 1
    dut.Trigger_mode[1].value = 1

    await Timer(40, units="ns")
    dut.B_Trigger_mult.value = 1

    await Timer(40, units="ns")
    dut.A_Trigger_mult.value = 1
    dut.B_Trigger_mult.value = 1
    
    await Timer(80, units="ns")
    dut.A_Trigger_mult.value = 1
    dut.B_Trigger_mult.value = 1

    await Timer(40, units="ns")
    inputs = [ dut.A_Trigger_singles.value, dut.A_Trigger_mult.value, dut.B_Trigger_singles.value, dut.B_Trigger_mult.value]
    dut._log.info("Input signal is %s", inputs)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Trigger_out.value)


@cocotb.test()
async def test_mode3(dut):

    await reset_dut(dut)
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK_trigger)

    await cocotb.start(everyClock(dut))

    inputs = [dut.A_Trigger_singles.value, dut.A_Trigger_mult.value, dut.B_Trigger_singles.value, dut.B_Trigger_mult.value]
    dut._log.info("Input signal is %s", inputs)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Trigger_out.value)
    dut.Reset.value = 0 

    #Set trigger mode
    await Timer(20, units="ns")
    dut.Trigger_mode[0].value = 1
    dut.Trigger_mode[1].value = 0

    await Timer(20, units="ns")
    dut.A_Trigger_singles.value = 1
    
    await Timer(80, units="ns")
    dut.B_Trigger_mult.value = 1
    dut.A_Trigger_mult.value = 0
    # dut.Trigger_mode[0].value = 1
    # dut.Trigger_mode[1].value = 0

    await Timer(40, units="ns")
    dut.B_Trigger_mult.value = 1

    await Timer(40, units="ns")
    dut.B_Trigger_mult.value = 0

    await Timer(40, units="ns")
    dut.A_Trigger_singles.value = 1

    await Timer(40, units="ns")
    dut.A_Trigger_singles.value = 0

    await Timer(40, units="ns")
    inputs = [ dut.A_Trigger_singles.value, dut.A_Trigger_mult.value, dut.B_Trigger_singles.value, dut.B_Trigger_mult.value]
    dut._log.info("Input signal is %s", inputs)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Trigger_out.value)

@cocotb.test()
async def test_mode4(dut):
    "checks the reset function works properly"

    await reset_dut(dut)
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK_trigger)

    await cocotb.start(everyClock(dut))

    inputs = [dut.A_Trigger_singles.value, dut.A_Trigger_mult.value, dut.B_Trigger_singles.value, dut.B_Trigger_mult.value]
    dut._log.info("Input signal is %s", inputs)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Trigger_out.value)
    dut.Reset.value = 0 

    #Set trigger mode
    await Timer(20, units="ns")
    dut.Trigger_mode[0].value = 0
    dut.Trigger_mode[1].value = 1

    await Timer(80, units="ns")
    dut.A_Trigger_singles.value = 1
    
    await Timer(80, units="ns")
    dut.B_Trigger_mult.value = 1
    dut.A_Trigger_mult.value = 0

    await Timer(40, units="ns")
    dut.B_Trigger_mult.value = 1
    dut.Reset.value = 1 

    await Timer(80, units="ns")
    # dut.B_Trigger_mult.value = 1
    dut.Reset.value = 0

    await Timer(80, units="ns")
    inputs = [ dut.A_Trigger_singles.value, dut.A_Trigger_mult.value, dut.B_Trigger_singles.value, dut.B_Trigger_mult.value]
    dut._log.info("Input signal is %s", inputs)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Trigger_out.value)