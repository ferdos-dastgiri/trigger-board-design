import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb.triggers import FallingEdge
from queue import Queue
from collections import deque

class GlobalTriggerOracle:
    def __init__(self):
        self.clockCycle = 0
        self.Reset = 0
        self.A_Trigger_singles = 0
        self.A_Trigger_mult = 0
        self.B_Trigger_singles = 0
        self.B_Trigger_mult = 0
        self.Trigger_mode = []

    def setInput(self, A_Trigger_singles, A_Trigger_mult, B_Trigger_singles, B_Trigger_mult, Trigger_modeLower, Trigger_modeUpper, Reset):
        self.A_Trigger_singles = A_Trigger_singles
        self.A_Trigger_mult = A_Trigger_mult
        self.B_Trigger_singles = B_Trigger_singles
        self.B_Trigger_mult = B_Trigger_mult
        self.Trigger_mode = [Trigger_modeLower, Trigger_modeUpper]
        self.Reset = Reset
        self.clockCycle += 1

    def checkOutput(self, moduleOutput):
        self.output = moduleOutput
        if self.Reset == 1:
            assert moduleOutput == 0, f"Output should be zero and isn't: {moduleOutput} != 0, on cycle number {self.clockCycle}, the input mode is {self.Trigger_mode}"
        else:
            print(self.Trigger_mode)
            if self.Trigger_mode == [0,0]:
                if (self.A_Trigger_singles == 1) or (self.A_Trigger_mult == 1) or (self.B_Trigger_singles == 1) or (self.B_Trigger_mult == 1):
                    assert moduleOutput == 1, f"Output is wrong: {moduleOutput} != 1, on cycle number {self.clockCycle}"
                else:
                    assert moduleOutput == 0, f"Output should be zero and isn't: {moduleOutput} != 0, on cycle number {self.clockCycle}, the input mode is {self.Trigger_mode}"

            if self.Trigger_mode == [1,0]:
                if (self.A_Trigger_singles == 1) or (self.A_Trigger_mult == 1):
                    assert moduleOutput == 1, f"Output is wrong, should be triggering on crystals: {moduleOutput} != 1, on cycle number {self.clockCycle}"
                else:
                    assert moduleOutput == 0, f"Output should be zero and isn't: {moduleOutput} != 0, on cycle number {self.clockCycle}, the input mode is {self.Trigger_mode}"

            if self.Trigger_mode == [0,1]:
                
                if (self.B_Trigger_singles == 1) or (self.B_Trigger_mult == 1):
                    assert moduleOutput == 1, f"Output is wrong: {moduleOutput} != 1, on cycle number {self.clockCycle}"
                else:
                    assert moduleOutput == 0, f"Output should be zero and isn't: {moduleOutput} != 0, on cycle number {self.clockCycle}, the input mode is {self.Trigger_mode}"
            
            if self.Trigger_mode == [1,1]:
                if (self.A_Trigger_mult == 1) and (self.B_Trigger_mult == 1):
                    assert moduleOutput == 1, f"Output is wrong: {moduleOutput} != 1, on cycle number {self.clockCycle}"
                else:
                    assert moduleOutput == 0, f"Output should be zero and isn't: {moduleOutput} != 0, on cycle number {self.clockCycle}, the input mode is {self.Trigger_mode}"


    def getPredictedOutput(self):
        if self.Reset == 1:
            return 0
        else:
            print(self.Trigger_mode)
            if self.Trigger_mode == [0,0]:
                if (self.A_Trigger_singles == 1) or (self.A_Trigger_mult == 1) or (self.B_Trigger_singles == 1) or (self.B_Trigger_mult == 1):
                    return 1
                else:
                    return 0

            if self.Trigger_mode == [1,0]:
                if (self.A_Trigger_singles == 1) or (self.A_Trigger_mult == 1):
                    return 1
                else:
                    return 0

            if self.Trigger_mode == [0,1]:
                
                if (self.B_Trigger_singles == 1) or (self.B_Trigger_mult == 1):
                    return 1
                else:
                    return 0
                    
            if self.Trigger_mode == [1,1]:
                if (self.A_Trigger_mult == 1) and (self.B_Trigger_mult == 1):
                    return 1
                else:
                    return 0


async def everyClock(dut):
    globalTriggerOracle = GlobalTriggerOracle()
    while True:
        await FallingEdge(dut.CLK_trigger)
        #do the check first to gain a 1 clock cycle delay between input and output
        globalTriggerOracle.setInput(dut.A_Trigger_singles.value, dut.A_Trigger_mult.value, dut.B_Trigger_singles.value, dut.B_Trigger_mult.value, dut.Trigger_mode[0].value, dut.Trigger_mode[1].value, dut.Reset.value) 
        globalTriggerOracle.checkOutput(dut.Trigger_out.value)
