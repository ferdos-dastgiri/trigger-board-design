
import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb.triggers import FallingEdge
from queue import Queue
from collections import deque
import sys
import os
import importlib

sys.path.insert(0,'../PulseExtenderInputState/')
from PulseExtenderInputStateOracle import PulseExtenderInputStateOracle

sys.path.insert(0,'../PulseExtenderRisingEdge/')
from PulseExtenderRisingEdge import PulseExtenderRisingEdgeOracle



"""
Oracle for the combined pulse extender module.
Will continue to populate both input memory states each clock cycle
 and only check the output against the correct mode selection.
 Output signal is delayed by one clock cycle, hence implementation of a previous input
"""

class PulseExtenderCombined:
    def __init__(self, Mode, Window):
        self.pulseExtenderInputStateOracle = PulseExtenderInputStateOracle(Window)
        self.pulseExtenderRisingEdgeOracle = PulseExtenderRisingEdgeOracle(Window)
        if Mode == 1:
            self.currentExtender = self.pulseExtenderRisingEdgeOracle
        else:
            self.currentExtender = self.pulseExtenderInputStateOracle


    def setInput(self, moduleInput, coincidence, reset, Mode):
        if Mode == 1:
            self.currentExtender = self.pulseExtenderRisingEdgeOracle
        else:
            self.currentExtender = self.pulseExtenderInputStateOracle
        self.pulseExtenderInputStateOracle.setInput(moduleInput, coincidence, reset)
        self.pulseExtenderRisingEdgeOracle.setInput(moduleInput, coincidence, reset)


    def checkOutput(self, moduleOutput):
        self.currentExtender.checkOutput(moduleOutput)


    def getPredictedOutput(self):
        return self.currentExtender.getPredictedOutput()


async def everyClock(dut):
    pulseExtenderCombined = PulseExtenderCombined(dut.Pulse_Select_Mode.value, dut.Coincidence_Window.value)

    while True:
        await FallingEdge(dut.CLK)
        pulseExtenderCombined.setInput(dut.Pulse_Extend_Signal_In.value, dut.Coincidence_Window.value, dut.Reset.value, dut.Pulse_Select_Mode.value) 
        pulseExtenderCombined.checkOutput(dut.Pulse_Extend_Out.value) 
