import random
from PulseExtenderCombinedOracle import everyClock
import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb import simulator
from cocotb.handle import *

checkpoint_hier = []
checkpoints = {}
#this function must be called prior to creating any checkpoint
def get_checkpoint_hier(entity):
   for ii in entity._handle.iterate(simulator.OBJECTS):
      hdl = SimHandle(ii, entity._path + "." + entity._handle.get_name_string())
      if ((entity._handle.get_type() is simulator.MODULE) or
          (entity._handle.get_type() is simulator.NETARRAY) or
          (entity._handle.get_type() is simulator.GENARRAY)):
            get_checkpoint_hier(hdl)
      elif entity._handle.get_type() is simulator.REG:
            checkpoint_hier.append(hdl)
            
#returns a created checkpoint
def checkpoint():
    checkpoint_data = {}
    #print(len(checkpoint_hier))
    for reg in checkpoint_hier:
        checkpoint_data[reg] = reg.value
    return checkpoint_data
#restores a checkpoint
def restore(checkpoint_data):
    for reg in checkpoint_data:
        reg.setimmediatevalue(checkpoint_data[reg])

async def generate_clock(dut):
    """Generate clock pulses."""
    for cycle in range(200000):
            dut.CLK.value = 0
            await Timer(10, units="ns")
            dut.CLK.value = 1
            await Timer(10, units="ns")

async def reset_dut(dut):
    """Resets all the values of the module before a test"""
    dut.Pulse_Extend_Signal_In.value = 0
    for i in range(8):
        dut.Coincidence_Window[i].value = 0
    dut.Pulse_Extend_Out.value = 0
    #for i in range(8):
    #    dut.Counter_PE[i].value = 0
    dut.CLK.value = 0
    dut.Pulse_Select_Mode.value = 0
    dut.inputStateOutput.value = 0
    dut.risingEdgeOutput.value = 0
    await Timer(100, units="ns")

async def reset_signal(dut):
    dut.Reset.value = 1
    for _ in range(5):
            dut.CLK.value = 0
            await Timer(10, units="ns")
            dut.CLK.value = 1
            await Timer(10, units="ns")
    dut.Reset.value = 0
    await Timer(20, units="ns")

"""
TESTS ARE BELOW HERE
debugging is dangerous to go alone, take this!
dut._log.info("Input signal is %s", dut.Pulse_Extend_Signal_In.value)
dut._log.info("Output signal is %s", dut.Pulse_Extend_Out.value)
dut._log.info("Coincidence window is %s", dut.Coincidence_Window.value)
"""

@cocotb.test()
async def test1_basic_setcheckpoint(dut):

    """
    This basic test is used to set the checkpoints to which the 
    module will return to at the beginning of each future test
    """
    await Timer(40, units="ns")
    await reset_dut(dut)
    #Set up checkpoint
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)
    await cocotb.start(everyClock(dut))

    #Set coincidence window
    await Timer(20, units="ns")
    coincidence = 3
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)

    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1
    
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0
    
    #Reset space for new test
    await Timer(100, units="ns")
    


@cocotb.test()
async def test2_basic_long_pulse(dut):
    """
    This is a basic test to check pulse extension with a pulse longer than the pulse extender
    """
    await Timer(40, units="ns")
    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    #Set coincidence window
    await Timer(20, units="ns")
    coincidence = 3
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)

    #Test pulse
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1
    await Timer(100, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0
    
    #Reset time
    await Timer(160, units="ns")
    

@cocotb.test()
async def test3_multiple_peaks(dut):
    """
    This is a test to check what happens when the module is retriggered in the pulse extender window
    """
    await Timer(40, units="ns")
    await reset_dut(dut)
    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))


    #Set coincidence window
    await Timer(20, units="ns")
    coincidence = 3
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)

    #First pulse
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0
    
    #Second Pulse
    await Timer(40, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0
    
    #reset for next test
    await Timer(200, units="ns")

@cocotb.test()
async def test4_increase_extension(dut):
    """
    This is a basic test to check changing the coincidence window on the fly
    """
    await Timer(40, units="ns")
    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    #Set coincidence window
    await Timer(20, units="ns")
    coincidence = 3
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)

    #Test pulse 1
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0

    #Set new coincidence window
    await Timer(20, units="ns")
    coincidence = 5
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)

    #Test pulse 2
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0
    
    #Reset time
    await Timer(200, units="ns")

@cocotb.test()
async def test5_decrease_extension(dut):
    """
    This is a basic test to check changing the coincidence window on the fly
    """
    await Timer(40, units="ns")
    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    #Set coincidence window
    await Timer(20, units="ns")
    coincidence = 5
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)

    #Test pulse 1
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0

    #Set new coincidence window
    await Timer(20, units="ns")
    coincidence = 3
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)

    #Test pulse 2
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0
    
    #Reset time
    await Timer(200, units="ns")

@cocotb.test()
async def test6_all_extensions(dut):
    """
    This tests all of the available extension to the pulse extender
    """
    await Timer(40, units="ns")
    #await reset_dut(dut)
    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await Timer(40, units="ns")
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    for i in range(255):
        #Set coincidence window
        await Timer(20, units="ns")
        coincidence = i
        coinwind = format(coincidence,'08b')
        dut.Coincidence_Window.value = int(coinwind, base=2)

        #Test pulse
        await Timer(20, units="ns")
        dut.Pulse_Extend_Signal_In.value = 1
        await Timer(20, units="ns")
        dut.Pulse_Extend_Signal_In.value = 0
        await Timer(20*i+40, units="ns")
        
    #Reset time
    await Timer(100, units="ns")

@cocotb.test()
async def test7_reset_test(dut):
    """
    This is a basic test to check pulse extension with a pulse longer than the pulse extender
    """
    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)
    dut.Pulse_Select_Mode.value = 0
    await cocotb.start(everyClock(dut))

    #Set coincidence window
    await Timer(20, units="ns")
    coincidence = 3
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)

    #Test pulse
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1

    await Timer(60, units="ns")
    dut.Reset.value = 1

    await Timer(60, units="ns")
    dut.Reset.value = 0

    await Timer(60, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0
    
    #Reset time
    await Timer(160, units="ns")


@cocotb.test()
async def test8_basic_long_pulse(dut):
    """
    This is a basic test to check pulse extension with a pulse longer than the pulse extender
    """
    await Timer(40, units="ns")
    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await Timer(40, units="ns")
    dut.Pulse_Select_Mode.value = 1
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    #Set coincidence window
    await Timer(20, units="ns")
    coincidence = 3
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)

    #Test pulse
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1
    await Timer(100, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0
    
    #Reset time
    await Timer(160, units="ns")
    

@cocotb.test()
async def test9_multiple_peaks(dut):
    """
    This is a test to check what happens when the module is retriggered in the pulse extender window
    """
    await Timer(40, units="ns")
    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await Timer(40, units="ns")
    dut.Pulse_Select_Mode.value = 1
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))


    #Set coincidence window
    await Timer(20, units="ns")
    coincidence = 3
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)

    #First pulse
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0
    
    #Second Pulse
    await Timer(40, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0
    
    #reset for next test
    await Timer(200, units="ns")

@cocotb.test()
async def test10_increase_extension(dut):
    """
    This is a basic test to check changing the coincidence window on the fly
    """
    await Timer(40, units="ns")
    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await Timer(40, units="ns")
    dut.Pulse_Select_Mode.value = 1
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    #Set coincidence window
    await Timer(20, units="ns")
    coincidence = 3
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)

    #Test pulse 1
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0

    #Set new coincidence window
    await Timer(20, units="ns")
    coincidence = 5
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)

    #Test pulse 2
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0
    
    #Reset time
    await Timer(200, units="ns")

@cocotb.test()
async def test11_decrease_extension(dut):
    """
    This is a basic test to check changing the coincidence window on the fly
    """
    await Timer(40, units="ns")
    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await Timer(40, units="ns")
    dut.Pulse_Select_Mode.value = 1
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    #Set coincidence window
    await Timer(20, units="ns")
    coincidence = 5
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)

    #Test pulse 1
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0

    #Set new coincidence window
    await Timer(20, units="ns")
    coincidence = 3
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)

    #Test pulse 2
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0
    
    #Reset time
    await Timer(200, units="ns")

@cocotb.test()
async def test12_all_extensions(dut):
    """
    This tests all of the available extension to the pulse extender
    """
    await Timer(40, units="ns")
    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await Timer(40, units="ns")
    dut.Pulse_Select_Mode.value = 1
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    for i in range(255):
        #Set coincidence window
        await Timer(20, units="ns")
        coincidence = i
        coinwind = format(coincidence,'08b')
        dut.Coincidence_Window.value = int(coinwind, base=2)

        #Test pulse
        await Timer(20, units="ns")
        dut.Pulse_Extend_Signal_In.value = 1
        await Timer(20, units="ns")
        dut.Pulse_Extend_Signal_In.value = 0
        await Timer(20*i+40, units="ns")
        
    #Reset time
    await Timer(100, units="ns")

@cocotb.test()
async def test13_reset_test(dut):
    """
    This is a basic test to check pulse extension with a pulse longer than the pulse extender
    """
    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)
    dut.Pulse_Select_Mode.value = 1
    await cocotb.start(everyClock(dut))

    #Set coincidence window
    await Timer(20, units="ns")
    coincidence = 3
    coinwind = format(coincidence,'08b')
    dut.Coincidence_Window.value = int(coinwind, base=2)

    #Test pulse
    await Timer(20, units="ns")
    dut.Pulse_Extend_Signal_In.value = 1

    await Timer(60, units="ns")
    dut.Reset.value = 1

    await Timer(60, units="ns")
    dut.Reset.value = 0

    await Timer(60, units="ns")
    dut.Pulse_Extend_Signal_In.value = 0
    
    #Reset time
    await Timer(160, units="ns")
    
