import random
import cocotb
from MultiplicityTriggerOracle import everyClock
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb import simulator
from cocotb.handle import *

checkpoint_hier = []
checkpoints = {}
#this function must be called prior to creating any checkpoint
def get_checkpoint_hier(entity):
   for ii in entity._handle.iterate(simulator.OBJECTS):
      hdl = SimHandle(ii, entity._path + "." + entity._handle.get_name_string())
      if ((entity._handle.get_type() is simulator.MODULE) or
          (entity._handle.get_type() is simulator.NETARRAY) or
          (entity._handle.get_type() is simulator.GENARRAY)):
            get_checkpoint_hier(hdl)
      elif entity._handle.get_type() is simulator.REG:
            checkpoint_hier.append(hdl)
            
#returns a created checkpoint
def checkpoint():
    checkpoint_data = {}
    #print(len(checkpoint_hier))
    for reg in checkpoint_hier:
        checkpoint_data[reg] = reg.value
    return checkpoint_data
#restores a checkpoint
def restore(checkpoint_data):
    for reg in checkpoint_data:
        reg.setimmediatevalue(checkpoint_data[reg])

async def generate_clock(dut):
    """Generate clock pulses."""
    for cycle in range(2000):
            dut.CLK.value = 0
            await Timer(10, units="ns")
            dut.CLK.value = 1
            await Timer(10, units="ns")

async def reset_dut(dut):
    """Resets all the values of the module before a test"""
    for i in range(32):
        dut.Multiplicity_Signal_in[i].value = 0
    for i in range(8):
        dut.Multiplicity[i].value = 1
    dut.MultiplicityTrigger_Out.value = 0
    await Timer(100, units="ns")

async def reset_signal(dut):
    dut.Reset.value = 1
    for _ in range(5):
            dut.CLK.value = 0
            await Timer(10, units="ns")
            dut.CLK.value = 1
            await Timer(10, units="ns")
    dut.Reset.value = 0
    await Timer(20, units="ns")

"""
Tests are located below here;
Print statements for debugging are here if you need.
dut._log.info("Input signal is %s", dut.Multiplicity_Signal_in.value)
dut._log.info("Output signal is %s", dut.MultiplicityTrigger_Out.value)
dut._log.info("Multiplicity is set to %s", dut.Multiplicity.value)
"""

@cocotb.test()
async def test1_Basic_checkpointsetup(dut):

    """
    This basic test is used to set the checkpoints to which the 
    module will return to at the beginning of each future test
    """
    await reset_dut(dut)
    #Set up checkpoint
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)
    await cocotb.start(reset_dut(dut))
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))
  
    #Set Multiplicity
    await Timer(20, units="ns")
    mult = [1,0,0,0,0,0,0,0]
    for i in range(8):
        dut.Multiplicity[i].value = mult[i]
    
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[1].value = 1
    
    await Timer(40, units="ns")
    dut.Multiplicity_Signal_in[1].value = 0

    await Timer(100, units="ns")


@cocotb.test()
async def test2_basic_mult2(dut):
    """
    This is a basic test to check setting of multiplicity 
    to 2 and triggering
    """

    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    #Set Multiplicity
    await Timer(20, units="ns")
    multiplicity = 2
    mult = format(multiplicity,'08b')
    print(type(mult))
    dut.Multiplicity.value = int(mult, base=2)
    
    await RisingEdge(dut.CLK)
    
    #Set value high -should not trigger
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[1].value = 1
    await Timer(40, units="ns")
    dut.Multiplicity_Signal_in[1].value = 0

    #Set 2 values high -should trigger
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[1].value = 1
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[2].value = 1
    await Timer(40, units="ns")

    #Return to zero and reset
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[2].value = 0
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[1].value = 0
    await Timer(100, units="ns")

@cocotb.test()
async def test3_change_mult2(dut):
    """
    This test initially has the module set to multiplicity of 2,
     after triggering it changes the multiplicity to 3, 
     attempts a two input and eventually has three simultaneous inputs
    """

    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    #Set Multiplicity
    await Timer(20, units="ns")
    multiplicity = 2
    mult = format(multiplicity,'08b')
    dut.Multiplicity.value = int(mult, base=2)
    
    await RisingEdge(dut.CLK)
    
    #Set value high -should not trigger
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[1].value = 1
    await Timer(40, units="ns")
    dut.Multiplicity_Signal_in[1].value = 0

    #Set 2 values high -should trigger
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[1].value = 1
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[2].value = 1
    await Timer(40, units="ns")

    #Return to zero
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[2].value = 0
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[1].value = 0

    #Set Multiplicity
    await Timer(20, units="ns")
    multiplicity = 3
    mult = format(multiplicity,'08b')
    dut.Multiplicity.value = int(mult, base=2)

    #Set 2 values high -should not trigger
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[1].value = 1
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[2].value = 1
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[3].value = 1
    await Timer(40, units="ns")

    #Return to zero
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[2].value = 0
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[1].value = 0
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[3].value = 0

    await Timer(100, units="ns")

@cocotb.test()
async def test4_all_mult(dut):

    """
    This is a long test. It loops through 32 multiplicities and tests then all with gradually up to 32 inputs.
    
    """

    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    for i in range(32):
        #Set Multiplicity
        await Timer(20, units="ns")
        multiplicity = i+1
        mult = format(multiplicity,'08b')
        dut.Multiplicity.value = int(mult, base=2)
        #dut._log.info("Multiplicity is set to %s", dut.Multiplicity.value)
        
        await RisingEdge(dut.CLK)
        for j in range(32):
            #Set value high
            await Timer(20, units="ns")
            dut.Multiplicity_Signal_in[j].value = 1
            
        await Timer(60, units="ns")

        for k in range(32):
            dut.Multiplicity_Signal_in[k].value = 0
        await Timer(40, units="ns")
    await Timer(100, units="ns")

@cocotb.test()
async def test5_all_mult_off_clock(dut):

    """
    This is the same test as the previous but condcted on the falling edges
    """

    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))
    await Timer(10, units="ns")
    for i in range(32):
        #Set Multiplicity
        await Timer(20, units="ns")
        multiplicity = i+1
        mult = format(multiplicity,'08b')
        dut.Multiplicity.value = int(mult, base=2)
        #dut._log.info("Multiplicity is set to %s", dut.Multiplicity.value)
        
        await RisingEdge(dut.CLK)
        for j in range(32):
            #Set value high
            await Timer(20, units="ns")
            dut.Multiplicity_Signal_in[j].value = 1
            
        await Timer(60, units="ns")

        for k in range(32):
            dut.Multiplicity_Signal_in[k].value = 0
        await Timer(40, units="ns")
    await Timer(100, units="ns")



@cocotb.test()
async def test6_reset_test(dut):
    """
    This is a basic test to check reset function
    """

    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    #Set Multiplicity
    await Timer(20, units="ns")
    multiplicity = 2
    mult = format(multiplicity,'08b')
    print(type(mult))
    dut.Multiplicity.value = int(mult, base=2)
    
    await RisingEdge(dut.CLK)
    
    #Set value high -should not trigger
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[1].value = 1
    await Timer(40, units="ns")
    dut.Multiplicity_Signal_in[1].value = 0

    #Set 2 values high -should trigger
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[1].value = 1
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[2].value = 1
    await Timer(40, units="ns")

    #Set reset to high, should make output low
    await Timer(60, units="ns")
    dut.Reset.value = 1
    await Timer(60, units="ns")
    dut.Reset.value = 0

    #Return to zero and reset
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[2].value = 0
    await Timer(20, units="ns")
    dut.Multiplicity_Signal_in[1].value = 0
    await Timer(100, units="ns")



