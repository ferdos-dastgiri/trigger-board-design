import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb.triggers import FallingEdge
from queue import Queue
from collections import deque

class MultiplicityTriggerOracle:
    def __init__(self):
        self.inputs = []
        for i in range(32):
            self.inputs.append(0)
        self.clockCycle = 0
        
    def setInput(self, chan, moduleInput, Multiplicity, Reset):
        self.inputs[chan] = (moduleInput)
        self.reset = Reset
        
        sum = 0
        for i in range(32):
            sum += self.inputs[i]
        self.sum = sum

        self.multiplicity = int(Multiplicity)
        

    def checkOutput(self, moduleOutput):
        self.clockCycle += 1
        self.output = moduleOutput
        
        if self.sum >= self.multiplicity:
            if self.reset == 0:
                assert moduleOutput == 1, f"Failed to set outputs high: {moduleOutput} != 1, on cycle number {self.clockCycle}, the previous inputs were {self.inputs}"
            else:
                assert moduleOutput == 0, f"Outputs not low or undefined: {moduleOutput} != 0, on cycle number {self.clockCycle}, the previous inputs were {self.inputs}"
        else:
            assert moduleOutput == 0, f"Outputs not low or undefined: {moduleOutput} != 0, on cycle number {self.clockCycle}, the previous inputs were {self.inputs}"


    def getPredictedOutput(self):
        self.clockCycle += 1
        
        if self.sum >= self.multiplicity:
            if self.reset == 0:
                return 1
            else:
                return 0
        else:
            return 0


async def everyClock(dut):
    multiplicityTriggerOracle = MultiplicityTriggerOracle()

    while True:
        await FallingEdge(dut.CLK)
        for i in range(32):
            multiplicityTriggerOracle.setInput(i, dut.Multiplicity_Signal_in[i].value, dut.Multiplicity.value, dut.Reset.value) 
        
        multiplicityTriggerOracle.checkOutput(dut.MultiplicityTrigger_Out.value) 
     