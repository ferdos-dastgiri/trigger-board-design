from ast import Del
import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb.triggers import FallingEdge
from queue import Queue
from collections import deque

import os
import sys

sys.path.insert(0,'../InputPortModule/')
from InputPortModuleOracle import InputPortModuleOracle

sys.path.insert(0,'../InputPortModule/')
from InputPortModuleOracle import InputPortModuleOracle

sys.path.insert(0,'../GlobalTrigger/')
from GlobalTriggerOracle import GlobalTriggerOracle

sys.path.insert(0,'../GlobalTrigger/')
from GlobalTriggerOracle import GlobalTriggerOracle


sys.path.insert(0,'../StartModule/')
from StartModuleOracle import StartModuleOracle

sys.path.insert(0,'../ResetModule/')
from StartModuleOracle import ResetModuleOracle


class SABRETriggerOracle:
    def __init__(self, logger):
        self.logger = logger
        self.clockcycle = 0

        self.StartModuleOracle = StartModuleOracle()
        self.ResetModuleOracle = ResetModuleOracle()
        self.InputPortModuleOracle = InputPortModuleOracle(1)
        self.GlobalTriggerOracle = GlobalTriggerOracle(1)
        


    def setInput(self, Port_Signal_In_A, Port_Signal_In_B, Delay_Value_A, Delay_Value_B, Coincidence_window_A, Coincidence_window_B, Coincidence_type_A, Coincidence_type_B, 
    Multiplicity_A, Multiplicity_B, Scalar_Value_A, Scalar_Value_B, Trigger_Mode, User_Start, Reset_In):
        self.clockcycle += 1
        self.reset = Reset_In
    
    def checkOutput(self, Trigger_Out_A, Trigger_Out_B):
        ...

async def everyClock(dut):
    SABRETriggerOracle = SABRETriggerOracle(dut._log)

    while True:
        await FallingEdge(dut.CLK)
        SABRETriggerOracle.setInput(dut.Port_Signal_In_A.value, dut.Port_Signal_In_B.value, dut.Delay_Value_A.value, dut.Delay_Value_B.value, dut.Coincidence_window_A.value,
        dut.Coincidence_window_B.value, dut.Coincidence_type_A.value, dut.Coincidence_type_B.value, dut.Multiplicity_A.value, dut.Multiplicity_B.value, dut.Scalar_Value_A.value, dut.Scalar_Value_B.value,
        dut.Trigger_Mode.value, dut.User_Start.value, dut.Reset_In.value)
        SABRETriggerOracle.checkOutput(dut.Trigger_Out_A.value, dut.Trigger_Out_B.value)