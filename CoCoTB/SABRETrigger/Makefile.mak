TOPLEVEL_LANG ?= vhdl

PWD=$(shell pwd)

VHDL_SOURCES = $(PWD)/../../Modules_VHDL/SABRETrigger.vhd $(PWD)/../../Modules_VHDL/GlobalTrigger.vhd $(PWD)/../../Modules_VHDL/InputPortModule.vhd $(PWD)/../../Modules_VHDL/SinglesCounter.vhd 
$(PWD)/../../Modules_VHDL/SinglesDetector.vhd $(PWD)/../../Modules_VHDL/MultiplicityTrigger.vhd $(PWD)/../../Modules_VHDL/Multiflop_Delay_PE_combined.vhd 
$(PWD)/../../Modules_VHDL/PulseExtenderCombined.vhd $(PWD)/../../Modules_VHDL/PulseExtenderInputState.vhd $(PWD)/../../Modules_VHDL/PulseExtenderRisingEdge.vhd 
$(PWD)/../../Modules_VHDL/DelayModule.vhd $(PWD)/../../Modules_VHDL/MultiFlopSynchroniser.vhd $(PWD)/../../Modules_VHDL/StartModule.vhd $(PWD)/../../Modules_VHDL/ResetModule.vhd

CUSTOM_COMPILE_DEPS = $(PWD)/../../Modules_VHDL/types.vhd

TOPLEVEL := sabretrigger
MODULE   := teststorun

include $(shell cocotb-config --makefiles)/Makefile.sim