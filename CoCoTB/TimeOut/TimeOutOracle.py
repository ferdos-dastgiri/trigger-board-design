import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb.triggers import FallingEdge
from queue import Queue
from collections import deque

class TimeOutOracle:
    def __init__(self):
        # TimeOut_duration is the max no of clock cycles before the timeout module outputs the signals to low for one clock cycle
        # self.TimeOut_duration = deque(maxlen = (TimeOut_duration))
        # self.TimeOut_duration.extend([0]*(TimeOut_duration)) #fill with the default value
        self.clockCycle = 0
        self.Reset = 0
    
    def setInput(self, moduleInput, Timeout_limit, Reset):
        self.input = moduleInput
        self.Reset = Reset
        self.clockCycle += 1

        counter_signalIn = 0
        self.Timeout_limit = int(Timeout_limit)

        if moduleInput == 1:
            counter_signalIn += 1
        self.counter_signalIn = counter_signalIn

    def checkOutput(self, moduleOutput):

        self.clockCycle += 1
        self.output = moduleOutput

        if self.Reset == 1:
            assert moduleOutput == 0, f"The output should be low, but is not: {moduleOutput} !=0"
        else:
            if self.input == 1:
                if self.counter_signalIn == self.Timeout_limit:
                    assert moduleOutput == 0, f"The output should be low, but is not: {moduleOutput} !=0"
                else:
                    assert moduleOutput == 1, f"Failed to set outputs high: {moduleOutput} != 1, on cycle number {self.clockCycle}, the previous inputs were {self.input}"
            else:
                assert moduleOutput == 0, f"The output should be low, but is not: {moduleOutput} !=0"
            
    def getPredictedOutput(self):
        self.clockCycle += 1
        
        if self.counter_signalIn >= self.Timeout_limit:
            if self.Reset == 0:
                return 1
            else:
                return 0
        else:
            return 0
        

async def everyClock(dut):
    timeOutOracle = TimeOutOracle()

    while True:
        await FallingEdge(dut.CLK)
        timeOutOracle.setInput(dut.Triggered_in.value, dut.Timeout_limit.value, dut.Reset.value) 
        timeOutOracle.checkOutput(dut.Timeout_out.value) 
