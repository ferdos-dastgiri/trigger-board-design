import random
import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from TimeOutOracle import everyClock
from cocotb import simulator
from cocotb.handle import *
import sys

checkpoint_hier = []
checkpoints = {}
#this function must be called prior to creating any checkpoint
def get_checkpoint_hier(entity):
   for ii in entity._handle.iterate(simulator.OBJECTS):
      hdl = SimHandle(ii, entity._path + "." + entity._handle.get_name_string())
      if ((entity._handle.get_type() is simulator.MODULE) or
          (entity._handle.get_type() is simulator.NETARRAY) or
          (entity._handle.get_type() is simulator.GENARRAY)):
            get_checkpoint_hier(hdl)
      elif entity._handle.get_type() is simulator.REG:
            checkpoint_hier.append(hdl)
            
#returns a created checkpoint
def checkpoint():
    checkpoint_data = {}
    #print(len(checkpoint_hier))
    for reg in checkpoint_hier:
        checkpoint_data[reg] = reg.value
    return checkpoint_data
#restores a checkpoint
def restore(checkpoint_data):
    for reg in checkpoint_data:
        reg.setimmediatevalue(checkpoint_data[reg])

async def generate_clock(dut):
    """Generate clock pulses."""
    for cycle in range(20):
            dut.CLK.value = 0
            await Timer(10, units="ns")
            dut.CLK.value = 1
            await Timer(10, units="ns")

async def reset_dut(dut):
    """Resets all the values of the module before a test"""

    dut.Triggered_in.value = 0
    dut.Timeout_limit.value = 0
    dut.Reset.value = 1
    dut.Timeout_out.value = 0
    await Timer(100, units="ns")


async def reset_signal(dut):
    dut.Reset.value = 1
    for _ in range(5):
            dut.CLK.value = 0
            await Timer(10, units="ns")
            dut.CLK.value = 1
            await Timer(10, units="ns")
    dut.Reset.value = 0
    await Timer(20, units="ns")


@cocotb.test()
async def test_mode1(dut):

    """
    This basic test is used to set the checkpoints to which the 
    module will return to at the beginning of each future test
    """
    await reset_dut(dut)
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut)) 

    #Set Timeout window
    await Timer(20, units="ns")
    Timeout_limit = 4
    Timeout_lim = format(Timeout_limit, '06b')
    dut.Timeout_limit.value = Timeout_limit

    # dut.Timeout_limit.value = int(Timeout_lim, base=2)

    await Timer(20, units="ns")
    dut.Triggered_in.value = 1
    
    await Timer(20, units="ns")
    dut.Triggered_in.value = 0
    
    #Reset space for new test
    await Timer(120, units="ns")


@cocotb.test()
async def test_mode2(dut):
     
    """
    This basic test will check what happens to a signal that is less than Timeout_limit
    """

    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    await Timer(20, units="ns")
    Timeout_limit = 4
    Timeout_lim = format(Timeout_limit, '06b')
    dut.Timeout_limit.value = Timeout_limit

    await Timer(40, units="ns")
    dut.Triggered_in.value = 1

    await Timer(40, units="ns")
    dut.Triggered_in.value = 0

    #Reset space for new test
    await Timer(120, units="ns")


@cocotb.test()
async def test_mode3(dut):
     
    """
    This basic test will check what happens to a signal that is greater than Timeout limit
    """

    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    await Timer(20, units="ns")
    Timeout_limit = 3
    Timeout_lim = format(Timeout_limit, '06b')
    dut.Timeout_limit.value = Timeout_limit

    await Timer(40, units="ns")
    dut.Triggered_in.value = 1

    await Timer(80, units="ns")
    dut.Triggered_in.value = 0

    #reset for next test
    await Timer(120, units="ns")


@cocotb.test()
async def test_mode4(dut):
     
    """
    This basic test will check what happens to a signal that is greater and less than the Timeout limit 
    """

    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    await Timer(40, units="ns")
    Timeout_limit = 3
    Timeout_lim = format(Timeout_limit, '06b')
    dut.Timeout_limit.value = Timeout_limit

    await Timer(40, units="ns")
    dut.Triggered_in.value = 1

    await Timer(60, units="ns")
    dut.Triggered_in.value = 0

    await Timer(120, units="ns")
    dut.Triggered_in.value = 1

    await Timer(20, units="ns")
    dut.Triggered_in.value = 0

    await Timer(80, units="ns")
    dut.Triggered_in.value = 1

    await Timer(20, units="ns")
    dut.Triggered_in.value = 0

    await Timer(20, units="ns")
    dut.Triggered_in.value = 1

    #reset for next test
    await Timer(120, units="ns")


@cocotb.test()
async def test_maxLimit(dut):
    """
    This basic test will check what happens when Timeout limit is full 111111 
    """

    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    await Timer(40, units="ns")
    Timeout_limit = 63
    Timeout_lim = format(Timeout_limit, '06b')
    dut.Timeout_limit.value = Timeout_limit

    await Timer(40, units="ns")
    dut.Triggered_in.value = 1

    await Timer(20, units="ns")
    dut.Triggered1_in.value = 0

    await Timer(20, units="ns")
    dut.Triggered_in.value = 1

    await Timer(1300, units="ns")
    dut.Triggered_in.value = 0

    #reset for next test
    await Timer(120, units="ns")

@cocotb.test()
async def test_zeroLimit(dut):
    """
    This basic test will check what happens when Timeout limit is empty 
    """

    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    await Timer(40, units="ns")
    Timeout_limit = 0
    Timeout_lim = format(Timeout_limit, '06b')
    dut.Timeout_limit.value = Timeout_limit

    await Timer(40, units="ns")
    dut.Triggered_in.value = 1

    await Timer(20, units="ns")
    dut.Triggered1_in.value = 0

    await Timer(20, units="ns")
    dut.Triggered_in.value = 1

    await Timer(60, units="ns")
    dut.Triggered_in.value = 0

    #reset for next test
    await Timer(120, units="ns")

@cocotb.test()
async def test_mode5(dut):
    """
    This test will check what happens when Timeout limit changes in  between.  
    """

    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    await Timer(40, units="ns")
    Timeout_limit = 2
    Timeout_lim = format(Timeout_limit, '06b')
    dut.Timeout_limit.value = Timeout_limit

    await Timer(40, units="ns")
    dut.Triggered_in.value = 1

    await Timer(20, units="ns")
    dut.Triggered1_in.value = 0
    Timeout_limit = 8
    Timeout_lim = format(Timeout_limit, '06b')
    dut.Timeout_limit.value = Timeout_limit

    await Timer(140, units="ns")
    dut.Triggered_in.value = 1

    await Timer(140, units="ns")
    dut.Triggered_in.value = 0

    #reset for next test
    await Timer(120, units="ns")