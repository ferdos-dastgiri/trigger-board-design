import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb.triggers import FallingEdge
from queue import Queue
from collections import deque

class PulseExtenderInputStateOracle:
    def __init__(self, Coincidence_Window):
        self.Coincidence = Coincidence_Window  
        self.inputs = deque()
        self.inputs.extend([0]*(self.Coincidence+1))
        self.clockCycle = 0
        
    def setInput(self, moduleInput, coincidence, reset):
        self.clockCycle += 1
        self.input = moduleInput
        self.inputs.append(moduleInput)
        if coincidence != self.Coincidence:
            if int(coincidence) > int(self.Coincidence):
                for _ in range(coincidence-self.Coincidence):
                    self.inputs.append(0)
            else:
                for _ in range(self.Coincidence - coincidence):
                    self.inputs.pop()
            self.Coincidence = coincidence
        self.inputs.popleft()

        self.reset = reset
        if reset:
            self.inputs = deque()
            self.inputs.extend([0]*(self.Coincidence+1))


    # needs to return values expected by checkOutput
    def getPredictedOutput(self):
        if 1 in self.inputs:
            if self.reset == 0:
                return 1
            else:
                return 0
        else:
            return 0

    def checkOutput(self, moduleOutput):
        self.output = moduleOutput
        if 1 in self.inputs:
            if self.reset == 0:
                assert moduleOutput == 1, f"Failed to set outputs high: {moduleOutput} != 1, on cycle number {self.clockCycle}, the previous inputs were {self.inputs}"
            else:
                assert moduleOutput == 0, f"Outputs not low or undefined: {moduleOutput} != 0, on cycle number {self.clockCycle}, the previous inputs were {self.inputs}"
        else:
            assert moduleOutput == 0, f"Outputs not low or undefined: {moduleOutput} != 0, on cycle number {self.clockCycle}, the previous inputs were {self.inputs}"

async def everyClock(dut):
    pulseExtenderInputStateOracle = PulseExtenderInputStateOracle(dut.Coincidence_Window.value)

    while True:
        await FallingEdge(dut.CLK)
        pulseExtenderInputStateOracle.setInput(dut.Pulse_Extend_Signal_In.value, dut.Coincidence_Window.value, dut.Reset.value)  
        pulseExtenderInputStateOracle.checkOutput(dut.Pulse_Extend_Out.value) 
     