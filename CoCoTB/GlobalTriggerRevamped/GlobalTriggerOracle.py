import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb.triggers import FallingEdge
from queue import Queue
from collections import deque

class GlobalTriggerOracle:
    def __init__(self, outputDelay):
        #outputDelay is the delay time between the input and output states
        #print("outputDelay: ", outputDelay)
        self.detecCounter = deque(maxlen = (outputDelay))
        self.detecCounter.extend([0]*(outputDelay)) #fill with the default value
        self.clockCycle = 0
        self.Reset = 0
        self.A_Trigger_singles = 0
        self.A_mult = []
        self.B_Trigger_singles = 0
        self.B_mult = []
        self.Trigger_mode = []
        self.A_Thresh = []
        self.B_Thresh = []

    def setInput(self, A_Trigger_singles, A_Trigger_mult, B_Trigger_singles, B_Trigger_mult, Trigger_modeLower, Trigger_modeUpper, A_Thresh, B_Thresh, Reset):
        self.A_Trigger_singles = A_Trigger_singles
        self.A_mult = int(A_Trigger_mult)
        self.B_Trigger_singles = B_Trigger_singles
        self.B_mult = int(B_Trigger_mult)
        self.Trigger_mode = [Trigger_modeLower, Trigger_modeUpper]
        self.Reset = Reset
        self.A_Thresh = int(A_Thresh)
        self.B_Thresh = int(B_Thresh)

        self.clockCycle += 1

    def checkOutput(self, moduleOutput):
        counterToCheck = self.detecCounter[0] #the one that was added to the outputdelay
        self.output = moduleOutput
        if self.Reset == 1:
            assert moduleOutput == 0, f"Output should be zero and isn't: {moduleOutput} != 0, on cycle number {self.clockCycle}, the input mode is {self.Trigger_mode}"
        else:
            print(self.Trigger_mode)
            if self.Trigger_mode == [0,0]:
                if (self.A_Trigger_singles == 1) or (self.A_mult >= self.A_Thresh) or (self.B_Trigger_singles == 1) or (self.B_mult >= self.B_Thresh):
                    assert moduleOutput == 1, f"Output is wrong: {moduleOutput} != 1, on cycle number {self.clockCycle}"
                    # print("output is high")
                else:
                    assert moduleOutput == 0, f"Output should be zero and isn't: {moduleOutput} != 0, on cycle number {self.clockCycle}, the input mode is {self.Trigger_mode}"
                    # print("output is low")

            if self.Trigger_mode == [1,0]:
                if (self.A_Trigger_singles == 1) or (self.A_mult >= self.A_Thresh):
                    assert moduleOutput == 1, f"Output is wrong, should be triggering on crystals: {moduleOutput} != 1, on cycle number {self.clockCycle}"
                    # print("output is high")
                else:
                    assert moduleOutput == 0, f"Output should be zero and isn't: {moduleOutput} != 0, on cycle number {self.clockCycle}, the input mode is {self.Trigger_mode}"
                    # print("output is low")

            if self.Trigger_mode == [0,1]:
                
                if (self.B_Trigger_singles == 1) or (self.B_mult >= self.B_Thresh):
                    assert moduleOutput == 1, f"Output is wrong: {moduleOutput} != 1, on cycle number {self.clockCycle}"
                    # print("output is high")
                else:
                    assert moduleOutput == 0, f"Output should be zero and isn't: {moduleOutput} != 0, on cycle number {self.clockCycle}, the input mode is {self.Trigger_mode}"
                    # print("output is low")
            
            if self.Trigger_mode == [1,1]:
                if (self.A_mult >= self.A_Thresh) and (self.B_mult >= self.B_Thresh):
                    assert moduleOutput == 1, f"Output is wrong: {moduleOutput} != 1, on cycle number {self.clockCycle}"
                    # print("output is high")
                else:
                    assert moduleOutput == 0, f"Output should be zero and isn't: {moduleOutput} != 0, on cycle number {self.clockCycle}, the input mode is {self.Trigger_mode}"
                    # print("output is low")


    def getPredictedOutput(self):
        counterToCheck = self.detecCounter[0] #the one that was added to the outputdelay
        if self.Reset == 1:
            return 0
        else:
            print(self.Trigger_mode)
            if self.Trigger_mode == [0,0]:
                if (self.A_Trigger_singles == 1) or (self.A_mult >= self.A_Thresh) or (self.B_Trigger_singles == 1) or (self.B_mult >= self.B_Thresh):
                    return 1
                else:
                    return 0

            if self.Trigger_mode == [1,0]:
                if (self.A_Trigger_singles == 1) or (self.A_mult >= self.A_Thresh):
                    return 1
                else:
                    return 0

            if self.Trigger_mode == [0,1]:
                
                if (self.B_Trigger_singles == 1) or (self.B_mult >= self.B_Thresh):
                    return 1
                else:
                    return 0
                    
            if self.Trigger_mode == [1,1]:
                if (self.A_mult >= self.A_Thresh) and (self.B_mult >= self.B_Thresh):
                    return 1
                else:
                    return 0


async def everyClock(dut):
    globalTriggerOracle = GlobalTriggerOracle(1)
    
    while True:
        await FallingEdge(dut.CLK)
        #do the check first to gain a 1 clock cycle delay between input and output
        for i in range(6):
            globalTriggerOracle.setInput(dut.A_Trigger_singles.value, dut.A_mult[i].value, dut.B_Trigger_singles.value, dut.B_mult[i].value, dut.Trigger_mode[0].value, dut.Trigger_mode[1].value, dut.A_Thresh.value, dut.B_Thresh.value, dut.Reset.value) 
        globalTriggerOracle.checkOutput(dut.Trigger_out.value)
