import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from cocotb.triggers import FallingEdge
from queue import Queue
from collections import deque

class SinglesDetectorOracle:
    def __init__(self, outputDelay):
        #outputDelay is the delay time between the input and output states
        #print("outputDelay: ", outputDelay)
        self.detecCounter = deque(maxlen = (outputDelay))
        self.detecCounter.extend([0]*(outputDelay)) #fill with the default value
        self.clockCycle = 0
        self.Reset = 0


    def setInput(self, moduleInput, Reset):
        self.input = moduleInput
        self.Reset = Reset
        self.clockCycle += 1

        counter_detector = 0

        for i in range(32):
            if moduleInput[i] == 1:
                counter_detector += 1  
        self.detecCounter.append(counter_detector)
        #print("counter_detector: ", self.detecCounter)


    def checkOutput(self, moduleOutput):
        counterToCheck = self.detecCounter[0] #the one that was added to the outputdelay
        if self.Reset == 1:
            assert moduleOutput == 0, f"The output should be low, but is not: {moduleOutput} !=0"
        else:
            if counterToCheck == 1:
                assert moduleOutput == 1, f"Failed to set outputs high: {moduleOutput} != 1, on cycle number {self.clockCycle}, the previous inputs were {self.input}"
            else:
                assert moduleOutput == 0, f"The output should be low, but is not: {moduleOutput} !=0"


    def getPredictedOutput(self):
        counterToCheck = self.detecCounter[0] #the one that was added to the outputdelay
        if self.Reset == 1:
            return 0
        else:
            if counterToCheck == 1:
                return 1
            else:
                return 0
        
async def everyClock(dut):
    singlesDetectorOracle = SinglesDetectorOracle(1)

    while True:
        await FallingEdge(dut.CLK_Sing_Veto)
        singlesDetectorOracle.setInput(dut.Pulses_In.value, dut.Reset.value) 
        singlesDetectorOracle.checkOutput(dut.Singlesveto.value) 