import random
import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge
from SinglesDetectorOracle import everyClock
from cocotb import simulator
from cocotb.handle import *
import sys

checkpoint_hier = []
checkpoints = {}

#this function must be called prior to creating any checkpoint
def get_checkpoint_hier(entity):
   for ii in entity._handle.iterate(simulator.OBJECTS):
      hdl = SimHandle(ii, entity._path + "." + entity._handle.get_name_string())
      if ((entity._handle.get_type() is simulator.MODULE) or
          (entity._handle.get_type() is simulator.NETARRAY) or
          (entity._handle.get_type() is simulator.GENARRAY)):
            get_checkpoint_hier(hdl)
      elif entity._handle.get_type() is simulator.REG:
            checkpoint_hier.append(hdl)

#returns a created checkpoint
def checkpoint():
    checkpoint_data = {}
    #print(len(checkpoint_hier))
    for reg in checkpoint_hier:
        checkpoint_data[reg] = reg.value
    return checkpoint_data
#restores a checkpoint
def restore(checkpoint_data):
    for reg in checkpoint_data:
        reg.setimmediatevalue(checkpoint_data[reg])

async def generate_clock(dut):
    """Generate clock pulses."""
    for cycle in range(20):
            dut.CLK_Sing_Veto.value = 0
            await Timer(10, units="ns")
            dut.CLK_Sing_Veto.value = 1
            await Timer(10, units="ns")

async def reset_dut(dut):
    """Resets all the values of the module before a test"""

    for i in range(32):
        dut.Pulses_in[i].value = 0
    dut.CLK_Sing_Veto.value = 0
    dut.Reset.value = 1
    dut.Singles_Active = 0
    dut.Singlesveto.value = 0
    await Timer(100, units="ns")

async def reset_signal(dut):
    dut.Reset.value = 1
    for _ in range(5):
            dut.CLK_Sing_Veto.value = 0
            await Timer(10, units="ns")
            dut.CLK_Sing_Veto.value = 1
            await Timer(10, units="ns")
    dut.Reset.value = 0
    await Timer(20, units="ns")
    

@cocotb.test()
async def test_on(dut):

    """Very basic test just turning on one input signal"""

    await reset_dut(dut)
    get_checkpoint_hier(dut)
    #the first checkpoint
    checkpoints['0'] = (checkpoint(), None)
    await reset_signal(dut)

    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK_Sing_Veto)

    await cocotb.start(everyClock(dut))

    signal = []
    for i in range(32):
        signal.append(dut.Pulses_In[i].value)
    dut._log.info("Input signal is %s", signal)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Singlesveto.value)

    await Timer(20, units="ns")
    dut.Singles_Active = 1

    await Timer(60, units="ns")
    dut.Pulses_In[1].value = 1
    
    await Timer(80, units="ns")
    #dut.Pulses_In[1].value = 0
    signal = []
    for i in range(32):
        signal.append(dut.Pulses_In[i].value)
    dut._log.info("Input signal is %s", signal)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Singlesveto.value)


@cocotb.test()
async def test_off(dut):

    """This basic test will check that the Singles counter doesn't go high given two signals are high at the same time"""

    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    signal = []
    for i in range(32):
        signal.append(dut.Pulses_In[i].value)
    dut._log.info("Input signal is %s", signal)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Singlesveto.value)
 
    # await Timer(20, units="ns")
    # dut.Pulses_In[1].value = 1
    
    # await Timer(30, units="ns")
    # signal = []
    # for i in range(32):
    #     signal.append(dut.Pulses_In[i].value)
    # dut._log.info("Input signal is %s", signal)
    # dut._log.info("Output signal is %s", dut.Singlesveto.value)

    await Timer(40, units="ns")
    dut.Pulses_In[5].value = 1
    dut.Pulses_In[17].value = 1


    await Timer(40, units="ns")
    dut.Pulses_In[31].value = 1
    dut.Pulses_In[5].value = 0

    await Timer(40, units="ns")
    signal = []
    for i in range(32):
        signal.append(dut.Pulses_In[i].value)
    dut._log.info("Input signal is %s", signal)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Singlesveto.value)


@cocotb.test()
async def test_mode1(dut):

    """This test introduces a range of signals that will be on at the same time and will turn off."""

    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    signal = []
    for i in range(32):
        signal.append(dut.Pulses_In[i].value)
    
    dut._log.info("Input signal is %s", signal)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Singlesveto.value)

    await Timer(40, units="ns")
    dut.Pulses_In[0].value = 1
    dut.Pulses_In[1].value = 1
    dut.Pulses_In[2].value = 1

    await Timer(40, units="ns")
    dut.Pulses_In[0].value = 0
    dut.Pulses_In[1].value = 0
    dut.Pulses_In[2].value = 0

    await Timer(40, units="ns")
    dut.Pulses_In[5].value = 1
    dut.Pulses_In[17].value = 1

    await Timer(40, units="ns")
    dut.Pulses_In[31].value = 1
    dut.Pulses_In[5].value = 0

    await Timer(40, units="ns")
    dut.Pulses_In[31].value = 0

    await Timer(40, units="ns")
    for i in range(32):
        dut.Pulses_In[i].value = 1

    await Timer(20, units="ns")
    signal = []
    for i in range(32):
        signal.append(dut.Pulses_In[i].value)
    dut._log.info("Input signal is %s", signal)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Singlesveto.value)

@cocotb.test()
async def test_mode2(dut):

    #Reset everything
    await reset_signal(dut)
    restore(checkpoints['0'][0])
    await cocotb.start(generate_clock(dut))
    await RisingEdge(dut.CLK)

    await cocotb.start(everyClock(dut))

    signal = []
    for i in range(32):
        signal.append(dut.Pulses_In[i].value)

    dut._log.info("Input signal is %s", signal)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Singlesveto.value)

    await Timer(40, units="ns")
    dut.Pulses_In[6].value = 1
    dut.Pulses_In[7].value = 1
    dut.Pulses_In[31].value = 1

    await Timer(40, units="ns")
    dut.Pulses_In[6].value = 0
    dut.Pulses_In[7].value = 0
    dut.Pulses_In[31].value = 1

    await Timer(40, units="ns")
    dut.Reset.value = 1

    await Timer(40, units="ns")
    dut.Reset.value = 0

    await Timer(40, units="ns")
    dut.Pulses_In[6].value = 1

    await Timer(20, units="ns")
    signal = []
    for i in range(32):
        signal.append(dut.Pulses_In[i].value)
    dut._log.info("Input signal is %s", signal)
    dut._log.info("Reset signal is %s", dut.Reset.value)
    dut._log.info("Output signal is %s", dut.Singlesveto.value)
