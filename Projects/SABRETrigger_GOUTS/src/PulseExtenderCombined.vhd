LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.std_logic_unsigned.ALL;

ENTITY PulseExtenderCombined IS
    PORT (
        CLK : IN STD_LOGIC;
        Reset : IN STD_LOGIC := '0';
        Pulse_Extend_Signal_In : IN STD_LOGIC;
        Pulse_Select_Mode : IN STD_LOGIC := '0'; -- 0 InputState, 1 Rising edge
        Coincidence_Window : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
        Pulse_Extend_Out : OUT STD_LOGIC := '0'
        );
END PulseExtenderCombined;


ARCHITECTURE Pulse_Extension OF PulseExtenderCombined IS
    
    COMPONENT PulseExtenderInputState IS
        PORT (
            CLK : IN STD_LOGIC;
            Reset : IN STD_LOGIC;
            Pulse_Extend_Signal_In : IN STD_LOGIC;
            Coincidence_Window : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
            Pulse_Extend_Out : OUT STD_LOGIC
            );
    END COMPONENT;

    COMPONENT PulseExtenderRisingEdge IS
        PORT (
            CLK : IN STD_LOGIC;
            Reset : IN STD_LOGIC;
            Pulse_Extend_Signal_In : IN STD_LOGIC;
            Coincidence_Window : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
            Pulse_Extend_Out : OUT STD_LOGIC
            );
    END COMPONENT;

    SIGNAL inputStateOutput : STD_LOGIC;
    SIGNAL risingEdgeOutput : STD_LOGIC;

BEGIN

        InputStateExtender : PulseExtenderInputState
        PORT MAP(
            CLK => CLK,
            Reset => Reset,
            Pulse_Extend_Signal_In => Pulse_Extend_Signal_In,
            Coincidence_Window => Coincidence_Window,
            Pulse_Extend_Out => inputStateOutput
        );

        RisingEdgeExtender : PulseExtenderRisingEdge
        PORT MAP(
            CLK => CLK,
            Reset => Reset,
            Pulse_Extend_Signal_In => Pulse_Extend_Signal_In,
            Coincidence_Window => Coincidence_Window,
            Pulse_Extend_Out => risingEdgeOutput
        );

    Pulse_Extend_Out <= risingEdgeOutput when (Pulse_Select_Mode = '1') else inputStateOutput;

END Pulse_Extension;