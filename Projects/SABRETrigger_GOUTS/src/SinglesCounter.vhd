LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;

LIBRARY work;

ENTITY SinglesCounter IS
	PORT (
		CLK_counter 	: IN STD_LOGIC;
		Reset 		  	: IN STD_LOGIC := '0';

		Singles_veto 	: IN STD_LOGIC;
		Singles_scalar  : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		Singles_in 		: IN STD_LOGIC;

		Singles_out 	: OUT STD_LOGIC := '0'
	);
END SinglesCounter;


ARCHITECTURE singles OF SinglesCounter IS
	
	SIGNAL Status : STD_LOGIC := '0';
	
BEGIN single : PROCESS (CLK_counter) IS
	Variable Counter : STD_LOGIC_VECTOR (15 DOWNTO 0) := "0000000000000001";
	BEGIN
	
	IF (rising_edge(CLK_counter)) THEN
	
		IF (Reset = '1') THEN
			Counter := "0000000000000001";
			Singles_out <= '0';
			Status <= '0';
		ELSE
			--Singles_out <= '0';
			IF Singles_veto = '1' THEN
				IF (Singles_in = '1') THEN
					IF (Status = '0') THEN
						Status <= '1';
						Counter := Counter + 1;
						IF (Counter > Singles_scalar) then
							Counter := "0000000000000001";
							Singles_out <= '1';
						END IF;
					ELSIF (Counter > Singles_scalar) then
						Counter := "0000000000000001";
						Singles_out <= '1';
					ELSE
						Singles_out <= '0';
					END IF;
					
				ELSE
					Status <= '0';
					Singles_out <= '0';
				END IF;
					
			else
				
				IF Singles_in = '1' then
				
					IF Status = '0' then
						Status <= '1';
						Counter := Counter + 1;
					ELSE
						Singles_out <= '0';
					END IF;
					
				ELSE
					Status <= '0';
					Singles_out <= '0';
				END IF;
			END IF;
			
		END IF;
	END IF;

	END PROCESS single;
	
END ARCHITECTURE singles;