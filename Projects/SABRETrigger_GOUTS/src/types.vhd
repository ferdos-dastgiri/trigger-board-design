LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.std_logic_unsigned.ALL;

package array_typez is
type delay_array is array (31 DOWNTO 0) of std_logic_vector(5 DOWNTO 0);
type scalar_array is array (31 DOWNTO 0) of std_logic_vector(15 DOWNTO 0);
type mode_array is array (31 DOWNTO 0) of std_logic_vector(1 DOWNTO 0);
type multiplicity_array is array (63 DOWNTO 0) of std_logic_vector(5 DOWNto 0);
--type long_array is array (0 TO 31) of std_logic_vector(31 DOWNTO 0);
end package array_typez;

