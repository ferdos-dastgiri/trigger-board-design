
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.std_logic_unsigned.ALL;
USE work.array_typez.all;


ENTITY SinglesModuleCombined IS
	
    PORT (
        CLK : IN STD_LOGIC;
        Signals_In : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Scalar_Value : IN scalar_array;
        Trigger_Out : OUT STD_LOGIC
		  );
END SinglesModuleCombined;


ARCHITECTURE Combined_Singles_Module OF SinglesModuleCombined IS

    COMPONENT DelayModule IS
	PORT (
		CLK : IN STD_LOGIC;
		Delay_Signal_In : IN STD_LOGIC;
		Delay_Out : OUT STD_LOGIC := '0';
		Delay_Value : IN INTEGER := 1
	);
    END COMPONENT DelayModule;

    COMPONENT SinglesCounter IS
	PORT (
		CLK_counter : IN STD_LOGIC;
		Singles_veto : IN STD_LOGIC;
		Singles_scalar : IN STD_LOGIC_VECTOR (5 DOWNTO 0); -- Will need to be updated
		Singles_in : IN STD_LOGIC;
		Singles_out : OUT STD_LOGIC := '0'
	);
    END COMPONENT SinglesCounter;

    COMPONENT SinglesDetector IS
	PORT (
		CLK_Sing_Veto : IN STD_LOGIC;
		Pulses_in : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		Singlesveto : OUT STD_LOGIC := '0'
	);
    END COMPONENT SinglesDetector;

    SIGNAL veto_active : STD_LOGIC;
    SIGNAL delayed_signals : STD_LOGIC_VECTOR (31 DOWNTO 0);
	 SIGNAL counter_out : STD_LOGIC_VECTOR (31 DOWNTO 0);

BEGIN

    GEN_Delay_Module:
        for i in 0 to 31 generate
            DelayModuleXin : DelayModule
            PORT MAP(
                CLK => CLK,
                Delay_Signal_In => Signals_In(i),
                Delay_Out => delayed_signals(i)
                --Delay_Value => '1'
            );
    END generate GEN_Delay_Module;
    
    GEN_Sing_Det_Module:
        for i in 1 to 1 generate
			  Singdetmodin : SinglesDetector
			  PORT MAP(
					CLK_Sing_Veto => CLK,
					Pulses_in => Signals_In,
					Singlesveto => veto_active
			  );
    END generate GEN_Sing_Det_Module; 

    GEN_Sing_Count_Module:
        for i in 0 to 31 generate
            SinglescounterXin : SinglesCounter
            PORT MAP(
                CLK_counter => CLK,
                Singles_in => delayed_signals(i),
                Singles_veto => veto_active,
                Singles_out => counter_out(i),
                Singles_scalar => (Scalar_Value(i))
            );
    END generate GEN_Sing_Count_Module; 
	 
	 Singles_trigger : PROCESS (CLK, counter_out) IS
    BEGIN
        IF (rising_edge(CLK)) THEN
            IF counter_out = "00000000000000000000000000000000" then
                Trigger_Out <= '0';
            ELSE
                Trigger_Out <= '1';
            END IF;
        END IF;
    END PROCESS Singles_trigger;

END Combined_Singles_Module;