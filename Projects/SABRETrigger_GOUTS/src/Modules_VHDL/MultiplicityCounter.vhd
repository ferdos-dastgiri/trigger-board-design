LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.std_logic_unsigned.ALL;
ENTITY MultiplicityCounter IS
	-- ----------------------------------------------
	PORT (
		CLK                     : IN STD_LOGIC;
		Reset			        : IN STD_LOGIC := '0';

		Multiplicity_Signal_in  : IN STD_LOGIC_VECTOR (31 DOWNTO 0);

		Multiplicity_Out        : OUT STD_LOGIC_VECTOR (5 DOWNTO 0):= "000000" 
		
	);

END MultiplicityCounter;


ARCHITECTURE Behavioural_ML OF MultiplicityCounter IS
BEGIN
	Multiplicity_Trigg : PROCESS (CLK, Reset) IS
	-- Counter will be used to detect when Multiplicity is satisfied
	VARIABLE Counter_Mult : STD_LOGIC_VECTOR (5 DOWNTO 0) := "000000";

	BEGIN

		IF (rising_edge(CLK)) THEN
			Counter_Mult := "000000";
			--Multiplicity_Out <= "000000";
			
			IF (Reset = '1') then
				Counter_Mult := "000000";
				Multiplicity_Out <= "000000";

			ELSE
				FOR i IN 0 TO 31 LOOP
					IF (Multiplicity_Signal_in(i) = '1') THEN
						Counter_Mult := Counter_Mult + 1;

						-- IF (Counter_Mult = Multiplicity) THEN
						-- 	MultiplicityTrigger_Out <= '1';
						-- 	Counter_Mult := "00000000";
						-- END IF;
					END IF;
				END LOOP;
                Multiplicity_Out <= Counter_Mult;
			END IF;
		END IF;

	END PROCESS Multiplicity_Trigg;

END Behavioural_ML;