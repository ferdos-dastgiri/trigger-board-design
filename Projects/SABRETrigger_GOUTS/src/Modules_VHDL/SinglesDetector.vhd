LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;

LIBRARY work;

ENTITY SinglesDetector IS
	PORT (
		CLK_Sing_Veto : IN STD_LOGIC;
		Reset 		  : IN STD_LOGIC := '0';

		Singles_Active: IN STD_LOGIC;
		Pulses_in 	  : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		Singlesveto   : OUT STD_LOGIC := '0'
	);
END SinglesDetector;


ARCHITECTURE findsingles OF SinglesDetector IS
	
BEGIN
	singlesevent : PROCESS (CLK_Sing_Veto) IS
	VARIABLE Counter : STD_LOGIC_VECTOR (5 DOWNTO 0) := "000000";
	BEGIN
		IF (rising_edge(CLK_Sing_Veto)) THEN

			IF Singles_Active = '1' THEN
			
				Counter := "000000";

				IF (Reset = '1') THEN
					Singlesveto <= '0';
					-- FOR i IN 0 TO 31 LOOP
					-- 	Pulses_in(i) <= '0';
					-- END LOOP
					Counter := "000000";

				ELSE				
					FOR i IN 31 DOWNTO 0 LOOP
						IF Pulses_in(i) = '1' THEN
							Counter := Counter + 1;
						END IF;
					END LOOP;
	
					IF (Counter = "000001") THEN
						Singlesveto <= '1';
						
					ELSE	
						Singlesveto <= '0';
					END IF;					
				END IF;

			else
				Singlesveto <= '0';
			END IF;
		END IF;
	END PROCESS singlesevent;
	
END ARCHITECTURE findsingles;