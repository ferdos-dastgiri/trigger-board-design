LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;


ENTITY DelayModule IS
	GENERIC (
		RAM_DEPTH : NATURAL := 64); -- this is the maximum allowable delay that can be set. 
	PORT (
		CLK : IN STD_LOGIC;
		-- Write port - Input
		Delay_Signal_In : IN STD_LOGIC;
		Reset				: IN STD_LOGIC := '0';
		-- Read port - Output
		Delay_Out : OUT STD_LOGIC := '0';
		Delay_Value : IN STD_LOGIC_VECTOR (5 DOWNTO 0) -- this is the user configureable delay, should be a logic vector in number of clock cycles.

	);
END DelayModule;


ARCHITECTURE Ring_Buff_Delay OF DelayModule IS

	TYPE RAM_type IS ARRAY (0 TO RAM_DEPTH - 1) OF STD_LOGIC;
	SIGNAL RAM : RAM_type := (OTHERS => '0');

	SUBTYPE index_type IS INTEGER RANGE RAM_type'RANGE;
	SIGNAL head : index_type;
	SIGNAL tail : index_type;
	-- Defines the incrementation procedure, to be used throuhgout design.
	--		Increment and wrap and wrap back to 0 when highest index for head/tail is reached
	PROCEDURE incr(SIGNAL index : INOUT index_type) IS
	BEGIN
		IF (Reset = '1') THEN
			index <= index_type'low;
		ELSE
			IF index = index_type'high OR (index = to_integer(unsigned(Delay_Value)) - 1) THEN -- if index is at the highest, then take back to the lowest, else increment
				index <= index_type'low;
			ELSE
				index <= index + 1;
			END IF;
		END IF;
	END PROCEDURE;


-- entering the process	
BEGIN
	
	--incrementing the head pointer		
	PROC_HEAD : PROCESS (CLK)
	BEGIN
		IF rising_edge(CLK) THEN
			incr(head);
		END IF;
	END PROCESS;

	-- updating the tail pointer
	PROC_TAIL : PROCESS (CLK)
	BEGIN
		IF rising_edge(CLK) THEN
			incr(tail);
		END IF;
	END PROCESS;

	-- declaring as a synchronous tool
	PROC_RAM : PROCESS (CLK, Reset, Delay_Value)
	BEGIN
		IF rising_edge(CLK) THEN
			
			IF (Reset = '1') THEN
				RAM <= (OTHERS => '0');
				Delay_Out <= '0';
			ELSIF (Reset = '0') AND  (Delay_Value /= "000000") THEN
				RAM(head) <= Delay_Signal_In;
				Delay_Out <= RAM(tail);
            ELSIF (Reset = '0') AND (Delay_Value = "000000") THEN
                Delay_Out <= Delay_Signal_In;
			END IF;
		END IF;
	END PROCESS;

END ARCHITECTURE;