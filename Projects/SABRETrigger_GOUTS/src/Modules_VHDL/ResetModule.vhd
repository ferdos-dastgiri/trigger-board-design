LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.std_logic_unsigned.ALL;

ENTITY ResetModule IS

	PORT (
		CLK         : IN STD_LOGIC;
	
		Reset_In    : IN STD_LOGIC;
		
		Reset_Out	: OUT STD_LOGIC := '0'
        );

END ResetModule;


ARCHITECTURE Behavioural_Reset OF ResetModule IS

BEGIN
	Send_reset : PROCESS (CLK, Reset_In) IS
		VARIABLE Counter : STD_LOGIC_VECTOR(2 DOWNTO 0) :="000"; --counter used in the process
	BEGIN

		IF (rising_edge(CLK)) THEN
			
            IF (Reset_In = '1') THEN
                IF (Counter = "000") THEN
                    Reset_Out <= '1';
                    Counter := Counter + 1;
                -- ELSIF (Counter = "101") THEN
                --     Reset_Out <= '0';
                    --Counter := "000";
                ELSIF (Counter = "101") THEN
                    Reset_Out <= '0';
                    Counter := "000";
                else
                    Reset_Out <= '1';
                    Counter := Counter + 1; 
                END IF;

            ELSE
                IF (Counter /= "000") THEN
                    IF (Counter = "101") THEN
                        Reset_Out <= '0';
                        Counter := "000";
                    -- ELSIF (Counter = "101") THEN
                    --     Reset_Out <= '0';
                    --     --Counter := "000";
                    else
                        Reset_Out <= '1';
                        Counter := Counter + 1;
                    END IF;

                else
                    Reset_Out <= '0';
                
                END IF;
            END IF;
		END IF;

	END PROCESS Send_reset;
END Behavioural_Reset;