LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.std_logic_unsigned.ALL;
USE IEEE.std_logic_misc.ALL;
library work;
use work.array_typez.all;
--use work.array_typez.all;


ENTITY InputPortModule_Revamped IS
	
    PORT (
        CLK : IN STD_LOGIC;
        Reset : IN STD_LOGIC;

        Port_Signal_In :     IN STD_LOGIC_VECTOR (31 DOWNTO 0);

        Channel_mask    :    IN STD_LOGIC_VECTOR (31 DOWNTO 0);

        Delay_Value :        IN delay_array;
        Coincidence_window : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
        Coincidence_type :   IN STD_LOGIC;

        Singles_Active:		  IN STD_LOGIC;
		Scalar_Value :       IN scalar_array;

		Test_Input			: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_Chmask			: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_MF				: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_Delay			: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_PE				: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_Mult			: OUT STD_LOGIC_VECTOR (5 DOWNTO 0);
        Test_SingDel		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_SingDet		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_SingCount		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);

        Single_Trigger_Out : OUT STD_LOGIC;
        Mult_Out :   OUT STD_LOGIC_VECTOR (5 DOWNTO 0)
		  );
END InputPortModule_Revamped;


ARCHITECTURE PortModuleLogic OF InputPortModule_Revamped IS

    COMPONENT Multiflop_Delay_PE_combined IS
    PORT (
        CLK : IN STD_LOGIC;
        Reset : IN STD_LOGIC;
        Signal_In : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Delay_Value : IN delay_array;
        Coincidence_window : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
        Coincidence_type : IN STD_LOGIC;
        Test_MF : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_Delay : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
        Signal_Out : OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
      );
    END COMPONENT Multiflop_Delay_PE_combined;

    COMPONENT DelayModule IS
	PORT (
		CLK : IN STD_LOGIC;
        Reset : IN STD_LOGIC;
		Delay_Signal_In : IN STD_LOGIC;
		Delay_Out : OUT STD_LOGIC;
        Delay_Value : IN STD_LOGIC_VECTOR (5 DOWNTO 0) 
	);
    END COMPONENT DelayModule;

    COMPONENT SinglesCounter IS
	PORT (
		CLK_counter : IN STD_LOGIC;
        Reset : IN STD_LOGIC;
		Singles_veto : IN STD_LOGIC;
		Singles_scalar : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		Singles_in : IN STD_LOGIC;
		Singles_out : OUT STD_LOGIC
	);
    END COMPONENT SinglesCounter;

    COMPONENT SinglesDetector IS
	PORT (
		CLK_Sing_Veto : IN STD_LOGIC;
        Reset 		  : IN STD_LOGIC;
		  Singles_Active: IN STD_LOGIC;
		Pulses_in : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		Singlesveto : OUT STD_LOGIC
	);
    END COMPONENT SinglesDetector;

    COMPONENT MultiplicityCounter IS
    PORT (
       CLK                     : IN STD_LOGIC;
       Reset			        : IN STD_LOGIC := '0';

       Multiplicity_Signal_in  : IN STD_LOGIC_VECTOR (31 DOWNTO 0);

       Multiplicity_Out        : OUT STD_LOGIC_VECTOR (5 DOWNTO 0):= "000000" 
       
   );

   END COMPONENT MultiplicityCounter;
 

    SIGNAL PEOutputs : STD_LOGIC_VECTOR(31 DOWNTO 0);
    SIGNAL delayedPEOutputs : STD_LOGIC_VECTOR(31 DOWNTO 0);
    SIGNAL multiplicityOut : STD_LOGIC;
    SIGNAL singleTriggerOutputs : STD_LOGIC_VECTOR(31 DOWNTO 0);
    SIGNAL singlesDetectorOut : STD_LOGIC;
    SIGNAL Multval : STD_LOGIC_VECTOR(5 DOWNTO 0);
    SIGNAL Del_Mult_val : STD_LOGIC_VECTOR(31 DOWNTO 0);

    SIGNAL Mask_out : STD_LOGIC_VECTOR (31 DOWNTO 0);

    constant multiplicityDelayValue : STD_LOGIC_VECTOR (5 DOWNTO 0) := "000000";
    constant PEDelayValue : STD_LOGIC_VECTOR (5 DOWNTO 0) := "000000";
BEGIN

    multiflopDelayPE : Multiflop_Delay_PE_combined
    PORT MAP(
        CLK => CLK,
        Reset => Reset,
        Signal_In => Mask_out,
        Delay_Value => Delay_Value,
        Coincidence_window => Coincidence_window,
        Coincidence_type => Coincidence_type,
        Test_MF => Test_MF,
        Test_Delay => Test_Delay,
        Signal_Out => PEOutputs
    );
    
    GEN_Mult_Delay_Module:
        for i in 0 to 31 generate
            PEDelayModuleX : DelayModule
            PORT MAP(
                CLK => CLK,
                Reset => Reset,
                Delay_Signal_In => PEOutputs(i),
                Delay_Out => Del_Mult_val(i),
                Delay_Value => multiplicityDelayValue
            );
    END generate GEN_Mult_Delay_Module;

    Multiplicity: MultiplicityCounter
	PORT MAP(
		CLK => CLK,                     
		Reset => Reset,			        
		Multiplicity_Signal_in => Del_Mult_val,  
		Multiplicity_Out => Multval
	);

    GEN_Delay_Module:
        for i in 0 to 31 generate
            PEDelayModuleX : DelayModule
            PORT MAP(
                CLK => CLK,
                Reset => Reset,
                Delay_Signal_In => PEOutputs(i),
                Delay_Out => delayedPEOutputs(i),
                Delay_Value => PEDelayValue
            );
    END generate GEN_Delay_Module;

    singlesDetector1 : SinglesDetector
    PORT MAP(
		CLK_Sing_Veto => CLK,
        Reset => Reset,
		  Singles_Active => Singles_Active,
		Pulses_in => PEOutputs,
		Singlesveto => singlesDetectorOut
    );

    GEN_SinglesCounter:
        for i in 0 to 31 generate
            SinglesCounterX : SinglesCounter
            PORT MAP(
                CLK_counter => CLK,
                Reset => Reset,
                Singles_veto => singlesDetectorOut,
                Singles_scalar => Scalar_Value(i),
                Singles_in => delayedPEOutputs(i),
                Singles_out => singleTriggerOutputs(i)
            );
    END generate GEN_SinglesCounter;

    --Single_Trigger_Out <= or_reduce( singleTriggerOutputs );
	-- Test_out(0) <= delayedPEOutputs(0);
	-- Test_out(1) <= singlesDetectorOut; 

    Toggle_Channel : Process (Port_Signal_In, Channel_mask) IS
    begin
        FOR i in 0 to 31 loop
            Mask_out(i) <= Port_Signal_In(i) AND Channel_mask(i);
        END LOOP;
    end process Toggle_Channel;

    Testing : Process (CLK) is
    begin
        IF (rising_edge(CLK)) THEN
            for i in 31 downto 0 loop
                Test_Input(i) <= Port_Signal_In(i);		
                Test_Chmask(i) <= Channel_mask(i);		
                --Test_MF(i) <= 				
                --Test_Delay(i) <= 		
                Test_PE(i) <= PEOutputs(i);				
                		
                Test_SingDel(i) <= delayedPEOutputs(i);	
                Test_SingDet(i) <= singlesDetectorOut;	
                Test_SingCount(i) <= singleTriggerOutputs(i);
                

                Single_Trigger_Out <= or_reduce(singleTriggerOutputs);

            end loop;
            for i in 5 downto 0 loop
                Test_Mult(i) <= Multval(i);
                Mult_Out(i) <= Multval(i);	
            end loop;
        end if;
    end process Testing;		

END PortModuleLogic;