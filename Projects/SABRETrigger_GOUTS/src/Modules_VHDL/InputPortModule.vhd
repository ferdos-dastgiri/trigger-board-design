LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.std_logic_unsigned.ALL;
USE IEEE.std_logic_misc.ALL;
library work;
use work.array_typez.all;
--use work.array_typez.all;


ENTITY InputPortModule IS
	
    PORT (
        CLK : IN STD_LOGIC;
        Reset : IN STD_LOGIC;

        Port_Signal_In :     IN STD_LOGIC_VECTOR (31 DOWNTO 0);

        Channel_mask    :    IN STD_LOGIC_VECTOR (31 DOWNTO 0);

        Delay_Value :        IN delay_array;
        Coincidence_window : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
        Coincidence_type :   IN STD_LOGIC;

        Multiplicity :       IN STD_LOGIC_VECTOR (7 DOWNTO 0) := "11111111";

        Singles_Active:		  IN STD_LOGIC;
		  Scalar_Value :       IN scalar_array;
		  Test_out				: OUT STD_LOGIC_VECTOR (1 DOWNTO 0); 
    
        Single_Trigger_Out : OUT STD_LOGIC;
        Mult_Trigger_Out :   OUT STD_LOGIC
		  );
END InputPortModule;


ARCHITECTURE PortModuleLogic OF InputPortModule IS

    COMPONENT Multiflop_Delay_PE_combined IS
    PORT (
        CLK : IN STD_LOGIC;
        Reset : IN STD_LOGIC;
        Signal_In : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Delay_Value : IN delay_array;
        Coincidence_window : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
        Coincidence_type : IN STD_LOGIC;
        Signal_Out : OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
      );
    END COMPONENT Multiflop_Delay_PE_combined;

    COMPONENT MultiplicityTrigger IS
	PORT (
		CLK : IN STD_LOGIC;
        Reset : IN STD_LOGIC;
		Multiplicity_Signal_in : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		Multiplicity : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		MultiplicityTrigger_Out : OUT STD_LOGIC
	);
    END COMPONENT MultiplicityTrigger;

    COMPONENT DelayModule IS
	PORT (
		CLK : IN STD_LOGIC;
        Reset : IN STD_LOGIC;
		Delay_Signal_In : IN STD_LOGIC;
		Delay_Out : OUT STD_LOGIC;
        Delay_Value : IN STD_LOGIC_VECTOR (5 DOWNTO 0) 
	);
    END COMPONENT DelayModule;

    COMPONENT SinglesCounter IS
	PORT (
		CLK_counter : IN STD_LOGIC;
        Reset : IN STD_LOGIC;
		Singles_veto : IN STD_LOGIC;
		Singles_scalar : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		Singles_in : IN STD_LOGIC;
		Singles_out : OUT STD_LOGIC
	);
    END COMPONENT SinglesCounter;

    COMPONENT SinglesDetector IS
	PORT (
		CLK_Sing_Veto : IN STD_LOGIC;
        Reset 		  : IN STD_LOGIC;
		  Singles_Active: IN STD_LOGIC;
		Pulses_in : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		Singlesveto : OUT STD_LOGIC
	);
    END COMPONENT SinglesDetector;
 

    SIGNAL PEOutputs : STD_LOGIC_VECTOR(31 DOWNTO 0);
    SIGNAL delayedPEOutputs : STD_LOGIC_VECTOR(31 DOWNTO 0);
    SIGNAL multiplicityOut : STD_LOGIC;
    SIGNAL singleTriggerOutputs : STD_LOGIC_VECTOR(31 DOWNTO 0);
    SIGNAL singlesDetectorOut : STD_LOGIC;

    SIGNAL Mask_out : STD_LOGIC_VECTOR (31 DOWNTO 0);

    constant multiplicityDelayValue : STD_LOGIC_VECTOR (5 DOWNTO 0) := "000000";
    constant PEDelayValue : STD_LOGIC_VECTOR (5 DOWNTO 0) := "000000";
BEGIN

    multiflopDelayPE : Multiflop_Delay_PE_combined
    PORT MAP(
        CLK => CLK,
        Reset => Reset,
        Signal_In => Mask_out,
        Delay_Value => Delay_Value,
        Coincidence_window => Coincidence_window,
        Coincidence_type => Coincidence_type,
        Signal_Out => PEOutputs
    );

    multiplicityTrigger1 : MultiplicityTrigger
    PORT MAP(
        CLK => CLK,
        Reset => Reset,
		Multiplicity_Signal_in => PEOutputs,
		Multiplicity => Multiplicity,
		MultiplicityTrigger_Out => multiplicityOut
    );
    
    multiplicityDelay : DelayModule
    PORT MAP(
        CLK => CLK,
        Reset => Reset,
		Delay_Signal_In => multiplicityOut,
		Delay_Out => Mult_Trigger_Out,
		Delay_Value => multiplicityDelayValue
    );

    GEN_Delay_Module:
        for i in 0 to 31 generate
            PEDelayModuleX : DelayModule
            PORT MAP(
                CLK => CLK,
                Reset => Reset,
                Delay_Signal_In => PEOutputs(i),
                Delay_Out => delayedPEOutputs(i),
                Delay_Value => PEDelayValue
            );
    END generate GEN_Delay_Module;

    singlesDetector1 : SinglesDetector
    PORT MAP(
		CLK_Sing_Veto => CLK,
        Reset => Reset,
		  Singles_Active => Singles_Active,
		Pulses_in => PEOutputs,
		Singlesveto => singlesDetectorOut
    );

    GEN_SinglesCounter:
        for i in 0 to 31 generate
            SinglesCounterX : SinglesCounter
            PORT MAP(
                CLK_counter => CLK,
                Reset => Reset,
                Singles_veto => singlesDetectorOut,
                Singles_scalar => Scalar_Value(i),
                Singles_in => delayedPEOutputs(i),
                Singles_out => singleTriggerOutputs(i)
            );
    END generate GEN_SinglesCounter;

    Single_Trigger_Out <= or_reduce( singleTriggerOutputs );
	 Test_out(0) <= delayedPEOutputs(0);
	 Test_out(1) <= singlesDetectorOut; 

    Toggle_Channel : Process (Port_Signal_In, Channel_mask) IS
    begin
        FOR i in 0 to 31 loop
            Mask_out(i) <= Port_Signal_In(i) AND Channel_mask(i);
        END LOOP;
    end process Toggle_Channel;
END PortModuleLogic;