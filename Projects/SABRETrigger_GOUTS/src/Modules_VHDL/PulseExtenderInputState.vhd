LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.std_logic_unsigned.ALL;

ENTITY PulseExtenderInputState IS

	PORT (
		CLK : IN STD_LOGIC;
		-- Signal into the Pulse Extension sub-module
		Pulse_Extend_Signal_In : IN STD_LOGIC;
		-- Input for the Pulse Extension amount (number of clock cycles)
		Coincidence_Window : IN STD_LOGIC_VECTOR(7 DOWNTO 0) := "00000000";
		-- Signal out of the Pulse Extension sub-module
		Reset			: IN STD_LOGIC := '0';
		Pulse_Extend_Out : OUT STD_LOGIC := '0');

END PulseExtenderInputState;


ARCHITECTURE Behavioural_PE OF PulseExtenderInputState IS

BEGIN
	pulse_extend : PROCESS (CLK, Reset) IS
		VARIABLE Counter_PE : STD_LOGIC_VECTOR(7 DOWNTO 0) :="00000000"; --counter used in the process
	BEGIN

		IF (rising_edge(CLK)) THEN
			IF (Reset = '1') then
				Counter_PE := "00000000";
				Pulse_Extend_Out <= '0';
	
			ELSE
				IF (Pulse_Extend_Signal_In = '1') THEN
					Pulse_Extend_Out <= '1';
					Counter_PE := "00000000";
				END IF;

				IF (Pulse_Extend_Signal_In = '0') THEN
					Counter_PE := Counter_PE + 1;
				END IF;

				IF (Counter_PE > Coincidence_Window) THEN
					Pulse_Extend_Out <= '0';
				END IF;
			END IF;
		END IF;

	END PROCESS pulse_extend;
END Behavioural_PE;