LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.std_logic_unsigned.ALL;

ENTITY PulseExtenderRisingEdge IS
	PORT (
		CLK : IN STD_LOGIC;
		Pulse_Extend_Signal_In : IN STD_LOGIC;
		Coincidence_Window : IN STD_LOGIC_VECTOR (7 DOWNTO 0) := "00000000";
		Pulse_Extend_Out : OUT STD_LOGIC;
		Reset : IN Std_logic := '0');
END PulseExtenderRisingEdge;


ARCHITECTURE Pulse_Extension OF PulseExtenderRisingEdge IS
	SIGNAL Input_Signal_On : STD_LOGIC := '0'; --counts from the start of signal in to delay being reached
	SIGNAL previous : STD_LOGIC := '0';
BEGIN

	Pulse_Extend : PROCESS (CLK, Reset) IS
		VARIABLE counter_1 : STD_LOGIC_VECTOR (7 DOWNTO 0) := "00000000"; --length of in pulse extension width
		
	BEGIN
		IF (rising_edge(CLK)) THEN
			IF (Reset = '1') THEN
				counter_1 := "00000000";
				Pulse_Extend_Out <= '0';
				Input_Signal_On <= '0';
			ELSE
				IF (Pulse_Extend_Signal_In = '1' AND previous = '0') THEN
					Input_Signal_On <= '1';
					Pulse_Extend_Out <= '1';
				END IF;

				previous <= Pulse_Extend_Signal_In;

				IF (Input_Signal_On = '1') THEN
					counter_1 := counter_1 + 1;
				END IF;

				IF (counter_1 >= Coincidence_Window) THEN
					Pulse_Extend_Out <= '0';
					Input_Signal_On <= '0';

					IF (Pulse_Extend_Signal_In = '0') THEN
						counter_1 := "00000000";
					END IF;
				END IF;
			END IF;
		END IF;
	END PROCESS Pulse_Extend;
	
END Pulse_Extension;