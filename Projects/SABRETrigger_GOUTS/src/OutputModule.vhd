LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.std_logic_unsigned.ALL;
USE IEEE.std_logic_misc.ALL;
library work;
use work.array_typez.all;
--use work.array_typez.all;


ENTITY OutputModule IS
	
    PORT (
        CLK             : IN STD_LOGIC;
        Reset           : IN STD_LOGIC;

        SinglesA        : IN STD_LOGIC;
        SinglesB        : IN STD_LOGIC;
        MultA           : IN STD_LOGIC;
        MultB           : IN STD_LOGIC;

        Trigger_Modes   : IN mode_array (others => (others=> "00"));
        Soft_trigger    : IN STD_LOGIC;
        
        Trigger_out     : OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
        
		  );
END OutputModule;


ARCHITECTURE OutputLogic OF OutputModule IS

    COMPONENT GlobalTrigger IS
    PORT (
        CLK_trigger : IN STD_LOGIC;
		Reset : IN STD_LOGIC := '0';
		A_Trigger_singles : IN STD_LOGIC;
		A_Trigger_mult : IN STD_LOGIC;
		B_Trigger_singles : IN STD_LOGIC;
		B_Trigger_mult : IN STD_LOGIC;
		Trigger_mode : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
		Trigger_out : OUT STD_LOGIC := '0'
      );
    END COMPONENT GlobalTrigger;

    COMPONENT StartModule IS
	PORT (
		CLK_start 		: IN STD_LOGIC;
		Reset 		  	: IN STD_LOGIC := '0';
		User_Start 		: IN STD_LOGIC;
		Start_Out 		: OUT STD_LOGIC := '0'
	);
    END COMPONENT StartModule;

    SIGNAL ChanTrig : STD_LOGIC_VECTOR (31 DOWNTO 0) (others='0');
    SIGNAL SoftTrig : STD_LOGIC := '0';

BEGIN

    GEN_Global_Trigger:
        for i in 0 to 31 generate
            GlobalTriggerX : GlobalTrigger
            PORT MAP(
                CLK_trigger => CLK,
		        Reset => Reset,
		        A_Trigger_singles => SinglesA,
		        A_Trigger_mult => MultA,
		        B_Trigger_singles => SinglesB,
		        B_Trigger_mult => MultB,
		        Trigger_mode => Trigger_Modes(i),
		        Trigger_out => ChanTrig(i)
            );
    END generate GEN_Global_Trigger;

    StartMod : StartModule
    PORT MAP(
		CLK_start => CLK,
		Reset => Reset,
		User_Start => Soft_trigger,
		Start_Out => SoftTrig
    );

    
    TrigOut : Process (ChanTrig, SoftTrig) IS
    begin
        FOR i in 0 to 31 loop
            Trigger_Out(i) <= ChanTrig(i) OR SoftTrig;

        END LOOP;
    end process TrigOut;

END OutputLogic; 