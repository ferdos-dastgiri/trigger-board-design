-- -- library IEEE;
-- -- use IEEE.STD_LOGIC_1164.ALL;

-- package array_typez is
--     type delay_array is array (31 DOWNTO 0) of std_logic_vector(7 DOWNTO 0);
-- end package array_typez;

-- package array_typez is
--     type scalar_array is array (31 DOWNTO 0) of std_logic_vector(5 DOWNTO 0);
-- end package array_typez;

-- -- package controlregs is
-- --     constant N_CONTROL_REGS : integer := 0;

-- --     type CONTROL_REGS_T is ARRAY (0 to N_CONTROL_REGS-1) of 
-- --                            STD_LOGIC_VECTOR(31 downto 0);
-- -- end package controlregs;


LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.std_logic_unsigned.ALL;
use work.array_typez.all;
--use work.array_typez.all;
-- use work.controlregs.all;



ENTITY SABRETrigger IS
	
    PORT (
        CLK : IN STD_LOGIC;
		  Test_out												: OUT STD_LOGIC_VECTOR (1 DOWNTO 0); 

        Port_Signal_In_A, Port_Signal_In_B          : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        --PortC_Out                                   : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);

        Channel_Mask_A, Channel_Mask_B              : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Delay_Value_A, Delay_Value_B                : IN delay_array;
        Coincidence_window_A, Coincidence_window_B  : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
        Coincidence_type_A, Coincidence_type_B      : IN STD_LOGIC;
        Multiplicity_A, Multiplicity_B              : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		  Singles_Active_A, Singles_Active_B			 : IN STD_LOGIC;
        Scalar_Value_A, Scalar_Value_B              : IN scalar_array;
        Trigger_Modes                               : IN mode_array; 
        Soft_Trigger                                  : IN STD_LOGIC;
        Reset_In                                    : IN STD_LOGIC;

        Trigger_Out                               : OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
      		);
END SABRETrigger;


ARCHITECTURE TriggerLogic OF SABRETrigger IS

    COMPONENT InputPortModule IS  
        PORT (
            CLK : IN STD_LOGIC;
            Reset : IN STD_LOGIC;
            Channel_mask    :    IN STD_LOGIC_VECTOR (31 DOWNTO 0);
            Port_Signal_In :     IN STD_LOGIC_VECTOR (31 DOWNTO 0);
            Delay_Value :        IN delay_array;
            Coincidence_window : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
            Coincidence_type :   IN STD_LOGIC;
            Multiplicity :       IN STD_LOGIC_VECTOR (7 DOWNTO 0);
				Singles_Active: 		IN STD_LOGIC;
            Scalar_Value :       IN scalar_array;
				
            Test_out					 : OUT STD_LOGIC_VECTOR (1 DOWNTO 0); 
            Single_Trigger_Out : OUT STD_LOGIC;
            Mult_Trigger_Out :   OUT STD_LOGIC
            );
    END COMPONENT InputPortModule;

    COMPONENT OutputModule IS
	
    PORT (
        CLK             : IN STD_LOGIC;
        Reset           : IN STD_LOGIC;

        SinglesA        : IN STD_LOGIC;
        SinglesB        : IN STD_LOGIC;
        MultA           : IN STD_LOGIC;
        MultB           : IN STD_LOGIC;

        Trigger_Modes   : IN mode_array;
        Soft_trigger    : IN STD_LOGIC;
        
        Trigger_out     : OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
        
		  );
    END COMPONENT OutputModule;

    COMPONENT ResetModule IS
	    PORT (
		CLK         : IN STD_LOGIC;
		Reset_In    : IN STD_LOGIC;
		Reset_Out	: OUT STD_LOGIC
	    );
    END COMPONENT ResetModule;

    SIGNAL Multi_Trigg_Out_A : STD_LOGIC;
    SIGNAL Singles_Trigg_A : STD_LOGIC;
    SIGNAL Multi_Trigg_Out_B : STD_LOGIC;
    SIGNAL Singles_Trigg_B : STD_LOGIC;

    --SIGNAL Start_Out : STD_LOGIC;
    SIGNAL Reset : STD_LOGIC;

BEGIN

    inputPortModuleA : InputPortModule
    PORT MAP(
        CLK => CLK,
        Reset => Reset_In,
        Channel_mask => Channel_Mask_A,
        Port_Signal_In => Port_Signal_In_A,
        Delay_Value => Delay_Value_A,
        Coincidence_window => Coincidence_window_A,
        Coincidence_type => Coincidence_type_A,
        Multiplicity => Multiplicity_A,
		  Singles_Active => Singles_Active_A,
        Scalar_Value => Scalar_Value_A,
        Single_Trigger_Out => Singles_Trigg_A,
        Mult_Trigger_Out => Multi_Trigg_Out_A,
		  Test_Out => Test_Out
    );

    inputPortModuleB : InputPortModule
    PORT MAP(
        CLK => CLK,
        Reset => Reset_In,
        Channel_mask => X"00000000",
        Port_Signal_In => Port_Signal_In_B,
        Delay_Value => Delay_Value_B,
        Coincidence_window => Coincidence_window_B,
        Coincidence_type => Coincidence_type_B,
        Multiplicity => Multiplicity_B,
		  Singles_Active => Singles_Active_B,
        Scalar_Value => Scalar_Value_B,
        Single_Trigger_Out => Singles_Trigg_B,
        Mult_Trigger_Out => Multi_Trigg_Out_B
    );

    OutPutMod : OutputModule
    PORT MAP(
      CLK => CLK,
      Reset => Reset,
      SinglesA => Singles_Trigg_A,
      SinglesB => Singles_Trigg_B,
      MultA => Multi_Trigg_Out_A,
      MultB => Multi_Trigg_Out_B,
      Trigger_Modes => Trigger_Modes,
      Soft_trigger => Soft_Trigger,
      Trigger_out => Trigger_Out
    );

    resetModuleOne : ResetModule
    PORT MAP(
		CLK => CLK,
		Reset_In => Reset_In,
		Reset_Out => Reset
    );
	
	
		--Test_out(0) <= Singles_Trigg_Out_A;
		--Test_out(1) <= Multi_Trigg_Out_B;
		--Test_out(1) <= Trigger_Out_A;
		
END TriggerLogic;