LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.std_logic_unsigned.ALL;
USE IEEE.std_logic_misc.ALL;
library work;
use work.array_typez.all;
--use work.array_typez.all;


ENTITY OutputModule_Revamped IS
	
    PORT (
        CLK             : IN STD_LOGIC;
        Reset           : IN STD_LOGIC;

        SinglesA        : IN STD_LOGIC;
        SinglesB        : IN STD_LOGIC;
        A_PE            : IN STD_LOGIC_VECTOR (5 DOWNTO 0);
        B_PE            : IN STD_LOGIC_VECTOR (5 DOWNTO 0);

        Trigger_Modes   : IN mode_array;
        Multiplicity    : IN multiplicity_array;
        Soft_trigger    : IN STD_LOGIC;
		TimeOut			: IN STD_LOGIC_VECTOR (5 DOWNTO 0);

		Test_MultA 		: OUT STD_LOGIC_VECTOR (5 DOWNTO 0);
		Test_MultB 		: OUT STD_LOGIC_VECTOR (5 DOWNTO 0);
		Test_Trig 		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
		Test_Soft 		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
        
        Trigger_out     : OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
        
		  );
END OutputModule_Revamped;


ARCHITECTURE OutputLogic OF OutputModule_Revamped IS

    COMPONENT GlobalTrigger_Revamped IS
    PORT (
        CLK			 			: IN STD_LOGIC;
		Reset 					: IN STD_LOGIC := '0';
		
		A_Trigger_singles 		: IN STD_LOGIC;
		A_mult 					: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		B_Trigger_singles 		: IN STD_LOGIC;
		B_mult 					: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		
		Trigger_mode 			: IN STD_LOGIC_VECTOR (1 DOWNTO 0);
		A_Thresh 				: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		B_Thresh 				: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		
		Trigger_out 			: OUT STD_LOGIC := '0'
      );
    END COMPONENT GlobalTrigger_Revamped;
	 
	--  COMPONENT MultiplicityCounter IS
	--  PORT (
	-- 	CLK                     : IN STD_LOGIC;
	-- 	Reset			        : IN STD_LOGIC := '0';

	-- 	Multiplicity_Signal_in  : IN STD_LOGIC_VECTOR (31 DOWNTO 0);

	-- 	Multiplicity_Out        : OUT STD_LOGIC_VECTOR (5 DOWNTO 0):= "000000" 
		
	-- );

	-- END COMPONENT MultiplicityCounter;

    COMPONENT StartModule IS
	PORT (
		CLK_start 		: IN STD_LOGIC;
		Reset 		  	: IN STD_LOGIC := '0';
		User_Start 		: IN STD_LOGIC;
		Start_Out 		: OUT STD_LOGIC := '0'
	);
    END COMPONENT StartModule;

	COMPONENT TimeOut_module IS
	PORT (
		CLK			 			: IN STD_LOGIC;
		Reset 					: IN STD_LOGIC := '0';
		Triggered_in	 		: IN STD_LOGIC;
		Timeout_limit 			: IN STD_LOGIC_VECTOR (5 DOWNTO 0) := "111111";
		Timeout_out 			: OUT STD_LOGIC := '0'
	);
END COMPONENT TimeOut_module;

    SIGNAL ChanTrig : STD_LOGIC_VECTOR (31 DOWNTO 0);
	SIGNAL TOTrig : STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL SoftTrig : STD_LOGIC := '0';
	-- SIGNAL AMUL : STD_LOGIC_VECTOR (5 DOWNTO 0);
	-- SIGNAL BMUL : STD_LOGIC_VECTOR (5 DOWNTO 0);

BEGIN

	-- A_Multiplicity: MultiplicityCounter
	-- PORT MAP(
	-- 	CLK => CLK,                     
	-- 	Reset => Reset,			        
	-- 	Multiplicity_Signal_in => A_PE,  
	-- 	Multiplicity_Out => AMUL
	-- );
	
	-- B_Multiplicity: MultiplicityCounter
	-- PORT MAP(
	-- 	CLK => CLK,                     
	-- 	Reset => Reset,			        
	-- 	Multiplicity_Signal_in => B_PE,  
	-- 	Multiplicity_Out => BMUL
	-- );
	

    GEN_Global_Trigger:
        for i in 0 to 31 generate
            GlobalTriggerX : GlobalTrigger_Revamped
            PORT MAP(
                CLK => CLK,
		        Reset => Reset,
		        A_Trigger_singles => SinglesA,
		        A_mult => A_PE,
		        B_Trigger_singles => SinglesB,
		        B_mult => B_PE,
		        Trigger_mode => Trigger_Modes(i),
                A_Thresh => Multiplicity(i),
                B_Thresh => Multiplicity(i+32),
		        Trigger_out => ChanTrig(i)
            );
    END generate GEN_Global_Trigger;

	GEN_Timeout:
		for i in 0 to 31 generate
			timeoutX : TimeOut_module
			PORT MAP(
				CLK => CLK,
				Reset => Reset,				
				Triggered_in => ChanTrig(i),
				Timeout_limit => TimeOut,
				Timeout_out => TOTrig(i)
			);
	END generate GEN_Timeout;

    StartMod : StartModule
    PORT MAP(
		CLK_start => CLK,
		Reset => Reset,
		User_Start => Soft_trigger,
		Start_Out => SoftTrig
    );

    
    TrigOut : Process (ChanTrig, SoftTrig) IS
    begin
		 IF (rising_edge(CLK)) THEN
			  FOR i in 0 to 31 loop
					Trigger_Out(i) <= TOTrig(i) OR SoftTrig;

			  END LOOP;
		 END IF;
    end process TrigOut;

	Testing : Process (CLK) is
        begin
            IF (rising_edge(CLK)) THEN
                for i in 5 downto 0 loop		
                    Test_MultA(i) <= A_PE(i);				
                    Test_MultB(i) <= B_PE(i);		
                end loop;
				for i in 31 DOWNTO 0 loop
					Test_Trig(i) <= TOTrig(i);
					Test_Soft(i) <= SoftTrig;
				end loop;
            end if;
        end process Testing;

END OutputLogic; 