library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity Counter_Method is
	port (
			CLK					: in	std_logic;
			Signal_In		: in 	std_logic;
			Counter_Out	: out std_logic_vector (5 DOWNTO 0) := "000000");

end Counter_Method;


architecture Counter_signal of Counter_Method is
	
BEGIN		
		counting		: process(CLK, Signal_In) is
		variable signal_holder	: std_logic;
		variable counter_sig		: std_logic_vector (5 DOWNTO 0) := "000000";
		
		
		begin
			if (rising_edge(CLK)) then
--				Counter_Out <= "000000";
--				if (Signal_In = '1') then
--					signal_holder := '1';
--				else
--					signal_holder := '0';
--				end if;
--				
--				
--				if (Signal_holder = '1') then
--					counter_sig <= counter_sig + 1;
--					Counter_Out <= counter_sig;
--				end if;
				
				if (rising_edge(Signal_In)) then
					counter_sig := counter_sig + 1;
					Counter_Out <= counter_sig;

				end if;
				
--				if (falling_edge(Signal_In)) then
--
--				end if;
				
				if (counter_sig = "111111") then
					counter_Out <= "000000";
				end if;
			else
				if (rising_edge(Signal_In)) then
					counter_sig := counter_sig + 1;
					Counter_Out <= counter_sig;

				end if;
				
--				if (falling_edge(Signal_In)) then
--
--				end if;
				
				if (counter_sig = "111111") then
					counter_Out <= "000000";
				end if;

				
			end if;
		
		end process counting;
end Counter_signal;
			