-- -- library IEEE;
-- -- use IEEE.STD_LOGIC_1164.ALL;

-- package array_typez is
--     type delay_array is array (31 DOWNTO 0) of std_logic_vector(7 DOWNTO 0);
-- end package array_typez;

-- package array_typez is
--     type scalar_array is array (31 DOWNTO 0) of std_logic_vector(5 DOWNTO 0);
-- end package array_typez;

-- -- package controlregs is
-- --     constant N_CONTROL_REGS : integer := 0;

-- --     type CONTROL_REGS_T is ARRAY (0 to N_CONTROL_REGS-1) of 
-- --                            STD_LOGIC_VECTOR(31 downto 0);
-- -- end package controlregs;


LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.std_logic_unsigned.ALL;
use work.array_typez.all;
--use work.array_typez.all;
-- use work.controlregs.all;



ENTITY SABRETrigger_Revamped IS
	
    PORT (
        CLK : IN STD_LOGIC;
		Test_out									: OUT STD_LOGIC_VECTOR (1 DOWNTO 0); 

        Port_Signal_In_A, Port_Signal_In_B          : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        --PortC_Out                                   : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);

        Channel_Mask_A, Channel_Mask_B              : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Delay_Value_A, Delay_Value_B                : IN delay_array;
        Coincidence_window_A, Coincidence_window_B  : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
        Coincidence_type_A, Coincidence_type_B      : IN STD_LOGIC;
        Multiplicity              				    : IN multiplicity_array;
		Singles_Active_A, Singles_Active_B			: IN STD_LOGIC;
        Scalar_Value_A, Scalar_Value_B              : IN scalar_array;
        Trigger_Modes                               : IN mode_array; 
        TimeOut                                     : IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		  
        Soft_Trigger                                : IN STD_LOGIC;
        Reset_In                                    : IN STD_LOGIC;

        GOUT0_Port                                  : IN STD_LOGIC_VECTOR (1 DOWNTO 0) := "00";
        GOUT0_Module                                : IN STD_LOGIC_VECTOR (3 DOWNTO 0) := "0000";
        GOUT0_Channel                               : IN STD_LOGIC_VECTOR (4 DOWNTO 0) := "00000";
        GOUT1_Port                                  : IN STD_LOGIC_VECTOR (1 DOWNTO 0) := "00";
        GOUT1_Module                                : IN STD_LOGIC_VECTOR (3 DOWNTO 0) := "0000";
        GOUT1_Channel                               : IN STD_LOGIC_VECTOR (4 DOWNTO 0) := "00000";

        Trigger_Out                                 : OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
      		);
END SABRETrigger_Revamped;


ARCHITECTURE TriggerLogic OF SABRETrigger_Revamped IS

    COMPONENT InputPortModule_Revamped IS  
        PORT (
            CLK : IN STD_LOGIC;
				Reset : IN STD_LOGIC;

				Port_Signal_In :     IN STD_LOGIC_VECTOR (31 DOWNTO 0);

				Channel_mask    :    IN STD_LOGIC_VECTOR (31 DOWNTO 0);

				Delay_Value :        IN delay_array;
				Coincidence_window : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
				Coincidence_type :   IN STD_LOGIC;

				Singles_Active:		  IN STD_LOGIC;
				Scalar_Value :       IN scalar_array;

				Test_Input			: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                Test_Chmask			: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                Test_MF				: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                Test_Delay			: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                Test_PE				: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                Test_Mult			: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                Test_SingDel		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                Test_SingDet		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                Test_SingCount		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
    
				Single_Trigger_Out : OUT STD_LOGIC;
				Mult_Out :   OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
            );
    END COMPONENT InputPortModule_Revamped;

    COMPONENT OutputModule_Revamped IS
	
    PORT (
        CLK             : IN STD_LOGIC;
        Reset           : IN STD_LOGIC;

        SinglesA        : IN STD_LOGIC;
        SinglesB        : IN STD_LOGIC;
        A_PE            : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        B_PE            : IN STD_LOGIC_VECTOR (31 DOWNTO 0);

        Trigger_Modes   : IN mode_array;
        Multiplicity    : IN multiplicity_array;
        Soft_trigger    : IN STD_LOGIC;
        TimeOut         : IN STD_LOGIC_VECTOR (5 DOWNTO 0);

        Test_MultA 		: OUT STD_LOGIC_VECTOR (5 DOWNTO 0);
		Test_MultB 		: OUT STD_LOGIC_VECTOR (5 DOWNTO 0);
		Test_Trig 		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
		Test_Soft 		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
        
        Trigger_out     : OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
        
		  );
    END COMPONENT OutputModule_Revamped;

    COMPONENT ResetModule IS
	    PORT (
		CLK         : IN STD_LOGIC;
		Reset_In    : IN STD_LOGIC;
		Reset_Out	: OUT STD_LOGIC
	    );
    END COMPONENT ResetModule;

    COMPONENT GOUTModule IS
	PORT (
		CLK                 : IN STD_LOGIC;
		Reset               : IN STD_LOGIC := '0';
		
		Test_Input_A		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_Chmask_A		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_MF_A			: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_Delay_A		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_PE_A			: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_Mult_A			: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_SingDel_A		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_SingDet_A		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_SingCount_A	: IN STD_LOGIC_VECTOR (31 DOWNTO 0);

        Test_Input_B		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_Chmask_B		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_MF_B			: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_Delay_B		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_PE_B			: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_Mult_B			: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_SingDel_B		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_SingDet_B		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_SingCount_B	: IN STD_LOGIC_VECTOR (31 DOWNTO 0);

        Test_MultA 		    : IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		Test_MultB 		    : IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		Test_Trig 		    : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		Test_Soft 		    : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		
		Test_Port           : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
        Test_Module         : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
        Test_Channel        : IN STD_LOGIC_VECTOR (4 DOWNTO 0);
		
		Test_out            : OUT STD_LOGIC := '0'
	);
    END COMPONENT GOUTModule;

    SIGNAL Multi_Out_A : STD_LOGIC_VECTOR(31 DOWNTO 0);
    SIGNAL Singles_Trigg_A : STD_LOGIC;
    SIGNAL Multi_Out_B : STD_LOGIC_VECTOR(31 DOWNTO 0);
    SIGNAL Singles_Trigg_B : STD_LOGIC;

    --Test signals
    SIGNAL Test_Input_A1		: STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_Chmask_A1		: STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_MF_A1			: STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_Delay_A1		: STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_PE_A1			: STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_Mult_A1		: STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_SingDel_A1		: STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_SingDet_A1		: STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_SingCount_A1	: STD_LOGIC_VECTOR (31 DOWNTO 0);

    SIGNAL Test_Input_B1		: STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_Chmask_B1		: STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_MF_B1			: STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_Delay_B1		: STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_PE_B1			: STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_Mult_B1		: STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_SingDel_B1		: STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_SingDet_B1		: STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_SingCount_B1	: STD_LOGIC_VECTOR (31 DOWNTO 0);

    SIGNAL Test_MultA1 	    : STD_LOGIC_VECTOR (5 DOWNTO 0);
    SIGNAL Test_MultB1 	    : STD_LOGIC_VECTOR (5 DOWNTO 0);
    SIGNAL Test_Trig1 		    : STD_LOGIC_VECTOR (31 DOWNTO 0);
    SIGNAL Test_Soft1 		    : STD_LOGIC_VECTOR (31 DOWNTO 0);

    --SIGNAL Start_Out : STD_LOGIC;
    SIGNAL Reset : STD_LOGIC;

BEGIN

    inputPortModuleA : InputPortModule_Revamped
    PORT MAP(
        CLK => CLK,
        Reset => Reset_In,
        Channel_mask => Channel_Mask_A,
        Port_Signal_In => Port_Signal_In_A,
        Delay_Value => Delay_Value_A,
        Coincidence_window => Coincidence_window_A,
        Coincidence_type => Coincidence_type_A,
		Singles_Active => Singles_Active_A,
        Scalar_Value => Scalar_Value_A,
        Single_Trigger_Out => Singles_Trigg_A,
        Mult_Out => Multi_Out_A,
		Test_Input => Test_Input_A1,
        Test_Chmask => Test_Chmask_A1,
        Test_MF => Test_MF_A1,
        Test_Delay => Test_Delay_A1,
        Test_PE => Test_PE_A1,
        Test_Mult => Test_Mult_A1,
        Test_SingDel => Test_SingDel_A1,
        Test_SingDet => Test_SingDet_A1,
        Test_SingCount => Test_SingCount_A1
    );

    inputPortModuleB : InputPortModule_Revamped
    PORT MAP(
        CLK => CLK,
        Reset => Reset_In,
        Channel_mask => Channel_Mask_B,
        Port_Signal_In => Port_Signal_In_B,
        Delay_Value => Delay_Value_B,
        Coincidence_window => Coincidence_window_B,
        Coincidence_type => Coincidence_type_B,
		Singles_Active => Singles_Active_B,
        Scalar_Value => Scalar_Value_B,
        Single_Trigger_Out => Singles_Trigg_B,
        Mult_Out => Multi_Out_B,
        Test_Input => Test_Input_B1,
        Test_Chmask => Test_Chmask_B1,
        Test_MF => Test_MF_B1,
        Test_Delay => Test_Delay_B1,
        Test_PE => Test_PE_B1,
        Test_Mult => Test_Mult_B1,
        Test_SingDel => Test_SingDel_B1,
        Test_SingDet => Test_SingDet_B1,
        Test_SingCount => Test_SingCount_B1
		  --Test_Out => Test_Out
    );

    OutPutMod : OutputModule_Revamped
    PORT MAP(
      CLK => CLK,
      Reset => Reset,
      SinglesA => Singles_Trigg_A,
      SinglesB => Singles_Trigg_B,
      A_PE => Multi_Out_A,
      B_PE => Multi_Out_B,
      Trigger_Modes => Trigger_Modes,
	  Multiplicity => Multiplicity,
      Soft_trigger => Soft_Trigger,
      Test_MultA => Test_MultA1,
	  Test_MultB =>	Test_MultB1,
	  Test_Trig => Test_Trig1,
	  Test_Soft => Test_Soft1,
      TimeOut => TimeOut,
      Trigger_out => Trigger_Out
    );

    resetModuleOne : ResetModule
    PORT MAP(
		CLK => CLK,
		Reset_In => Reset_In,
		Reset_Out => Reset
    );

    GOUT0Mod : GOUTModule
    PORT MAP(
        CLK => CLK,               
		Reset => Reset,         
		
		Test_Input_A => Test_Input_A1,		
        Test_Chmask_A => Test_Chmask_A1,	
        Test_MF_A => Test_MF_A1,			
        Test_Delay_A => Test_Delay_A1,		
        Test_PE_A => Test_PE_A1,			
        Test_Mult_A	=> Test_Mult_A1,		
        Test_SingDel_A => Test_SingDel_A1,	
        Test_SingDet_A => Test_SingDet_A1,	
        Test_SingCount_A => Test_SingCount_A1,	

        Test_Input_B => Test_Input_B1,
        Test_Chmask_B => Test_Chmask_B1,		
        Test_MF_B => Test_MF_B1,		
        Test_Delay_B => Test_Delay_B1,		
        Test_PE_B => Test_PE_B1,			
        Test_Mult_B	=> Test_Mult_B1,		
        Test_SingDel_B => Test_SingDel_B1,		
        Test_SingDet_B => Test_SingDet_B1,		
        Test_SingCount_B => Test_SingCount_B1,	

        Test_MultA => Test_MultA1, 		   
		Test_MultB => Test_MultB1,		   
		Test_Trig => Test_Trig1,		   
		Test_Soft => Test_Soft1, 		    
		
		Test_Port => GOUT0_Port,
        Test_Module => GOUT0_Module,     
        Test_Channel => GOUT0_Channel,        
		
		Test_out => Test_out(0)         
    );
	
    GOUT1Mod : GOUTModule
    PORT MAP(
        CLK => CLK,               
		Reset => Reset,         
		
		Test_Input_A => Test_Input_A1,		
        Test_Chmask_A => Test_Chmask_A1,	
        Test_MF_A => Test_MF_A1,			
        Test_Delay_A => Test_Delay_A1,		
        Test_PE_A => Test_PE_A1,			
        Test_Mult_A	=> Test_Mult_A1,		
        Test_SingDel_A => Test_SingDel_A1,	
        Test_SingDet_A => Test_SingDet_A1,	
        Test_SingCount_A => Test_SingCount_A1,	

        Test_Input_B => Test_Input_B1,
        Test_Chmask_B => Test_Chmask_B1,		
        Test_MF_B => Test_MF_B1,		
        Test_Delay_B => Test_Delay_B1,		
        Test_PE_B => Test_PE_B1,			
        Test_Mult_B	=> Test_Mult_B1,		
        Test_SingDel_B => Test_SingDel_B1,		
        Test_SingDet_B => Test_SingDet_B1,		
        Test_SingCount_B => Test_SingCount_B1,	

        Test_MultA => Test_MultA1, 		   
		Test_MultB => Test_MultB1,		   
		Test_Trig => Test_Trig1,		   
		Test_Soft => Test_Soft1, 		    
		
		Test_Port => GOUT1_Port,
        Test_Module => GOUT1_Module,     
        Test_Channel => GOUT1_Channel,        
		
		Test_out => Test_out(1)         
    );
	
    -- Branchsignals : Process (CLK) is
    --     begin
    --         IF (rising_edge(CLK)) THEN
    --             for i in 31 downto 0 loop
    --                 Test_Input(i) <= Port_Signal_In(i);		
                    
    --             end loop;
    --         end if;
    --     end process Branchsignals;
		--Test_out(0) <= Singles_Trigg_Out_A;
		--Test_out(1) <= Multi_Trigg_Out_B;
		--Test_out(1) <= Trigger_Out_A;
		
END TriggerLogic;