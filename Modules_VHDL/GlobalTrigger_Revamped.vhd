LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;

LIBRARY work;

ENTITY GlobalTrigger_Revamped IS
	PORT (
		CLK			 			: IN STD_LOGIC;
		Reset 					: IN STD_LOGIC := '0';
		
		A_Trigger_singles 		: IN STD_LOGIC;
		A_mult 					: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		B_Trigger_singles 		: IN STD_LOGIC;
		B_mult 					: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		
		Trigger_mode 			: IN STD_LOGIC_VECTOR (1 DOWNTO 0);
		A_Thresh 				: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		B_Thresh 				: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		
		Trigger_out 			: OUT STD_LOGIC := '0'
	);
END GlobalTrigger_Revamped;

ARCHITECTURE trigger OF GlobalTrigger_Revamped IS
BEGIN

	Global_trigger : PROCESS (CLK) IS
	BEGIN

		IF (rising_edge(CLK)) THEN
			-- Cycles through trigger modes
			IF (Reset = '1') THEN
				Trigger_out <= '0';
			ELSE
				IF (Trigger_mode = "00") THEN
					--output when any input fires
					IF ((A_Trigger_singles = '1') OR ((A_mult >= A_Thresh) and (A_Thresh > "00000")) OR (B_Trigger_singles = '1') OR  ((B_mult >= B_Thresh) and (B_Thresh > "00000"))) THEN
						Trigger_out <= '1';
					ELSE
						Trigger_out <= '0';
					END IF;
				

				ELSIF (Trigger_mode = "01") THEN
					--output when only input A fires
					IF ((A_Trigger_singles = '1') OR (A_mult >= A_Thresh)) THEN
						IF (A_Thresh > "00000") OR (A_Trigger_singles = '1') then
							Trigger_out <= '1';
						ELSE
							Trigger_out <= '0';
						END IF;
					ELSE
						Trigger_out <= '0';
					END IF;
				

				ELSIF (Trigger_mode = "10") THEN
					--output when only input B fires
					IF ((B_Trigger_singles = '1') OR (B_mult >= B_Thresh)) THEN
						IF (B_Thresh > "00000") OR (B_Trigger_singles = '1') then
							Trigger_out <= '1';
						ELSE
							Trigger_out <= '0';
						END IF;
					ELSE
						Trigger_out <= '0';
					END IF;
				

				ELSIF (Trigger_mode = "11") THEN
					--output as coincidence between A and B multiplicity
					IF ((A_mult >= A_Thresh) AND (B_mult >= B_Thresh)) THEN
						IF (A_Thresh > "00000") AND (B_Thresh > "00000") then
							Trigger_out <= '1';
						ELSE
							Trigger_out <= '0';
						END IF;
					ELSE
						Trigger_out <= '0';
					END IF;


				else
					Trigger_out <= CLK;
				END IF;
			END IF;
		END IF;

	END PROCESS Global_trigger;
END ARCHITECTURE trigger;