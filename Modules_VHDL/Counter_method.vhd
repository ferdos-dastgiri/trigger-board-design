library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity Counter_Method is
	port (
			CLK					: in	std_logic;
			Signal_In		: in 	std_logic;
			Reset			: in std_logic;
			Counter_Out	: out std_logic_vector (31 DOWNTO 0) := "00000000000000000000000000000000");

end Counter_Method;


architecture Counter_signal of Counter_Method is

	SIGNAL signal_In_carrier	: std_logic := '0';
	
BEGIN

		rising_edge_detect	: process(CLK, Reset, Signal_In) is

		variable rising_edge_signal	: std_logic := '0';
		variable signal_holder	: std_logic := '0';
		variable counter_sig		: std_logic_vector (31 DOWNTO 0) := "00000000000000000000000000000000";

			begin
				if (rising_edge(CLK)) then
					if (Reset = '1') then
						Signal_In_carrier <= '0';
						counter_sig := "00000000000000000000000000000000";
					
					else
						Signal_In_carrier <= Signal_In;
						rising_edge_signal := (not Signal_In_carrier) and (Signal_In);

						if (rising_edge_signal = '1') then
							counter_sig := counter_sig + 1;
							Counter_Out <= counter_sig;
						end if;
					end if;
				end if;

		
		end process rising_edge_detect;
end Counter_signal;
			