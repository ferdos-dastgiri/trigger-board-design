LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;

LIBRARY work;

ENTITY StartModule IS
	PORT (
		CLK_start 		: IN STD_LOGIC;
		Reset 		  	: IN STD_LOGIC := '0';

		User_Start 		: IN STD_LOGIC;

		Start_Out 		: OUT STD_LOGIC := '0'
	);
END StartModule;


ARCHITECTURE PushStart OF StartModule IS
	SIGNAL Status : STD_LOGIC;
	SIGNAL Counter : STD_LOGIC_VECTOR (2 DOWNTO 0);
BEGIN

	Startup : PROCESS (CLK_start, Reset, User_Start, Status, Counter) IS

	BEGIN
		IF (rising_edge(CLK_start)) THEN
			IF (Reset = '1') THEN
				Status <= '0';
				Counter <= "000";
				Start_Out <= '0';
			ELSE	
				IF (User_Start = '1') and (Status = '0') THEN
					IF (Counter = "101") THEN
						Start_out <= '0';
						Counter <= "000";
						--Status <= '0';	
					ELSE
						Status <= '1';
						Counter <= Counter + 1;
						Start_Out <= '1';
					END IF;
				ELSIF (User_start = '0') then
					IF (Counter = "101") THEN
						Status <= '0';
						Counter <= "000";
						Start_Out <= '0';
					elsif (Counter /= "000") then
						Counter <= Counter + 1;
						Start_Out <= '1';
						Status <= '0';
					ELSE
						Status <= '0';
						Counter <= "000";
						Start_Out <= '0';
					END IF;
				ELSE
					IF (Counter = "101") THEN
						--Status <= '0';
						Counter <= "000";
						Start_Out <= '0';
					elsif (Counter /= "000") then
						Counter <= Counter + 1;
						Start_Out <= '1';
					ELSE
						--Status <= '0';
						Counter <= "000";
						Start_Out <= '0';
					END IF;
				END IF;
			END IF;
		END IF;
	END PROCESS Startup;

END ARCHITECTURE PushStart;