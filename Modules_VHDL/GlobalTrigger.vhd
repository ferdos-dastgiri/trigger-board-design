LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;

LIBRARY work;

ENTITY GlobalTrigger IS
	PORT (
		CLK_trigger : IN STD_LOGIC;
		Reset : IN STD_LOGIC := '0';
		
		A_Trigger_singles : IN STD_LOGIC;
		A_Trigger_mult : IN STD_LOGIC;
		B_Trigger_singles : IN STD_LOGIC;
		B_Trigger_mult : IN STD_LOGIC;
		
		Trigger_mode : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
		
		Trigger_out : OUT STD_LOGIC := '0'
	);
END GlobalTrigger;

ARCHITECTURE trigger OF GlobalTrigger IS
BEGIN

	Global_trigger : PROCESS (CLK_trigger, Reset, A_Trigger_singles, A_Trigger_mult, B_Trigger_singles, B_Trigger_mult, Trigger_mode) IS
	BEGIN

		IF (rising_edge(CLK_trigger)) THEN
			-- Cycles through trigger modes
			IF (Reset = '1') THEN
				Trigger_out <= '0';
			ELSE
				IF (Trigger_mode = "00") THEN
					--output when any input fires
					IF (A_Trigger_singles = '1' OR A_Trigger_mult = '1' OR B_Trigger_singles = '1' OR B_Trigger_mult = '1') THEN
						Trigger_out <= '1';
					ELSE
						Trigger_out <= '0';
					END IF;
				END IF;

				IF (Trigger_mode = "01") THEN
					--output when only input A fires
					IF (A_Trigger_singles = '1' OR A_Trigger_mult = '1') THEN
						Trigger_out <= '1';
					ELSE
						Trigger_out <= '0';
					END IF;
				END IF;
				IF (Trigger_mode = "10") THEN
					--output when only input B fires
					IF (B_Trigger_singles = '1' OR B_Trigger_mult = '1') THEN
						Trigger_out <= '1';
					ELSE
						Trigger_out <= '0';
					END IF;
				END IF;

				IF (Trigger_mode = "11") THEN
					--output as coincidence between A and B multiplicity
					IF (A_Trigger_mult = '1' AND B_Trigger_mult = '1') THEN
						Trigger_out <= '1';
					ELSE
						Trigger_out <= '0';
					END IF;
				END IF;
			END IF;
		END IF;

	END PROCESS Global_trigger;
END ARCHITECTURE trigger;