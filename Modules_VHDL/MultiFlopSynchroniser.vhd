LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;


ENTITY RisingEdgeSyncFlipFlop IS
	PORT (
		CLK : IN STD_LOGIC;
		D : IN STD_LOGIC;
		Q : OUT STD_LOGIC := '0'
	);
END RisingEdgeSyncFlipFlop;

ARCHITECTURE rtl OF RisingEdgeSyncFlipFlop IS

BEGIN
	PROCESS (CLK) IS
	BEGIN
		IF (rising_edge(CLK)) THEN
            Q <= D;
		END IF;
	END PROCESS;

END ARCHITECTURE rtl;




LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;

library altera;
use altera.altera_syn_attributes.all;

ENTITY MultiFlopSynchroniser IS
	PORT (
		CLK : IN STD_LOGIC;
		asyncIn : IN STD_LOGIC;
		syncOut : OUT STD_LOGIC := '0'
	);
END MultiFlopSynchroniser;

ARCHITECTURE rtl OF MultiFlopSynchroniser IS
    SIGNAL tempState1 : STD_LOGIC;
    SIGNAL tempState2 : STD_LOGIC;

    attribute PRESERVE of tempState1            : signal is TRUE;
    attribute PRESERVE of tempState2            : signal is TRUE;
    attribute ALTERA_ATTRIBUTE of tempState1    : signal is "-name SYNCHRONIZER_IDENTIFICATION ""FORCED IF ASYNCHRONOUS""";
    attribute ALTERA_ATTRIBUTE of tempState2    : signal is "-name SYNCHRONIZER_IDENTIFICATION ""FORCED IF ASYNCHRONOUS""";
BEGIN

    FF1 : entity work.RisingEdgeSyncFlipFlop
    PORT MAP (
        CLK        => CLK,
        D          => asyncIn,
        Q          => tempState1
    );

    FF2 : entity work.RisingEdgeSyncFlipFlop
    PORT MAP (
        CLK      => CLK,
        D        => tempState1,
        Q        => tempState2
    );

    FF3 : entity work.RisingEdgeSyncFlipFlop
    PORT MAP (
        CLK      => CLK,
        D        => tempState2,
        Q        => syncOut
    );

END ARCHITECTURE rtl;


