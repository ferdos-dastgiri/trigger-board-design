-- this will be for the logic gate of the v2495
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.std_logic_unsigned.ALL;
ENTITY MultiplicityTrigger IS
	-- ----------------------------------------------
	PORT (
		CLK : IN STD_LOGIC; -- System clock (50 MHz)
		-- Port input A for Crystal
		Multiplicity_Signal_in : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		-- Multiplicity is a settable variable that must be put in as a signal 
		Multiplicity : IN STD_LOGIC_VECTOR (7 DOWNTO 0) := "11111111";
		MultiplicityTrigger_Out : OUT STD_LOGIC := '0'; -- trigger out signal to indicate multiplicity satisfied
		Reset			: IN STD_LOGIC := '0'
	);

END MultiplicityTrigger;


ARCHITECTURE Behavioural_ML OF MultiplicityTrigger IS
BEGIN
	Multiplicity_Trigg : PROCESS (CLK, Reset) IS
	-- Counter will be used to detect when Multiplicity is satisfied
	VARIABLE Counter_Mult : STD_LOGIC_VECTOR (7 DOWNTO 0) := "00000000";

	BEGIN

		IF (rising_edge(CLK)) THEN
			Counter_Mult := "00000000";
			MultiplicityTrigger_Out <= '0';
			
			IF (Reset = '1') then
				Counter_Mult := "00000000";
				MultiplicityTrigger_Out <= '0';
			ELSE
				FOR i IN 0 TO 31 LOOP
					IF (Multiplicity_Signal_in(i) = '1') THEN
						Counter_Mult := Counter_Mult + 1;

						IF (Counter_Mult = Multiplicity) THEN
							MultiplicityTrigger_Out <= '1';
							Counter_Mult := "00000000";
						END IF;
					END IF;
				END LOOP;
			END IF;
		END IF;

	END PROCESS Multiplicity_Trigg;

END Behavioural_ML;