-- library IEEE;
-- use IEEE.STD_LOGIC_1164.ALL;

-- package array_typez is
-- type delay_array is array (31 DOWNTO 0) of std_logic_vector(7 DOWNTO 0);
-- end package array_typez;


LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.std_logic_unsigned.ALL;
use work.array_typez.all;


ENTITY Delaymodulecombined IS
	
    PORT (
        CLK : IN STD_LOGIC;
        Delay_Signal_In : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Delay_Value : IN delay_array;
        Delay_Out : OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
		  );
END Delaymodulecombined;


ARCHITECTURE Delay_Mega_Module OF Delaymodulecombined IS

    COMPONENT DelayModule IS
	PORT (
		CLK : IN STD_LOGIC;
		Delay_Signal_In : IN STD_LOGIC;
		Delay_Out : OUT STD_LOGIC := '0';
		Delay_Value : IN INTEGER
	);
    END COMPONENT DelayModule;

BEGIN

    GEN_Delay_Module:
        for i in 0 to 31 generate
            DelayModuleXin : DelayModule
            PORT MAP(
                CLK => CLK,
                Delay_Signal_In => Delay_Signal_In(i),
                Delay_Out => Delay_Out(i),
                Delay_Value => to_integer(signed(Delay_Value(i)))
            );
    END generate GEN_Delay_Module;      

END Delay_Mega_Module;