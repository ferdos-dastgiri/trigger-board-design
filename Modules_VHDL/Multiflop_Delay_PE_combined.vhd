LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.std_logic_unsigned.ALL;
use work.array_typez.all;


ENTITY Multiflop_Delay_PE_combined IS
	
    PORT (
        CLK : IN STD_LOGIC;
        Reset : IN STD_LOGIC;
        Signal_In : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Delay_Value : IN delay_array :=(others=>"000000");
        Coincidence_window : IN STD_LOGIC_VECTOR (7 DOWNTO 0) := "00000001";
        Coincidence_type : IN STD_LOGIC := '0';

        Test_MF : OUT STD_LOGIC_VECTOR (31 DOWNTO 0) :="00000000000000000000000000000000";
        Test_Delay : OUT STD_LOGIC_VECTOR (31 DOWNTO 0) := "00000000000000000000000000000000";
        
        Signal_Out : OUT STD_LOGIC_VECTOR (31 DOWNTO 0) :="00000000000000000000000000000000"
		  );
END Multiflop_Delay_PE_combined;


ARCHITECTURE Input_Port_Stage1_Module OF Multiflop_Delay_PE_combined IS

    COMPONENT MultiFlopSynchroniser IS
        PORT (
            CLK : IN STD_LOGIC;
            asyncIn : IN STD_LOGIC;
            syncOut : OUT STD_LOGIC
        );
    END COMPONENT MultiFlopSynchroniser;

    COMPONENT DelayModule IS
	PORT (
		CLK : IN STD_LOGIC;
		Delay_Signal_In : IN STD_LOGIC;
        Reset : IN STD_LOGIC;
		Delay_Out : OUT STD_LOGIC := '0';
		Delay_Value : IN STD_LOGIC_VECTOR (5 DOWNTO 0) := "000000"
	);
    END COMPONENT DelayModule;

    COMPONENT PulseExtenderCombined IS
    PORT (
        CLK : IN STD_LOGIC;
        Reset : IN STD_LOGIC := '0';
        Pulse_Extend_Signal_In : IN STD_LOGIC;
        Pulse_Select_Mode : IN STD_LOGIC;
        Coincidence_Window : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
        Pulse_Extend_Out : OUT STD_LOGIC);
    END COMPONENT PulseExtenderCombined;
	 
    SIGNAL MultiFlop_Output : STD_LOGIC_VECTOR(31 DOWNTO 0);
    SIGNAL Delay_Output : STD_LOGIC_VECTOR(31 DOWNTO 0);

BEGIN

    GENMultiFlopSynchroniser:
        for i in 0 to 31 generate
            MultiFlopSynchroniserX : MultiFlopSynchroniser
            PORT MAP(
                CLK => CLK,
                asyncIn => Signal_In(i),
                syncOut => MultiFlop_Output(i)
            );
    END generate GENMultiFlopSynchroniser; 

    GEN_Delay_Module:
        for i in 0 to 31 generate
            DelayModuleXin : DelayModule
            PORT MAP(
                CLK => CLK,
                Reset => Reset,
                Delay_Signal_In => MultiFlop_Output(i),
                Delay_Out => Delay_Output(i),
                Delay_Value => Delay_Value(i)
            );
    END generate GEN_Delay_Module; 
    
    GEN_PE_Module:
        for i in 0 to 31 generate
            PEModuleXin : PulseExtenderCombined
            PORT MAP(
                CLK => CLK,
                Reset => Reset,
                Pulse_Extend_Signal_In => Delay_Output(i),
                Pulse_Select_Mode => Coincidence_type,
                Coincidence_Window => Coincidence_window,
                Pulse_Extend_Out => Signal_Out(i)
            );
    END generate GEN_PE_Module; 

    Testing : Process (CLK) is
        begin
            IF (rising_edge(CLK)) THEN
                for i in 31 downto 0 loop		
                    Test_MF(i) <= MultiFlop_Output(i);				
                    Test_Delay(i) <= Delay_Output(i);		
                end loop;
            end if;
        end process Testing;

END Input_Port_Stage1_Module;