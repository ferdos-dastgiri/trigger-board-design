LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;

LIBRARY work;

ENTITY GOUTModule IS
	PORT (
		CLK                 : IN STD_LOGIC;
		Reset               : IN STD_LOGIC := '0';
		
		Test_Input_A		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_Chmask_A		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_MF_A			: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_Delay_A		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_PE_A			: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_Mult_A			: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
        Test_SingDel_A		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_SingDet_A		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_SingCount_A	: IN STD_LOGIC_VECTOR (31 DOWNTO 0);

        Test_Input_B		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_Chmask_B		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_MF_B			: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_Delay_B		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_PE_B			: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_Mult_B			: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
        Test_SingDel_B		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_SingDet_B		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        Test_SingCount_B	: IN STD_LOGIC_VECTOR (31 DOWNTO 0);

        Test_MultA 		    : IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		Test_MultB 		    : IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		Test_Trig 		    : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		Test_Soft 		    : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		
		Test_Port           : IN STD_LOGIC_VECTOR (1 DOWNTO 0) := "00";
        Test_Module         : IN STD_LOGIC_VECTOR (3 DOWNTO 0) := "0000";
        Test_Channel        : IN STD_LOGIC_VECTOR (4 DOWNTO 0) := "00000";
		
		Test_out            : OUT STD_LOGIC := '0'
	);
END GOUTModule;

ARCHITECTURE Testing OF GOUTModule IS

    SIGNAL channel : INTEGER;

BEGIN

	Get_GOUT : PROCESS (CLK) IS
	BEGIN

		IF (rising_edge(CLK)) THEN
			-- Cycles through trigger modes
			IF (Reset = '1') THEN
				Test_out <= '0';
			ELSE
                channel <= to_integer(unsigned(Test_Channel));

                --Select port
                IF (Test_Port = "00") THEN
                    --Port A selected
                    IF (Test_Module = "0000") THEN
                        --outputs 
                        Test_out <= Test_Input_A(channel);
                    
                    ELSIF (Test_Module = "0001") THEN
                        --outputs 
                        Test_out <= Test_Chmask_A(channel);

                    ELSIF (Test_Module = "0010") THEN
                        --outputs 
                        Test_out <= Test_MF_A(channel);

                    ELSIF (Test_Module = "0011") THEN
                        --outputs 
                        Test_out <= Test_Delay_A(channel);
                    
                    ELSIF (Test_Module = "0100") THEN
                        --outputs 
                        Test_out <= Test_PE_A(channel);
                    
                    ELSIF (Test_Module = "0101") THEN
                        --outputs 
                        Test_out <= Test_Mult_A(channel);
                    
                    ELSIF (Test_Module = "0110") THEN
                        --outputs 
                        Test_out <= Test_SingDel_A(channel);
                    
                    ELSIF (Test_Module = "0111") THEN
                        --outputs 
                        Test_out <= Test_SingDet_A(channel);
                    
                    ELSIF (Test_Module = "1000") THEN
                        --outputs 
                        Test_out <= Test_SingCount_A(channel);
                    END IF;
				

                ELSIF (Test_Port = "01") THEN
                    --Port B selected
                    IF (Test_Module = "0000") THEN
                        --outputs 
                        Test_out <= Test_Input_B(channel);
                    
                    ELSIF (Test_Module = "0001") THEN
                        --outputs 
                        Test_out <= Test_Chmask_B(channel);

                    ELSIF (Test_Module = "0010") THEN
                        --outputs 
                        Test_out <= Test_MF_B(channel);

                    ELSIF (Test_Module = "0011") THEN
                        --outputs 
                        Test_out <= Test_Delay_B(channel);
                    
                    ELSIF (Test_Module = "0100") THEN
                        --outputs 
                        Test_out <= Test_PE_B(channel);
                    
                    ELSIF (Test_Module = "0101") THEN
                        --outputs 
                        Test_out <= Test_Mult_B(channel);
                    
                    ELSIF (Test_Module = "0110") THEN
                        --outputs 
                        Test_out <= Test_SingDel_B(channel);
                    
                    ELSIF (Test_Module = "0111") THEN
                        --outputs 
                        Test_out <= Test_SingDet_B(channel);
                    
                    ELSIF (Test_Module = "1000") THEN
                        --outputs 
                        Test_out <= Test_SingCount_B(channel);
                    END IF;
                
                ELSIF (Test_Port = "10") THEN
                    --Output module selected
                    IF (Test_Module = "0000") THEN
                        --outputs 
                        Test_out <= Test_MultA(channel);

                    ELSIF (Test_Module = "0001") THEN
                        --outputs 
                        Test_out <= Test_MultB(channel);
                    

                    ELSIF (Test_Module = "0010") THEN
                        --outputs 
                        Test_out <= Test_Trig(channel);
                    

                    ELSIF (Test_Module = "0011") THEN
                        --outputs 
                        Test_out <= Test_Soft(channel);
                   
                    END IF;
                END IF;

			END IF;
		END IF;

	END PROCESS Get_GOUT;
END ARCHITECTURE Testing;